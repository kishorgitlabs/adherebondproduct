package SharedPreference;

/**
 * Created by Systems02 on 23-Oct-17.
 */

public class Shared {

    public static final String MyPREFERENCES = "Registration" ;
    public static final String K_Register = "isregister";
    public static final String K_RegisterName = "Name";
    public static final String K_RegisterEmail = "Email";
    public static final String K_RegisterMobNo = "MobNo";
    public static final String K_RegisterUsertype = "Usertype";
    public static final String K_Login = "islogin";
    public static final String K_Sales_name = "name";
    public static final String K_Sales_pass = "pass";
    public static final String K_Sales_email = "email";
    public static final String K_Sales_mobile = "mobile";
    public static final String K_Sale_emailpass = "emailpass";
    public static final String K_Sales_signature = "signature";
    public static final String K_Sales_salesID = "salesID";
    public static final String K_Sales_salesbranch = "salesbranch";
    public static final String K_Sales_salesemail = "salesemail";
    public static final String K_Sales_salessms = "salessms";
    public static final String K_Sales_paymentemail = "paymentemail";
    public static final String K_Sales_paymentsms = "paymentsms";
    public static final String K_Synchronize = "synchronize";

}
