package alertbox;


import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;


/**
 * Created by Systems02 on 22-May-17.
 */

public class AlertDialogue {

    private Context context;
    public AlertDialog alertDialog;

    public AlertDialogue(Context context) {
        this.context = context;
    }

    public void showAlertbox(String msg) {
        alertDialog = new Builder(
                context).create();
        alertDialog.setMessage(msg);
        alertDialog.setTitle("Adhere Bonds");
        //alertDialog.setButton();
        alertDialog.setButton(Dialog.BUTTON_POSITIVE,"Okay",new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialog.show();
    }
}
