package alertbox;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


//import com.brainmagic.adherebond.R;

import com.brainmagic.adherebond.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import network.NetworkConnection;
import persistency.ServerConnection;

import static android.content.Context.MODE_PRIVATE;


public class LoginAlert {
	Context context;
	Connection connection;
	Statement statement;
	ResultSet resultSet;
	private ProgressDialog progress;
	String user,pass;
	private SharedPreferences myshare;
	private SharedPreferences.Editor editor;
	private String salesName,salesPass,salesEmail,salesEmailPass,salesEmailSign,salesID;

	//editor = myshare.edit();

	public LoginAlert(Context con) {
		// TODO Auto-generated constructor stub
		this.context = con;
			}

//	editor = myshare.edit();

	public void showLoginbox()
	{

		myshare = context.getSharedPreferences("Registration", MODE_PRIVATE);
		editor = myshare.edit();
		final AlertDialog alertDialog = new Builder(
				context).create();
		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.loginalert, null);
		alertDialog.setView(dialogView);
		TextView log = (TextView) dialogView.findViewById(R.id.loglabel);

		final EditText Username=(EditText)dialogView.findViewById(R.id.username1);
		final EditText Password=(EditText)dialogView.findViewById(R.id.password1);
		Button Login1=(Button) dialogView.findViewById(R.id.login1);
		Button Cancel1=(Button) dialogView.findViewById(R.id.cancel1);
		Cancel1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				alertDialog.dismiss();
			}
		});



		Login1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

				user = Username.getText().toString();
				pass = Password.getText().toString();
				if (user.equals("")) {
					Username.setError("Enter Username");
				} else if (pass.equals("")) {
					Password.setError("Enter Password");
				} else {
					CheckInternet();
				}
				alertDialog.dismiss();
			}
		});

		alertDialog.show();
	}


	private void CheckInternet() {
		try {
			NetworkConnection network = new NetworkConnection(context);
			if (network.CheckInternet()) {
				new verifylogin().execute();
				//askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, WRITE_EXST);
			} else {
				Alertbox alert = new Alertbox(context);
				alert.showAlertbox("Kindly check your Internet Connection");
			}

		}catch (Exception e) {
			e.printStackTrace();

		}
	}

	class verifylogin extends AsyncTask<String, Void, String> {
		int i;

		protected void onPreExecute() {
			super.onPreExecute();
			progress = new ProgressDialog(context);
			progress.setMessage("Loading...");
			progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progress.setCancelable(false);
			progress.show();

		}

		@Override
		protected String doInBackground(String... strings) {

			ServerConnection serverConnection = new ServerConnection();
			try {
				connection = serverConnection.getConnection();
                /*statement = connection.prepareStatement(selectquery);*/
				statement = connection.createStatement();
				String selectquery = "SELECT id,UserName,Password,EmailId,EmailPassword,EmailSignature from CeraLogin where UserName='" + user + "' and Password='" + pass + "'";
				Log.v("query", selectquery);
				resultSet = statement.executeQuery(selectquery);
				if (resultSet.next())
				{
					salesID = resultSet.getString("id");
					salesName = resultSet.getString("UserName");
					salesPass = resultSet.getString("Password");
					salesEmail = resultSet.getString("EmailId");
					salesEmailPass = resultSet.getString("EmailPassword");
					salesEmailSign = resultSet.getString("EmailSignature");
					statement.close();
					connection.close();
					resultSet.close();
					return "sucess";

				} else
				{
					statement.close();
					connection.close();
					resultSet.close();
					return "missmatch";
				}


			} catch (Exception e) {
				e.printStackTrace();
				return "error_insertion";
			}

		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);
			progress.dismiss();
			if (s.equals("sucess")) {
				Toast.makeText(context,"Login sucessfull", Toast.LENGTH_SHORT).show();
				//alert.showAlertbox("Login sucessful");
				editor.putBoolean("islogin", true);
				editor.putBoolean("islogin", true);
				editor.putString("name", salesName);
				editor.putString("pass", salesPass);
				editor.putString("email", salesEmail);
				editor.putString("emailpass", salesEmailPass);
				editor.putString("signature",salesEmailSign);
				editor.putString("salesID",salesID);
				editor.commit();

				//Intent i = new Intent(Login_Activity.this, Welcome_Activity.class);
				//Intent i=new Intent(context, Offers_Activity.class);
				//context.startActivity(i);

			} else if (s.equals("missmatch")) {
				Alertbox alert = new Alertbox(context);
				alert.showAlertbox("Check username and password");
			} else {
				Alertbox alert = new Alertbox(context);
				alert.showAlertbox("unble to connect Please check your internet connecion");
			}
		}
	}
	}




