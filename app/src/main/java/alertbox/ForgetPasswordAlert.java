package alertbox;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;


import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.PaymentingActivity;
//import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.Welcome_Activity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import api.models.forget.ForgetData;
import api.models.master.Master;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import email.Mail;
import network.NetworkConnection;
import persistency.ServerConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;


public class ForgetPasswordAlert {
	Context context;
	EditText Email;
	Button send,cancel;
	//private SpotsDialog progressDialog;
	Connection connection;
	ProgressDialog progressDialog1,progressDialog2;
	Statement stmt;
	ResultSet resultSet;
	String Name,UserName,Password,Enteredemail;
	SharedPreferences myshare;
	Alertbox alert;



	private SharedPreferences.Editor editor;
	private String SalesPass,SalesId,Old,New,Confirm;

	//editor = myshare.edit();

	public ForgetPasswordAlert(Context con) {
		// TODO Auto-generated constructor stub
		this.context = con;
		alert =new Alertbox(this.context);
			}

 	//editor = myshare.edit();
	public void showLoginbox()
	{

		myshare = context.getSharedPreferences("Registration", MODE_PRIVATE);
		editor = myshare.edit();
		final AlertDialog alertDialog = new Builder(
				context).create();
		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.forget_password, null);
		alertDialog.setView(dialogView);
		//TextView log = (TextView) dialogView.findViewById(R.id.loglabel);

		Email=(EditText)dialogView.findViewById(R.id.email);
		send =(Button) dialogView.findViewById(R.id.send);
		cancel=(Button) dialogView.findViewById(R.id.cancel);
		cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				alertDialog.dismiss();
			}
		});



		send.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Enteredemail = Email.getText().toString();

				if (Enteredemail.equals("")) {
					Email.setFocusable(true);
					Email.setError("Please enter your Adherebonds Email !");
					}else if(!isValidEmail(Enteredemail.trim()))
				{
					Email.setFocusable(true);
					Email.setError("Enter valid Email ID");

				}
				else{

				   CheckInternet();
					alertDialog.dismiss();
                }
			}
		});
		alertDialog.setCancelable(false);
		alertDialog.show();
	}


	private void CheckInternet() {
		try {
			NetworkConnection network = new NetworkConnection(context);
			if (network.CheckInternet()) {
				forgotpassword(Enteredemail);
				//askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, WRITE_EXST);
			} else {
				Alertbox alert = new Alertbox(context);
				alert.showAlertbox("Kindly check your Internet Connection");

			}

		}catch (Exception e) {
			e.printStackTrace();

		}
	}

	private void forgotpassword(String email) {
		final ProgressDialog progressDialog = new ProgressDialog(context);
		progressDialog.setMessage("Loading...");
		progressDialog.show();

		APIService service = RetroClient.getApiService();
		Call<ForgetData> call = service.GetMasterData1(email);
		call.enqueue(new Callback<ForgetData>() {
			@Override
			public void onResponse(Call<ForgetData> call, Response<ForgetData> response) {
				progressDialog.dismiss();
				switch (response.body().getResult())
				{
					case "Success":
						alert.showAlertbox("Password has been sent to your Email successfully");
						break;
					case "MailIdNotFound":
						alert.showAlertbox("Please enter the registered Email!");
						break;
					default: {
						alert.showAlertbox("Connection interrupted! Please try again");
						break;
					}
				}
				}



			@Override
			public void onFailure(Call<ForgetData> call, Throwable t)
			{
				progressDialog.dismiss();
				alert.showAlertbox("Connection interrupted! Please try again");


			}
		});

	}


	public static boolean isValidEmail(CharSequence target) {
		if (TextUtils.isEmpty(target))
		{
			return false;
		}
		else
		{
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
		}
	}

}










