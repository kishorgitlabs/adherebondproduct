package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

//import com.brainmagic.adherebond.R;

import com.brainmagic.adherebond.R;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Systems02 on 20-Jan-17.
 */

 public class Offers_Adapter extends ArrayAdapter {
    Context context;

    ArrayList<String> SchemeList,DescriptionList,DateList;
    //ArrayList<String> map;
    //ArrayList<String> whatsnew_segment,whatsnew_eng_pec,Descriptionlistb,whatsnew_gates_partno,whatsnew_oe_partno,whatsnew_mrp;


    public Offers_Adapter(Context context, ArrayList<String> SchemeList, ArrayList<String> DescriptionList, ArrayList<String> DateList) {
        super(context, R.layout.ftp_adapter_offers, SchemeList);
        this.context = context;
        this.SchemeList = SchemeList;
        this.DescriptionList = DescriptionList;
        this.DateList = DateList;
        // this.map = map;
        //this. whatsnew_segment =whatsnew_segment;
        //this. whatsnew_eng_spec =whatsnew_eng_spec;
        //this. Descriptionlistb =Descriptionlistb;
        //this. whatsnew_gates_partno =whatsnew_gates_partno;
        //this. whatsnew_oe_partno =whatsnew_oe_partno;
        //this. whatsnew_mrp =whatsnew_mrp;

    }




    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Offers_Adapter.Whats_NewOrderHolder holder;

        if (convertView == null) {

            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.ftp_adapter_offers, parent, false);

            holder = new Offers_Adapter.Whats_NewOrderHolder();


            TextView name = (TextView) convertView.findViewById(R.id.adapter_scheme);
            TextView desc = (TextView) convertView.findViewById(R.id.adapter_desc);
            TextView details = (TextView) convertView.findViewById(R.id.adapter_date);
           /*TextView adapter_s_no = (TextView) convertView.findViewById(R.id.adapter_sno);ImageView adapter_more_info = (ImageView) convertView.findViewById(R.id.adapter_more_info);
           ImageView adapter_image = (ImageView) convertView.findViewById(R.id.adapter_image);*/
            //adapter_s_no.setText(Integer.toString(position+1)+".");
            name.setText(SchemeList.get(position));
            desc.setText(DescriptionList.get(position));

            if(DateList.get(position) != null);
            String[] date1 =DateList.get(position).split(" ");
            details.setText(date1[0]);
            //adapter_image.setImageBitmap(map.get(position));

           /*Picasso.with(context)
                    .load(map.get(position).toString())
                    // .resize(200,200)                       // optional
                    .into(adapter_image);

            adapter_more_info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    Intent a = new Intent(context, Whats_New_More_Details.class);
                    a.putExtra("WHATS_NEW_VEHICLE",ProductnameList.get(position).toString());
                    a.putExtra("WHATS_NEW_MODEL",Descriptionlist.get(position).toString());
                    a.putExtra("WHATS_NEW_PRODUCTATTACHMENT",map.get(position).toString());
                    // a.putExtra("WHATS_NEW_MAP",map.get(i).toString());
                    a.putExtra("WHATS_NEW_SEGMENT",whatsnew_segment.get(position).toString());
                    a.putExtra("WHATS_NEW_ENGG_SPEC",whatsnew_eng_spec.get(position).toString());
                    a.putExtra("WHATS_NEW_PRD_TYPE",DetailsList.get(position).toString());
                    a.putExtra("WHATS_NEW_DESCB",Descriptionlistb.get(position).toString());
                    a.putExtra("WHATS_NEW_GATES_PARTNO",whatsnew_gates_partno.get(position).toString());
                    a.putExtra("WHATS_NEW_PARTNO",whatsnew_oe_partno.get(position).toString());
                    a.putExtra("WHATS_NEW_MRP",whatsnew_mrp.get(position).toString());
                    context.startActivity(a);
                }
                });*/




            convertView.setTag(holder);
        }


        //convertView.setTag(holder);


        else

        {
            holder = (Offers_Adapter.Whats_NewOrderHolder) convertView.getTag();
        }


        return convertView;
    }


    private class Whats_NewOrderHolder {
        public TextView sno;
        public TextView orderno;
        public TextView datetxt;
        public TextView delarname;


    }
}

