package adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


//import com.brainmagic.adherebond.R;

import com.brainmagic.adherebond.Preview_Activity;
import com.brainmagic.adherebond.R;

import java.util.ArrayList;
import java.util.List;

import api.models.OrderView.DataItem;
import database.DBHelpersync;

/*
*
 * Created by system01 on 2/12/2017.
*/


public class PreviewAdapter extends ArrayAdapter {

    private Context context;
    private ArrayList<String> NameList,ProductList,PackList,QuantityList,DisList,DateList,TypeList,Modelist,Colorlist;
    private ArrayList<Integer> IDList;



    public PreviewAdapter(Context context, ArrayList<Integer> IDList, ArrayList<String> NameList, ArrayList<String> ProductList, ArrayList<String> PackList, ArrayList<String> QuantityList, ArrayList<String> DisList, ArrayList<String> DateList, ArrayList<String> TypeList,ArrayList<String> Modelist,ArrayList<String> Colorlist) {
        super(context, R.layout.preview_adapter, NameList);
        this.context = context;
        this.NameList = NameList;
        this.ProductList = ProductList;
        this.PackList = PackList;
        this.QuantityList = QuantityList;
        this.DisList = DisList;
        this.DateList= DateList;
        this.TypeList= TypeList;
        this.IDList= IDList;
        this.Modelist=Modelist;
        this.Colorlist=Colorlist;

    }


    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null)
        {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.preview_adapter, parent, false);

            ImageView delete  = (ImageView) convertView.findViewById(R.id.delete);
            ImageView edit  = (ImageView) convertView.findViewById(R.id.update);
            TextView SNo = (TextView) convertView.findViewById(R.id.sno);
            TextView text1 = (TextView) convertView.findViewById(R.id.text1);
            TextView text2 = (TextView) convertView.findViewById(R.id.text2);
            TextView text3 = (TextView) convertView.findViewById(R.id.text3);
            TextView text4 = (TextView) convertView.findViewById(R.id.text4);
            TextView text5 = (TextView) convertView.findViewById(R.id.text5);
            TextView text6 = (TextView) convertView.findViewById(R.id.text6);
            TextView text7 = (TextView) convertView.findViewById(R.id.text7);
            TextView text8 = (TextView) convertView.findViewById(R.id.text8);
            TextView text9=(TextView)convertView.findViewById(R.id.text9);

            convertView.setTag(convertView);
            SNo.setText(Integer.toString(position+1));
            text1.setText(NameList.get(position));
            text2.setText(ProductList.get(position));
            text3.setText(PackList.get(position));
            text4.setText(QuantityList.get(position));
            text5.setText(DisList.get(position));
            text6.setText(DateList.get(position));
            text7.setText(TypeList.get(position));
            text8.setText(Modelist.get(position));
            text9.setText(Colorlist.get(position));


            delete.setTag(position);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder adb=new AlertDialog.Builder(context);
                    adb.setTitle("Delete?");
                    adb.setMessage("Are you sure you want to delete " + ProductList.get(position));
                    adb.setNegativeButton("Cancel", null);
                    adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            deletelist(IDList.get(position));
                            remove(ProductList.get(position));
                            IDList.remove(position);
                            NameList.remove(position);
                            ProductList.remove(position);
                            QuantityList.remove(position);
                            DisList.remove(position);
                            DateList.remove(position);
                            TypeList.remove(position);
                            Modelist.remove(position);
                            Colorlist.remove(position);
                            notifyDataSetChanged();
                        }});
                    adb.show();
                }
            });
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showAlertbox(IDList.get(position), position,NameList.get(position),ProductList.get(position),PackList.get(position),TypeList.get(position),DisList.get(position),QuantityList.get(position),Modelist.get(position),Colorlist.get(position));
                    notifyDataSetChanged();
                }
            });

        }

        return convertView;
    }
    private void deletelist(int orderid)
    {

        SQLiteDatabase db = null;
        DBHelpersync dbhelper = new DBHelpersync(context);
        db = dbhelper.getWritableDatabase();
        db.execSQL("delete from Orderpreview where id='"+orderid+"'");
        db.close();
    }
    public void showAlertbox(final int id, final int pos, String name, String product, String pack, String type, String discount, String quantity,String mode,String color)
    {
        final AlertDialog alertDialog = new AlertDialog.Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.update_order, null);
        alertDialog.setView(dialogView);

        TextView Name = (TextView) dialogView.findViewById(R.id.customer);
        TextView Product = (TextView) dialogView.findViewById(R.id.product);
        TextView Pack = (TextView) dialogView.findViewById(R.id.pack);
        TextView Type = (TextView) dialogView.findViewById(R.id.type);
        TextView Mode = (TextView) dialogView.findViewById(R.id.mode);
        TextView Colour=(TextView)dialogView.findViewById(R.id.color);
        final EditText Discount = (EditText) dialogView.findViewById(R.id.discount);
        final EditText Quantity = (EditText) dialogView.findViewById(R.id.quantity);

        Button Save = (Button)  dialogView.findViewById(R.id.save);
        Button Cancel = (Button)  dialogView.findViewById(R.id.cancel);
        Name.setText(name);
        Product.setText(product);
        Pack.setText(pack);
        Type.setText(type);
        Mode.setText(mode);
        Discount.setText(discount);
        Quantity.setText(quantity);
        Colour.setText(color);

        Save.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                SQLiteDatabase db = null;
                DBHelpersync dbhelper = new DBHelpersync(context);
                db = dbhelper.getWritableDatabase();
                String query = "update Orderpreview set quantity = '"+Quantity.getText().toString()+"', discount = '"+Discount.getText().toString()+"' where id = '"+id+"'";
                //update Orderpreview set quantity = 5, discount = 10 where id = 1
                db.execSQL(query);
                db.close();

                QuantityList.remove(pos);
                DisList.remove(pos);
                QuantityList.add(pos,Quantity.getText().toString());
                DisList.add(pos,Discount.getText().toString());

//                QuantityList.replace(QuantityList.get(pos),Quantity.getText().toString());
//                DisList.get(pos).replace(DisList.get(pos),Discount.getText().toString());
                notifyDataSetChanged();
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }


}
