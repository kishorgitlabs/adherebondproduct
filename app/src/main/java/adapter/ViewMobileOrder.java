package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


//import com.brainmagic.adherebond.R;

import com.brainmagic.adherebond.R;

import java.util.ArrayList;

/**
 * Created by system01 on 2/12/2017.
 */

public class ViewMobileOrder extends ArrayAdapter<String> {

    private  Context context;
    private ArrayList<String> CustomerNameList,OrderIdlist,Productnamelist,StatusList,PackList,QtyList,DateList,modelist,numberlist,colorlist,discountlist;

    public ViewMobileOrder(Context context, ArrayList<String> CustomerNameList, ArrayList<String> OrderIdlist, ArrayList<String> Productnamelist, ArrayList<String> StatusList, ArrayList<String> PackList, ArrayList<String> QtyList, ArrayList<String> DateList,ArrayList<String> modelist,ArrayList<String> numberlist,ArrayList<String> colorlist,ArrayList<String> discountlist) {
        super(context, R.layout.view_status_adapter, CustomerNameList);
        this.context = context;
        this.CustomerNameList= CustomerNameList;
        this.OrderIdlist  = OrderIdlist;
        this.Productnamelist = Productnamelist;
        this.StatusList = StatusList;
        this.PackList = PackList;
        this.QtyList = QtyList;
        this.DateList = DateList;
        this.modelist = modelist;
        this.numberlist=numberlist;
        this.colorlist=colorlist;
        this.discountlist=discountlist;



    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null)
        {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.view_status_adapter, parent, false);

            TextView text1 = (TextView) convertView.findViewById(R.id.text1);
            TextView text2 = (TextView) convertView.findViewById(R.id.text2);
            TextView text3 = (TextView) convertView.findViewById(R.id.text3);
            TextView text4 = (TextView) convertView.findViewById(R.id.text4);
            TextView text5 = (TextView) convertView.findViewById(R.id.text5);
            TextView text6 = (TextView) convertView.findViewById(R.id.text6);
            TextView text7 = (TextView) convertView.findViewById(R.id.text7);
            TextView text8 = (TextView) convertView.findViewById(R.id.text8);
            TextView text9 = (TextView) convertView.findViewById(R.id.text9);
            TextView text10= (TextView) convertView.findViewById(R.id.text10);
            TextView text11= (TextView) convertView.findViewById(R.id.text11);

            text1.setText(CustomerNameList.get(position));
            text2.setText(OrderIdlist.get(position));
            text3.setText(Productnamelist.get(position));
            text4.setText(StatusList.get(position));
            text5.setText(PackList.get(position));
            text6.setText(QtyList.get(position));
            text7.setText(DateList.get(position));
            text8.setText(modelist.get(position));
            text9.setText(numberlist.get(position));
            text10.setText(colorlist.get(position));
            text11.setText(discountlist.get(position));
        }

        return convertView;
    }

}
