package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


//import com.brainmagic.adherebond.R;

import com.brainmagic.adherebond.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by system01 on 2/12/2017.
 */

public class SalesHistoryAdapter extends ArrayAdapter<String> {

    private  Context context;
    private ArrayList<String> CustomerNameList,DateList,InvoiceList,AmountList;

    public SalesHistoryAdapter(Context context, ArrayList<String> CustomerNameList, ArrayList<String> DateList, ArrayList<String> InvoiceList, ArrayList<String> AmountList) {
        super(context, R.layout.saleshistory_adapter, CustomerNameList);
        this.context = context;
        this.CustomerNameList= CustomerNameList;
        this.DateList  = DateList;
       // this.Productnamelist = Productnamelist;
        this.InvoiceList = InvoiceList;
        this.AmountList = AmountList;


    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null)
        {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.saleshistory_adapter, parent, false);

            TextView text1 = (TextView) convertView.findViewById(R.id.customer_name);
            TextView text = (TextView) convertView.findViewById(R.id.date);
           // TextView text2 = (TextView) convertView.findViewById(R.id.proname);
            TextView text3 = (TextView) convertView.findViewById(R.id.invoice_no);
            TextView text4 = (TextView) convertView.findViewById(R.id.amount);
            //TextView excessDate = (TextView) convertView.findViewById(R.id.excess_date);
            //TextView toatalDate = (TextView) convertView.findViewById(R.id.days);

            text1.setText(CustomerNameList.get(position));
            text.setText(changedate_format(String.format("%s",  (DateList.get(position)))));
            //text2.setText(Productnamelist.get(position));
            text3.setText(InvoiceList.get(position));
            text4.setText(AmountList.get(position));

        }

        return convertView;
    }
    private String changedate_format(String s) {
        try {
            if(!s.equals("") && !s.equals("null")) {

                s.substring(0, Math.min(s.length(), 10));
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                DateFormat ndf = new SimpleDateFormat("dd-MM-yyyy");
                Date startDate;
                startDate = df.parse(s);
                s = ndf.format(startDate);
                //System.out.println(newDateString);
            }
            else
                return s = "";
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return s;
    }

}
