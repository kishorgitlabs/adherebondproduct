package adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

//import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by system01 on 12/30/2016.
 */

public class ImageAdapter extends BaseAdapter {

    private Context mContext;
    ArrayList<String> categories;
    ArrayList<String> imagelist;
    private LayoutInflater layoutInflator;

    public ImageAdapter(Context context, ArrayList<String> categories, ArrayList<String> imagelist) {
        //mContext = c;
        this.categories = categories;
        this.imagelist = imagelist;
        this.mContext = context;

    }

    public int getCount() {
        return categories.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a news ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        convertView = null;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list, parent, false);
            TextView grid_text = (TextView) convertView.findViewById(R.id.grid_text);
            ImageView grid_image = (ImageView) convertView.findViewById(R.id.grid_image);
            grid_text.setText(categories.get(position));
            //grid_image.setLayoutParams(news GridView.LayoutParams(85, 85));
            grid_image.setScaleType(ImageView.ScaleType.CENTER_CROP);
            grid_image.setPadding(8, 8, 8, 8);
            Picasso.with(mContext)
                    .load("file:///android_asset/products/" + imagelist.get(position))
                    .error(R.drawable.noimage)
                    .resize(75, 75)
                    .into(grid_image);
        } /*else {
                imageView = (ImageView) convertView;
            }*/


        return convertView;
    }

    // references to our images
        /*private Integer[] mThumbIds = {
                R.drawable.sample_2
        };*/
}