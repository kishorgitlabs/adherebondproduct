package adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainmagic.adherebond.R;

import java.util.ArrayList;
import java.util.List;

import api.models.DataItem;

public class ExpenseViewAdapter extends ArrayAdapter {
    private Context mContext;
    List<DataItem> categories;
    public ExpenseViewAdapter(@NonNull Context context, List<DataItem> resource) {
        super(context, R.layout.expenseview);
        this.mContext=context;
        this.categories=resource;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.expenseview,parent,false);

        TextView sno=(TextView)view.findViewById(R.id.sno);
        TextView mode=(TextView)view.findViewById(R.id.mode);
        TextView ticamt=(TextView)view.findViewById(R.id.ticamt);
        TextView foodamt=(TextView)view.findViewById(R.id.foodamt);
        TextView openkm=(TextView)view.findViewById(R.id.openkm);
        TextView closekm=(TextView)view.findViewById(R.id.closekm);
        TextView fuel=(TextView)view.findViewById(R.id.fuel);
        TextView otheramt=(TextView)view.findViewById(R.id.otheramt);
        TextView date=(TextView)view.findViewById(R.id.date);
        TextView total=(TextView)view.findViewById(R.id.total);
        TextView bike=(TextView)view.findViewById(R.id.bike);

        if (categories.get(position).getMode().equals("Bus")){
            mode.setText(categories.get(position).getMode());

        }
        else{
            mode.setText("-");
        }

        if (categories.get(position).getMode().equals("Bike")){
            bike.setText(categories.get(position).getMode());

        }
        else{
            bike.setText("-");
        }


        if (categories.get(position).getTicketamt().equals("")){
            ticamt.setText("-");
        }
        else{
            ticamt.setText(categories.get(position).getTicketamt());
        }

        if (categories.get(position).getFoodamt().equals("")){
            foodamt.setText("-");
        }
        else{
            foodamt.setText(categories.get(position).getFoodamt());
        }

        if (categories.get(position).getOpenkm().equals("")){
            openkm.setText("-");
        }
        else{
            openkm.setText(categories.get(position).getOpenkm());
        }

        if (categories.get(position).getClosekm().equals("")){
            closekm.setText("-");
        }
        else{
            closekm.setText(categories.get(position).getClosekm());
        }

        if (categories.get(position).getFuelamt().equals("")){
            fuel.setText("-");
        }
        else{
            fuel.setText(categories.get(position).getFuelamt());
        }

        if (categories.get(position).getOtheramt().equals("")){
            otheramt.setText("-");
        }
        else{
            otheramt.setText(categories.get(position).getOtheramt());
        }

        if (categories.get(position).getOtheramt().equals("")){
            otheramt.setText("-");
        }
        else{
            otheramt.setText(categories.get(position).getOtheramt());
        }
        if (categories.get(position).getTotamt().equals("")){
            total.setText("-");
        }
        else{
            total.setText(categories.get(position).getTotamt());
        }

        sno.setText(position+1+"");
        String[] dates=categories.get(position).getInsertdate().split("T");
        date.setText(dates[0]);

        return view;
    }

    @Override
    public int getCount() {
        return  categories.size();
    }
}

