package email;

import java.util.Date;
import java.util.Properties;

import javax.activation.CommandMap;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.activation.MailcapCommandMap;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Mail
        extends Authenticator {
    private boolean _auth = true;
    private String _body = "";
    private boolean _debuggable = false;
    private String _from = "";
    private String _host = "smtp.gmail.com";
    private Multipart _multipart = new MimeMultipart();
    private String _pass = "";
    private String _port = "587";
    private String _subject = "";
    private String[] _to;
    private String[] _cc= {""};
    private String _user = "";
    private String _sport ="587";
    private String _replyto;

    public Mail()
    {
        MailcapCommandMap localMailcapCommandMap = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
        localMailcapCommandMap.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
        localMailcapCommandMap.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
        localMailcapCommandMap.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
        localMailcapCommandMap.addMailcap("multipart;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
        localMailcapCommandMap.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822");
        CommandMap.setDefaultCommandMap(localMailcapCommandMap);
    }

    public Mail(String paramString1, String paramString2)
    {
        this();
        this._user = paramString1;
        this._pass = paramString2;
    }

    private Properties _setProperties() {
        Properties localProperties = new Properties();
        localProperties.put("mail.smtp.host", this._host);
        localProperties.put("mail.smtp.starttls.enable", "true");
        if (this._debuggable) {
            localProperties.put("mail.debug", "true");
        }
        if (this._auth) {
            localProperties.put("mail.smtp.auth", "true");
        }
        localProperties.put("mail.smtp.port", this._port);
        localProperties.put("mail.smtp.socketFactory.port", this._sport);
        return localProperties;
    }

    public void addAttachment(String paramString)
            throws Exception {
        MimeBodyPart localMimeBodyPart = new MimeBodyPart();
        localMimeBodyPart.setDataHandler(new DataHandler(new FileDataSource(paramString)));
        localMimeBodyPart.setFileName(paramString);
        this._multipart.addBodyPart(localMimeBodyPart);
    }

    public String getBody() {
        return this._body;
    }

    public void setBody(String paramString) {
        this._body = paramString;
    }
    public String get_replyto() {
        return this._replyto;
    }

    public void set_replyto(String _replyto) {
        this._replyto = _replyto;
    }

    public PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(this._user, this._pass);
    }

    public boolean send() throws Exception {
        Properties props = _setProperties();

        if (!_user.equals("") && !_pass.equals("") && _to.length > 0 && !_from.equals("") && !_subject.equals("") && !_body.equals("")) {
            Session session = Session.getInstance(props, this);

            MimeMessage msg = new MimeMessage(session);


            msg.setReplyTo(new javax.mail.Address[]
                    {
                            new javax.mail.internet.InternetAddress(_replyto)
                    });
            msg.setFrom(new InternetAddress(_from));

            InternetAddress[] addressTo = new InternetAddress[_to.length];
            for (int i = 0; i < _to.length; i++)
            {
                addressTo[i] = new InternetAddress(_to[i]);
            }
            msg.setRecipients(MimeMessage.RecipientType.TO, addressTo);
            for (String a_cc : _cc) {
                if (!a_cc.equals("")) {
                    InternetAddress[] addressCC = new InternetAddress[_cc.length];
                    for (int i = 0; i < _cc.length; i++) {
                        addressCC[i] = new InternetAddress(_cc[i]);
                    }
                    msg.setRecipients(MimeMessage.RecipientType.CC, addressCC);
                }
            }



            msg.setSubject(_subject);
            msg.setSentDate(new Date());

            // setup message body
            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setText(_body);
            _multipart.addBodyPart(messageBodyPart);

            // Put parts in message
            msg.setContent(_multipart);

            // send email
            Transport.send(msg);

            return true;
        } else {
            return false;
        }
    }

    public void setFrom(String paramString) {
        this._from = paramString;
    }

    public void setSubject(String paramString) {
        this._subject = paramString;
    }

    public void setTo(String[] paramArrayOfString) {
        this._to = paramArrayOfString;
    }
    public void setCC(String[] paramArrayOfString) {
        this._cc = paramArrayOfString;
    }
}
