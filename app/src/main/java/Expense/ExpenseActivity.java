package Expense;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;

import com.brainmagic.adherebond.ProjectActivity;
import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.Welcome_Activity;

import java.util.Calendar;

import Logout.logout;
import about.About_Activity;
import alertbox.Alertbox;
import alertbox.PasswordChangeAlert;
import api.models.Expense.ExpesneResponse;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import contact.Contact_Activity;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import product.Product_Activity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import search.Search_Activity;

public class ExpenseActivity extends AppCompatActivity {

    private NetworkConnection network = new NetworkConnection(ExpenseActivity.this);
    private Alertbox alert = new Alertbox(ExpenseActivity.this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private PopupWindow mPopupWindow;
    ImageView home, back, menu;
CheckBox bus,bike;
EditText ticketamount,openkm,closekm,fuelamount,foodexpense,otherexpense,remarks,tarveldate;
LinearLayout ticketlayout,openlayout,closelayput,fuellayout;
Calendar myCalendar;
Button submit;
int mDay;
String date;
    int mMonth;
    int mYear;
    Alertbox box = new Alertbox(ExpenseActivity.this);
    String name,mobile,usertype,mode,tickam,foodamt,otheramt,open,closkm,fuel,remar,travel;
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expense);

        submit=findViewById(R.id.submit);
        myshare = getSharedPreferences("Registration", Context.MODE_PRIVATE);
        editor = myshare.edit();
        back = (ImageView) findViewById(R.id.back);
        home = (ImageView) findViewById(R.id.home);
        menu=(ImageView)findViewById(R.id.menubar);
        bus=findViewById(R.id.bus);
        bike=findViewById(R.id.bike);
        fuellayout=findViewById(R.id.fuellayout);
        ticketamount=findViewById(R.id.ticketamount);
        openkm=findViewById(R.id.openkm);
        closekm=findViewById(R.id.closekm);

        name=myshare.getString("name","");
        mobile=myshare.getString("mobile","");
        usertype=myshare.getString("Usertype","");
        editor.commit();
        tarveldate=findViewById(R.id.tarveldate);
        ticketlayout=findViewById(R.id.ticketlayout);
        openlayout=findViewById(R.id.openlayout);
        closelayput=findViewById(R.id.closelayput);
        fuelamount=findViewById(R.id.fuelamount);
        foodexpense=findViewById(R.id.foodexpense);
        otherexpense=findViewById(R.id.otherexpense);
        remarks=findViewById(R.id.remarks);

        tarveldate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate = Calendar.getInstance();

                mDay   = mcurrentDate.get(Calendar.DAY_OF_MONTH);
                mMonth = mcurrentDate.get(Calendar.MONTH);
                mYear  = mcurrentDate.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(ExpenseActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month = month + 1;
                        date = month + "/" + dayOfMonth + "/" + year;
                        tarveldate.setText(date);
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(ExpenseActivity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(ExpenseActivity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }

        });

        menu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                final PopupMenu popupMenu = new PopupMenu(ExpenseActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub
                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(ExpenseActivity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(ExpenseActivity.this, Product_Activity.class);
                                startActivity(product);
                                return true;

                            case R.id.searchmenu:
                                Intent search = new Intent(ExpenseActivity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(ExpenseActivity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.watsnew:
                                Intent contact1 = new Intent(ExpenseActivity.this, ProjectActivity.class);
                                startActivity(contact1);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(ExpenseActivity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.expense:
                                PasswordChangeAlert alert = new PasswordChangeAlert(ExpenseActivity.this);// new saleslogin().execute();
                                alert.showLoginbox();
                                return true;

                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {

                                    Intent b = new Intent(ExpenseActivity.this, Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));

                                    Intent b = new Intent(ExpenseActivity.this, com.brainmagic.adherebond.Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(ExpenseActivity.this).log_out();

                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_login_inner);
                //Whatsnew_Activity.super.onPrepareOptionsMenu(popupMenu);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NetworkConnection network = new NetworkConnection(ExpenseActivity.this);
                if (network.CheckInternet()) {
                    // new verifylogin().execute();
                    //askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, WRITE_EXST);
                    tickam=ticketamount.getText().toString();
                    foodamt=foodexpense.getText().toString();
                    otheramt=otherexpense.getText().toString();
                    open=openkm.getText().toString();
                    closkm=closekm.getText().toString();
                    fuel=fuelamount.getText().toString();
                    remar=remarks.getText().toString();
                    travel=tarveldate.getText().toString();
                    Log.e("travel",travel);

                    postDetails();
                } else {
                    Alertbox alert = new Alertbox(ExpenseActivity.this);
                    alert.showAlertbox("Kindly check your Internet Connection");
                }

            }
        });

    }

  public  void  postDetails(){
      try {

          final ProgressDialog loading = ProgressDialog.show(ExpenseActivity.this, getResources().getString(R.string.app_launcher_name), "Loading...", false, false);
          APIService service = RetroClient.getApiService();
          Log.e("travels",travel);
          Call<ExpesneResponse> call = service.postExpense(name,mobile,usertype,mode,tickam,foodamt,otheramt,open,closkm,fuel,remar,travel);


call.enqueue(new Callback<ExpesneResponse>() {
    @Override
    public void onResponse(Call<ExpesneResponse> call, Response<ExpesneResponse> response) {
        try {
            loading.dismiss();
            if (response.body().getResult().equals("Success")) {

     box.showAlertboxwithback("Travel data inserted successfully");
            }

            else
            {
                box.showAlertbox(getResources().getString(R.string.server_error));
            }

        } catch (Exception e) {
            e.printStackTrace();
            loading.dismiss();
            box.showAlertbox(getResources().getString(R.string.server_error));

        }
    }

    @Override
    public void onFailure(Call<ExpesneResponse> call, Throwable t) {
        loading.dismiss();
        t.printStackTrace();
        box.showAlertbox(getResources().getString(R.string.server_error));

    }
});



      } catch (Exception e) {
          e.printStackTrace();
      }
  }

    public void onCheckboxClicked(View view) {
        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();

        // Check which checkbox was clicked
        switch(view.getId()) {
            case R.id.bus:
               if (checked){
                 mode=((CheckBox) view).getText().toString();

                   ticketlayout.setVisibility(View.VISIBLE);
               }
else{
                   ticketlayout.setVisibility(View.GONE);
               }
                break;
            case R.id.bike:
                if (checked){
                    mode=((CheckBox) view).getText().toString();
                    openlayout.setVisibility(View.VISIBLE);
                    closelayput.setVisibility(View.VISIBLE);
                    fuellayout.setVisibility(View.VISIBLE);
                }
                else{
                    openlayout.setVisibility(View.GONE);
                    closelayput.setVisibility(View.GONE);
                    fuellayout.setVisibility(View.GONE);
                }
                break;

        }
    }
}