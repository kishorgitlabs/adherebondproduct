package Logout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;


import SQL_Fields.SQLITE;
import SharedPreference.Shared;
import database.DBHelpersync;
import home.Home_Activity;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Systems02 on 30-Oct-17.
 */

public class
logout {
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private Context context;

    public logout(Context context) {
        this.context = context;
    }


    public void log_out()
    {
        myshare = context.getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();

        if(myshare.getBoolean(Shared.K_Login,false))
        editor.putBoolean("islogin", false);
        editor.apply();
        DBHelpersync dbHelper = new DBHelpersync(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        try {
            db.execSQL("DROP TABLE IF EXISTS "+ SQLITE.TABLE_Additems);
            db.execSQL("DROP TABLE  IF EXISTS "+ SQLITE.TABLE_Create_Profile);
            db.execSQL("DROP TABLE  IF EXISTS "+ SQLITE.TABLE_NewPartsOrder);
            db.close();
        }
        catch (Exception e)
        {
            db.close();
            Log.v("Deletetable","Delete");
        }

        context.startActivity(new Intent(context, Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        Toast.makeText(context, "Logged out successfully ", Toast.LENGTH_SHORT).show();



    }





}
