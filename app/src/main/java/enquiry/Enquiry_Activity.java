package enquiry;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.Login_Activity;
import com.brainmagic.adherebond.PaymentingActivity;
import com.brainmagic.adherebond.Payments_Activity;
import com.brainmagic.adherebond.ProjectActivity;
//import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.Welcome_Activity;

import Logout.logout;
import about.About_Activity;
import alertbox.Alertbox;
import alertbox.PasswordChangeAlert;
import api.models.enquiry.Enqiry;
import api.models.enquiry.EnqiryData;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import contact.Contact_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import offers.Offers_Activity;
import product.Product_Activity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import search.Search_Activity;

public class Enquiry_Activity extends AppCompatActivity {
    private EditText name,contactno,description,address,email,job_desc;
    private Button submit;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String salesID,SalesName;
    private Alertbox box = new Alertbox(Enquiry_Activity.this);
    private Enqiry Data;
    private ImageView menu,back,home;


    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enquiry_);
        myshare = getSharedPreferences("Registration", Enquiry_Activity.this.MODE_PRIVATE);
        editor = myshare.edit();
        salesID = myshare.getString("salesID", "");
        SalesName = myshare.getString("name", "");
        name=(EditText)findViewById(R.id.req_name);
        contactno=(EditText)findViewById(R.id.contact_number);
        email=(EditText)findViewById(R.id.email);
        address=(EditText)findViewById(R.id.address);
        job_desc=(EditText)findViewById(R.id.job_desc);
        submit=(Button) findViewById(R.id.submit);
        menu=(ImageView)findViewById(R.id.menubar);
        back=(ImageView)findViewById(R.id.back);
        home=(ImageView)findViewById(R.id.home);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(name.getText().toString().equals("")){

                    name.setError("Enter Name");

                }else if(contactno.getText().toString().equals("")){
                    contactno.setError("Enter Contact number");


                }else if(!isValidEmail(email.getText().toString().trim())){

                    email.setError(" Invalid email");

                }else{
                    checkinternet();
                }
            }
        });

        TextView mTitle_text = findViewById(R.id.title_text);
        mTitle_text.setText("Enquiry");


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(Enquiry_Activity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                final PopupMenu popupMenu = new PopupMenu(Enquiry_Activity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }

                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub




                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(Enquiry_Activity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.expense:
                                PasswordChangeAlert alert = new PasswordChangeAlert(Enquiry_Activity.this);// new saleslogin().execute();
                                alert.showLoginbox();
                                return true;

                            case R.id.productmenu:
                                Intent product = new Intent(Enquiry_Activity.this, Product_Activity.class);
                                startActivity(product);
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(Enquiry_Activity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(Enquiry_Activity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.watsnew:
                                Intent contact1 = new Intent(Enquiry_Activity.this, ProjectActivity.class);
                                startActivity(contact1);
                                return true;
                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {

                                    Intent b = new Intent(Enquiry_Activity.this, Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));

                                    Intent b = new Intent(Enquiry_Activity.this, Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(Enquiry_Activity.this).log_out();
                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_offer);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });
    }



    private void checkinternet() {
        NetworkConnection isnet = new NetworkConnection(Enquiry_Activity.this);
        if (isnet.CheckInternet()) {
            GetServiceEngineerComplaints();
        } else {
            box.showAlertbox(getResources().getString(R.string.no_internet));
        }
    }

    private void GetServiceEngineerComplaints() {
        try {
            final ProgressDialog loading = ProgressDialog.show(Enquiry_Activity.this, "Adhere bonds", "Loading...", false, false);
            APIService service = RetroClient.getApiService();
            Call<EnqiryData> call = service.enquiry(name.getText().toString(),
                    contactno.getText().toString(),
                    job_desc.getText().toString(),
                    address.getText().toString(),
                    email.getText().toString());
            call.enqueue(new Callback<EnqiryData>() {
                @Override
                public void onResponse(Call<EnqiryData> call, Response<EnqiryData> response) {
                    try {
                        loading.dismiss();

                        if (response.body().getResult().equals("Success")) {
                            Data = response.body().getData();
                                SuccessRegister(response.body().getData());


                        } else if (response.body().getResult().equals("NotSuccess")) {
                            box.showAlertbox(getResources().getString(R.string.server_error));
                        }else if(response.body().getResult().equals("NoData")){
                            box.showAlertbox(getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.v("GetServiceComplaints", e.getMessage());
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }


                @Override
                public void onFailure(Call<EnqiryData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void SuccessRegister(Enqiry data) {
        final AlertDialog alertDialogbox = new AlertDialog.Builder(
                Enquiry_Activity.this).create();

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertbox, null);
        alertDialogbox.setView(dialogView);

        TextView log = (TextView) dialogView.findViewById(R.id.textView1);
        Button okay = (Button) dialogView.findViewById(R.id.okay);
        log.setText("Thank You For your Enquiry.Enquiry Send Successfully");
        okay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent go = new Intent(Enquiry_Activity.this, Home_Activity.class);
                go.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(go);
                alertDialogbox.dismiss();
            }
        });
        alertDialogbox.show();
    }
}

