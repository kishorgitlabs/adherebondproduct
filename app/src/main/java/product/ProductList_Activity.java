package product;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.Login_Activity;
import com.brainmagic.adherebond.ProjectActivity;
//import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.Welcome_Activity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import Logout.logout;
import about.About_Activity;
import adapter.ImageAdapter;
import alertbox.Alertbox;
import alertbox.LoginAlert;
import alertbox.PasswordChangeAlert;
import api.models.project.Project;
import contact.Contact_Activity;
import database.DBHelper;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import offers.Offers_Activity;
import search.Search_Activity;
import whatsnew.Whatsnew_Activity;

public class ProductList_Activity extends AppCompatActivity {


    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    String usertype;
    NetworkConnection network = new NetworkConnection(ProductList_Activity.this);
    Alertbox alert = new Alertbox(ProductList_Activity.this);
    Connection connection;
    Statement statement;
    ResultSet resultSet;
    GridView grid_view;
    ArrayList<String> catagoryList,imageList,prioritylist;
    ProgressDialog progressDialog;
    private ListView listview;
    private ArrayList<String> IDList;
    TextView title;
    String id,s,Mprod_id,s2;
    ImageView back,home,menu;
    private String productquery;
    private String subid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list_);

        myshare = getSharedPreferences("Registration",ProductList_Activity.this.MODE_PRIVATE);
        editor = myshare.edit();
        grid_view = (GridView) findViewById(R.id.gridView);
        catagoryList = new ArrayList<String>();
        imageList = new ArrayList<String>();
        prioritylist= new ArrayList<String>();
        IDList = new ArrayList<String>();
        //listview = (ListView) findViewById(R.id.items);
        title=(TextView)findViewById(R.id.title_text);

        s = getIntent().getStringExtra("Category");
        s2 = getIntent().getStringExtra("Category");
        title.setText(capitalizeString(s));
        id=getIntent().getStringExtra("ID");
        //subid=getIntent().getStringExtra("SUBIDList");
        Mprod_id=getIntent().getStringExtra("MainID");

        if(s.equals(""))
        {
            productquery = "select distinct * from Product_details where MProd_ID='" + id + "'";
            Log.v("Product Query",productquery);

            //productquery = "select distinct Product_details.Prod_Name,Product_details.Prod_ID,Product_Sub_Category.MProdsub_Img from Product_details,Product_Sub_Category where Product_Sub_Category.MProd_ID='"+subid+"' order by Prod_Priority asc";
            title.setText(s2);
            new GetCatagory().execute(productquery);
        }
        else
        {
           /* productquery = "select distinct \n" +
                    "Product_details.Prod_Name,\n" +
                    "Product_details.Prodshort_desc,\n" +
                    "Product_details.Prodlong_desc,\n" +
                    "Product_details.Prodexc_img,\n" +
                    "Product_details.specification,\n" +
                    "Product_details.Prodvid_url,\n" +
                    "Product_details.Prod_brocher,\n" +
                    "Product_details.Prod_ID,\n" +
                    "Product_Sub_Category.MProdsub_ID,\n" +
                    "Product_Sub_Category.MProdsub_Img from Product_details,Product_Sub_Category where Product_Sub_Category.MProdsub_ID='" + id + "'";*/

            productquery = "select distinct * from Product_details where MProd_ID='" + id + "'";
            Log.v("Product Query",productquery);

           // productquery = "select distinct Prod_Name,Prodexc_img,Prod_ID from Product_details where MProd_ID='"+id+"' order by Prod_Priority asc";
            //productquery = "select distinct Product_details.Prod_Name,Product_details.Prod_ID,Product_Sub_Category.MProdsub_Img from Product_details,Product_Sub_Category where Product_Sub_Category.MProd_ID='"+subid+"' order by Prod_Priority asc";
            new GetCatagory().execute(productquery);

        }

        grid_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent a=new Intent(ProductList_Activity.this,ProductDetails_Activity.class);
                a.putExtra("ID",IDList.get(i));
                a.putExtra("subCata",s);
                startActivity(a);
            }
        });


        back=(ImageView)findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                onBackPressed();

            }
        });

        home=(ImageView)findViewById(R.id.home);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(ProductList_Activity.this, Home_Activity.class);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(mainIntent);
                finish();
            }
        });

        menu=(ImageView)findViewById(R.id.menubar);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(ProductList_Activity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub


                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(ProductList_Activity.this, About_Activity.class);
                                startActivity(about);
                                return true;

                            case R.id.expense:
                                PasswordChangeAlert alerts = new PasswordChangeAlert(ProductList_Activity.this);// new saleslogin().execute();
                                alerts.showLoginbox();
                                return true;

                            case R.id.searchmenu:
                                Intent search = new Intent(ProductList_Activity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(ProductList_Activity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(ProductList_Activity.this, ProjectActivity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.watsnew:
                                if (network.CheckInternet()) {
                                    Intent f = new Intent(ProductList_Activity.this, Whatsnew_Activity.class);
                                    startActivity(f);}
                                else{
                                    Alertbox alert = new Alertbox(ProductList_Activity.this);
                                    alert.showAlertbox("Kindly check your Internet Connection");
                                }
                                return true;
                            case R.id.offers:
                                usertype=myshare.getString("Usertype","");
                                if(usertype.equals("Cera Employee"))
                                {
                                    if (!myshare.getBoolean("islogin",false))
                                    {
                                        CheckInternet();
                                        Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                        Log.v("is  Login ",Boolean.toString(myshare.getBoolean("alertlogin",false)));
                                    }
                                    else
                                    {
                                        if (network.CheckInternet()) {
                                            Intent f = new Intent(ProductList_Activity.this, Offers_Activity.class);
                                            startActivity(f);
                                        }
                                        else {

                                            alert.showAlertbox("Kindly check your Internet Connection");
                                        }

                                    }
                                }
                                else
                                {
                                    Intent f = new Intent(ProductList_Activity.this, Offers_Activity.class);
                                    startActivity(f);
                                }return true;




                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {
                                    //menu.findViewById(R.id.loginmenu).setVisibility(View.VISIBLE);
                                    Intent b = new Intent(ProductList_Activity.this, Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {

                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                    ///menu.findViewById(R.id.loginmenu).setVisibility(View.VISIBLE);
                                    Intent b = new Intent(ProductList_Activity.this, Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(ProductList_Activity.this).log_out();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popupmenu_products);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();
            }
        });



    }
    @Override
    public void  onBackPressed() {


        if(s.equals(""))
        {
            Intent gomain = new Intent(ProductList_Activity.this,Product_Activity.class);
            gomain.putExtra("a","");
            //  gomain.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            startActivity(gomain);
        }
        else
        {
            super.onBackPressed();
        }

    }

    public static String capitalizeString(String string )
    {
        try

        {
            char[] chars = string.toLowerCase().toCharArray();
            boolean found = false;
            for (int i = 0; i < chars.length; i++) {
                if (!found && Character.isLetter(chars[i])) {
                    chars[i] = Character.toUpperCase(chars[i]);
                    found = true;
                } else if (Character.isWhitespace(chars[i]) || chars[i] == '.' || chars[i] == '&') { // You can add other chars here
                    found = false;
                }
            }
            return String.valueOf(chars);
        }catch (Exception e)
        {
            e.printStackTrace();
            return "error";
        }
    }
    class  GetCatagory extends AsyncTask<String,Void,String>
    {

        private SQLiteDatabase db;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ProductList_Activity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            try

            {
                DBHelper dbhelper = new DBHelper(ProductList_Activity.this);
                db = dbhelper.readDataBase();

                Cursor cursor = db.rawQuery(strings[0], null);
                Log.v("strings[0]",strings[0]);

                if (cursor.moveToFirst())
                {
                    do
                    {

                        catagoryList.add(cursor.getString(cursor.getColumnIndex("Prod_Name")));
                        imageList.add(cursor.getString(cursor.getColumnIndex("subImage")));
                        IDList.add(cursor.getString(cursor.getColumnIndex("Prod_ID")));

                    }
                    while (cursor.moveToNext());
                    cursor.close();
                    return "success";
                }
                else
                {
                    cursor.close();
                    return "empty";
                }




            }
            catch (Exception e)
            {
                e.printStackTrace();
                return "error";
            }

        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if(s.equals("success"))
            {
                grid_view.setAdapter(new ImageAdapter(ProductList_Activity.this, catagoryList, imageList));;
            }
            else if(s.equals("empty"))
            {

            }
            else
            {

            }


        }
    }
    private void CheckInternet() {
        if (network.CheckInternet()) {
            LoginAlert alert= new LoginAlert(ProductList_Activity.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {

            alert.showAlertbox("Kindly check your Internet Connection");
        }
    }

}
