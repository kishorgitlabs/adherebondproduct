package product;

import android.Manifest;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.JavaScriptInterface;
import com.brainmagic.adherebond.Login_Activity;
import com.brainmagic.adherebond.ProjectActivity;
//import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.Welcome_Activity;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;

import Logout.logout;
import about.About_Activity;
import alertbox.Alertbox;
import alertbox.LoginAlert;
import alertbox.PasswordChangeAlert;
import contact.Contact_Activity;
import database.DBHelper;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import im.delight.android.webview.AdvancedWebView;
import network.NetworkConnection;
import offers.Offers_Activity;
import search.Search_Activity;
import uk.co.senab.photoview.PhotoView;
import whatsnew.Whatsnew_Activity;

import static android.os.Environment.DIRECTORY_DOWNLOADS;

public class ProductDetails_Activity extends AppCompatActivity {


    private static final int PERMISSION_REQUEST = 100, PERMISSION_REQUEST2 = 110;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    String usertype;
    NetworkConnection network = new NetworkConnection(ProductDetails_Activity.this);
    Alertbox alert = new Alertbox(ProductDetails_Activity.this);
    private RelativeLayout mRelativeLayout;
    private View customView;
    ImageView productimage, back, home, menu;
    PhotoView photoView;
    /*holder.mAttacher = news PhotoViewAttacher(holder.image);
    holder.mAttacher.setScaleType(ImageView.ScaleType.CENTER_CROP);
    */
    TextView prod_name, short_desc, long_desc, title, product_name;
    ProgressDialog progressDialog;
    private PopupWindow mPopupWindow;
    ImageButton pdf, Share;
    String id, string_name, string_image, string_long, string_short, string_high, string_you, string_pdf,subCata;
    private static Context appContext;
    private AdvancedWebView browser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details_);

        myshare = getSharedPreferences("Registration", ProductDetails_Activity.this.MODE_PRIVATE);
        editor = myshare.edit();
        appContext = this;
        productimage = (ImageView) findViewById(R.id.productimage);
        title=(TextView)findViewById(R.id.title_text);
        prod_name = (TextView) findViewById(R.id.prod_name);
        product_name = (TextView) findViewById(R.id.product_name);
        mRelativeLayout = (RelativeLayout) findViewById(R.id.activity_product_details);
        short_desc = (TextView) findViewById(R.id.short_desc);
        long_desc = (TextView) findViewById(R.id.long_desc);
        browser = (AdvancedWebView) findViewById(R.id.webview);
        browser.addJavascriptInterface(new JavaScriptInterface(), "android");
        // browser.setWebViewClient(new Callback());
        browser.getSettings().setJavaScriptEnabled(true);
        pdf = (ImageButton) findViewById(R.id.pdf);
        Share = (ImageButton) findViewById(R.id.share);
        id = getIntent().getStringExtra("ID");
        subCata = getIntent().getStringExtra("subCata");
        title.setText(subCata);
        new getdetails().execute();

        // Initialize a news instance of LayoutInflater service
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        // Inflate the custom layout/view
        customView = inflater.inflate(R.layout.pop_photo, null);

        // Initialize a news instance of popup window
        mPopupWindow = new PopupWindow(customView, ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);

        // Set an elevation value for popup window
        // Call requires API level 21
        if (Build.VERSION.SDK_INT >= 21) {
            mPopupWindow.setElevation(5.0f);
        }

        // Get a reference for the custom view close button
        ImageButton closeButton = (ImageButton) customView.findViewById(R.id.close);

        // Set a click listener for the popup window close button
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Dismiss the popup window
                mPopupWindow.dismiss();
            }
        });

        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        home=(ImageView)findViewById(R.id.home);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(ProductDetails_Activity.this, Home_Activity.class);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(mainIntent);
                finish();
            }
        });


        menu = (ImageView) findViewById(R.id.menubar);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(ProductDetails_Activity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub


                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(ProductDetails_Activity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.expense:
                                PasswordChangeAlert alerts = new PasswordChangeAlert(ProductDetails_Activity.this);// new saleslogin().execute();
                                alerts.showLoginbox();
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(ProductDetails_Activity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(ProductDetails_Activity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(ProductDetails_Activity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.watsnew:
                                if (network.CheckInternet()) {
                                    Intent f = new Intent(ProductDetails_Activity.this, ProjectActivity.class);
                                    startActivity(f);
                                } else {
                                    Alertbox alert = new Alertbox(ProductDetails_Activity.this);
                                    alert.showAlertbox("Kindly check your Internet Connection");
                                }
                                return true;
                            case R.id.offers:
                                usertype = myshare.getString("Usertype", "");
                                if (usertype.equals("Cera Employee")) {
                                    if (!myshare.getBoolean("islogin", false)) {
                                        CheckInternet();
                                        Log.v("is  Login ", Boolean.toString(myshare.getBoolean("islogin", false)));
                                        Log.v("is  Login ", Boolean.toString(myshare.getBoolean("alertlogin", false)));
                                    } else {
                                        if (network.CheckInternet()) {
                                            Intent f = new Intent(ProductDetails_Activity.this, Offers_Activity.class);
                                            startActivity(f);
                                        } else {

                                            alert.showAlertbox("Kindly check your Internet Connection");
                                        }

                                    }
                                } else {
                                    Intent f = new Intent(ProductDetails_Activity.this, Offers_Activity.class);
                                    startActivity(f);
                                }
                                return true;


                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin", false)) {
                                    //menu.findViewById(R.id.loginmenu).setVisibility(View.VISIBLE);
                                    Intent b = new Intent(ProductDetails_Activity.this, Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ", Boolean.toString(myshare.getBoolean("islogin", false)));
                                } else {

                                    Log.v("is  Login ", Boolean.toString(myshare.getBoolean("islogin", false)));
                                    ///menu.findViewById(R.id.loginmenu).setVisibility(View.VISIBLE);
                                    Intent b = new Intent(ProductDetails_Activity.this, Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(ProductDetails_Activity.this).log_out();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popupmenu_products);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin", false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();
            }
        });

        productimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Finally, show the popup window at the center location of root relative layout
                mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);
                photoView = (PhotoView) customView.findViewById(R.id.pop_photo);
                Picasso.with(ProductDetails_Activity.this).load("file:///android_asset/products/" + string_image)
                        .memoryPolicy(MemoryPolicy.NO_CACHE )
                        .networkPolicy(NetworkPolicy.NO_CACHE).into(photoView);
            }
        });


        pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Checknet();

            }
        });
        Share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Checkshare();

            }
        });


    }

    public static Context getContext() {
        return appContext;
    }


    @Override
    public void onBackPressed() {

        if (mPopupWindow.isShowing()) {
            mPopupWindow.dismiss();
        } else {
            super.onBackPressed();
        }


    }

    class getdetails extends AsyncTask<String, Void, String> {
        private String productquery;
        private SQLiteDatabase db;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ProductDetails_Activity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            try

            {
                DBHelper dbhelper = new DBHelper(ProductDetails_Activity.this);
                db = dbhelper.readDataBase();
                productquery = "select distinct \n" +
                        "PRoduct_details.Prod_Name,\n" +
                        "PRoduct_details.Prodshort_desc,\n" +
                        "PRoduct_details.Prodlong_desc,\n" +
                        "PRoduct_details.Prodexc_img,\n" +
                        "PRoduct_details.specification,\n" +
                        "PRoduct_details.Prodvid_url,\n" +
                        "PRoduct_details.Prod_brocher,\n" +
                        "Product_Sub_Category.MProdsub_ID,\n" +
                        "Product_Sub_Category.MProdsub_Img from PRoduct_details,Product_Sub_Category where Prod_ID='" + id + "' and  Product_Sub_Category.MProdsub_ID = PRoduct_details.MProdsub_ID";
                Log.v("Product Query",productquery);
                Cursor cursor = db.rawQuery(productquery, null);

                if (cursor.moveToFirst()) {
                    do {
                        string_image = cursor.getString(cursor.getColumnIndex("MProdsub_Img"));
                        string_name = cursor.getString(cursor.getColumnIndex("Prod_Name"));
                        string_short = cursor.getString(cursor.getColumnIndex("Prodshort_desc"));
                        string_long = cursor.getString(cursor.getColumnIndex("Prodlong_desc"));
                        string_high = cursor.getString(cursor.getColumnIndex("specification"));
                        string_you = cursor.getString(cursor.getColumnIndex("Prodvid_url"));
                        string_pdf = cursor.getString(cursor.getColumnIndex("Prod_brocher"));
                        db.close();
                    }
                    while (cursor.moveToNext());
                    cursor.close();
                    return "success";
                } else {
                    return "empty";
                }


            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }

        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if (s.equals("success")) {
                product_name.setText(string_name);
                prod_name.setText(string_name);
                short_desc.setText(string_short);
                long_desc.setText(string_long);

                final WebSettings webSettings = browser.getSettings();

                webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
                webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
                webSettings.setAppCacheEnabled(false);
                webSettings.setBlockNetworkImage(true);
                webSettings.setLoadsImagesAutomatically(true);
                webSettings.setGeolocationEnabled(false);
                webSettings.setNeedInitialFocus(false);
                webSettings.setSaveFormData(false);
                webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
                browser.setWebChromeClient(new WebChromeClient());
                browser.loadHtml(string_high, "text/html", null);

                  //browser.loadUrl("file:///android_asset/sample.html");


                Picasso.with(ProductDetails_Activity.this)
                        .load("file:///android_asset/products/" + string_image)
                        .memoryPolicy(MemoryPolicy.NO_CACHE )
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .error(R.drawable.noimage)
                        .resize(200, 200)
                        .into(productimage);
                get_pdf();

            } else if (s.equals("empty")) {

            }


        }
    }


    private void CheckNetwork() {
        NetworkConnection network = new NetworkConnection(ProductDetails_Activity.this);
        if (network.CheckInternet()) {
            play();

        } else {
            Alertbox alert = new Alertbox(ProductDetails_Activity.this);
            alert.showAlertbox("Kindly check your Internet Connection");
        }
    }

    private void Checkshare() {
        NetworkConnection network = new NetworkConnection(ProductDetails_Activity.this);
        if (network.CheckInternet()) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    if (shouldShowRequestPermissionRationale(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Snackbar.make(findViewById(R.id.activity_product_details), "You need to give permission", Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
                            @RequiresApi(api = Build.VERSION_CODES.M)
                            @Override
                            public void onClick(View v) {
                                requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST2);
                            }
                        }).show();
                    } else {
                        requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST2);
                    }
                } else {
                    startshare();
                }
            } else {
                startshare();

            }


            //   startshare();
        } else {
            Alertbox alert = new Alertbox(ProductDetails_Activity.this);
            alert.showAlertbox("Kindly check your Internet Connection");
        }
    }

    private void Checknet() {
        NetworkConnection network = new NetworkConnection(ProductDetails_Activity.this);
        if (network.CheckInternet()) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    if (shouldShowRequestPermissionRationale(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Snackbar.make(findViewById(R.id.activity_product_details), "You need to give permission", Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
                            @RequiresApi(api = Build.VERSION_CODES.M)
                            @Override
                            public void onClick(View v) {
                                requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST);
                            }
                        }).show();
                    } else {
                        requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST);
                    }
                } else {
                    startDownload();
                }
            } else {
                startDownload();

            }


        } else {
            Alertbox alert = new Alertbox(ProductDetails_Activity.this);
            alert.showAlertbox("Kindly check your Internet Connection");
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSION_REQUEST:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Snackbar.make(findViewById(R.id.activity_product_details), "Permission Granted",
                            Snackbar.LENGTH_LONG).show();
                    startDownload();

                } else {
                    Snackbar.make(findViewById(R.id.activity_product_details), "Permission denied",
                            Snackbar.LENGTH_LONG).show();

                }
            default:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Snackbar.make(findViewById(R.id.activity_product_details), "Permission Granted",
                            Snackbar.LENGTH_LONG).show();
                    sharedownload();

                } else {

                    Snackbar.make(findViewById(R.id.activity_product_details), "Permission denied",
                            Snackbar.LENGTH_LONG).show();
                }
        }
    }

    /*@RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Snackbar.make(findViewById(R.id.activity_product_details), "Permission Granted",
                    Snackbar.LENGTH_LONG).show();
            startDownload();

        } else {

            Snackbar.make(findViewById(R.id.activity_product_details), "Permission denied",
                    Snackbar.LENGTH_LONG).show();

        }
    }*/


    public void play() {
        String url = string_you;
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }


    public void startshare() {
        DownloadManager downloadmanager;
        //String pdfurl="http://ceracms.brainmagicllc.com/CERA/Pdf/Cera%20bond%2027.pdf";
        String pdfurl = string_pdf;
        Log.v("url", pdfurl);
        downloadmanager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        pdfurl = pdfurl.replace(" ", "%20");
        Uri uri = Uri.parse(pdfurl);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setDestinationInExternalPublicDir(DIRECTORY_DOWNLOADS, File.separator + string_pdf);
        downloadmanager.enqueue(request);
        sharedownload();
    }

    void sharedownload() {

        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS).getPath() + File.separator + string_pdf);
            Uri path = Uri.fromFile(file);
            Log.i("Fragment2", String.valueOf(path));
            Intent share = new Intent(Intent.ACTION_SEND);
            share.putExtra(Intent.EXTRA_STREAM, path);
            share.setType("application/pdf");
            startActivity(Intent.createChooser(share, "Share pdf"));
            /*/*File internalFile = getFileStreamPath(string_pdf);
            Uri internal = Uri.fromFile(internalFile);
            Log.i("Fragment", String.valueOf(internal));
            Intent i=new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS);
            startActivity(i);*/
            //File outputFile = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), string_pdf);
            /*File file = new File(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS).getPath() + File.separator + string_pdf);
            Uri path = Uri.fromFile(file);
            Log.i("Fragment2", String.valueOf(path));
            Intent share = new Intent(Intent.ACTION_SEND);
            share.putExtra(Intent.EXTRA_STREAM, path);
            share.setType("application/pdf");
            //share.setPackage("com.whatsapp");
            startActivity(Intent.createChooser(share, getResources().getText(R.string.send_to)));*/

        } catch (Exception e) {
            Log.v("Pdf Exception", e.getMessage());
        }
    }


    public void startDownload() {
        DownloadManager downloadmanager;
        //String pdfurl="http://cerachem.brainmagicllc.com/Pdf/Cera%20bond%2027.pdf";
        String pdfurl = string_pdf;
        Log.v("url", pdfurl);
        downloadmanager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        pdfurl = pdfurl.replace(" ", "%20");
        Uri uri = Uri.parse(pdfurl);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setDestinationInExternalPublicDir(DIRECTORY_DOWNLOADS, File.separator + string_pdf);
        downloadmanager.enqueue(request);
        showdownload();
    }

    void showdownload() {

        try {
            Intent i = new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS);
            startActivity(i);
        } catch (Exception e) {
            Log.v("Pdf Exception", e.getMessage());
        }
    }

    void get_pdf() {
        if (string_pdf.equals("")) {
            pdf.setVisibility(View.INVISIBLE);
            Share.setVisibility(View.INVISIBLE);
        } else {
            pdf.setVisibility(View.VISIBLE);
            Share.setVisibility(View.VISIBLE);
        }
    }

    private void CheckInternet() {
        if (network.CheckInternet()) {
            LoginAlert alert = new LoginAlert(ProductDetails_Activity.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {

            alert.showAlertbox("Kindly check your Internet Connection");
        }
    }


    public String generateHtmlData() {
        StringBuffer html;
        String[] tableContent = {"Name:Tamil:English:Maths:Science:Social Science", "Vinoth:93:96:100:98:99", "Kumar:94:99:100:98:98", "Raju:95:95:100:94:99", "Bala:96:99:100:98:99", "Franklin:94:99:100:98:98", "Jovey:93:96:100:98:99"};
        String[] tableArray = null;
        html = new StringBuffer("<html><head>" + getCSSForTable() + "</head>" + "<table class=\"TableStyle\">");
        for (String data : tableContent) {
            tableArray = data.split(":");
            html.append("<tr>");
            for (int i = 0; i < tableArray.length; i++) {
                html.append("<td>" + tableArray[i] + "</td>");
                Log.v("table array", tableArray[i]);
            }
            html.append("</tr>");
        }
        html.append("</tr>" + "</table>" + "<br/><br/><div align=\"center\"><<<< Scroll horizontally >>>></div><br/><div align=\"center\">Supports Pinch Zoom</div></html>");
        Log.v("html appended value", html.toString());
        return html.toString();
    }

    /**
     * Generate CSS styles
     *
     * @return
     */
    private String getCSSForTable() {
        String cssString;
        cssString = "<style>" +
                ".TableStyle {" +
                "margin:0px;padding:0px;" +
                "width:100%;" +
                "box-shadow: 10px 10px 5px #888888;" +
                "border:1px solid #07214f;" +
                "-moz-border-radius-bottomleft:0px;" +
                "-webkit-border-bottom-left-radius:0px;" +
                "border-bottom-left-radius:0px;" +
                "-moz-border-radius-bottomright:0px;" +
                "-webkit-border-bottom-right-radius:0px;" +
                "border-bottom-right-radius:0px;" +
                "-moz-border-radius-topright:0px;" +
                "-webkit-border-top-right-radius:0px;" +
                "border-top-right-radius:0px;" +
                "-moz-border-radius-topleft:0px;" +
                "-webkit-border-top-left-radius:0px;" +
                "border-top-left-radius:0px;" +
                "}.TableStyle table{" +
                "border-collapse: collapse;" +
                "border-spacing: 0;" +
                "width:100%;" +
                "height:100%;" +
                "margin:0px;padding:0px;" +
                "}.TableStyle tr:last-child td:last-child {" +
                "-moz-border-radius-bottomright:0px;" +
                "-webkit-border-bottom-right-radius:0px;" +
                "border-bottom-right-radius:0px;" +
                "}" +
                ".TableStyle table tr:first-child td:first-child {" +
                "-moz-border-radius-topleft:0px;" +
                "-webkit-border-top-left-radius:0px;" +
                "border-top-left-radius:0px;" +
                "}" +
                ".TableStyle table tr:first-child td:last-child {" +
                "-moz-border-radius-topright:0px;" +
                "-webkit-border-top-right-radius:0px;" +
                "border-top-right-radius:0px;" +
                "}.TableStyle tr:last-child td:first-child{" +
                "-moz-border-radius-bottomleft:0px;" +
                "-webkit-border-bottom-left-radius:0px;" +
                "border-bottom-left-radius:0px;" +
                "}.TableStyle tr:hover td{" +
                "}" +
                ".TableStyle tr:nth-child(odd){ background-color:#ffffff; }" +
                ".TableStyle tr:nth-child(even)    { background-color:#a6d3ed; }.TableStyle td{" +
                "vertical-align:middle;" +
                "border:1px solid #07214f;" +
                "border-width:0px 1px 1px 0px;" +
                "text-align:left;" +
                "padding:7px;" +
                "font-size:12px;" +
                "font-family:Arial;" +
                "font-weight:normal;" +
                "color:#020101;" +
                "}.TableStyle tr:last-child td{" +
                "border-width:0px 1px 0px 0px;" +
                "}.TableStyle tr td:last-child{" +
                "border-width:0px 0px 1px 0px;" +
                "}.TableStyle tr:last-child td:last-child{" +
                "border-width:0px 0px 0px 0px;" +
                "}" +
                ".TableStyle tr:first-child td{" +
                "background:-o-linear-gradient(bottom, #629edb 5%, #003f7f 100%);   background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #629edb), color-stop(1, #003f7f) );" +
                "background:-moz-linear-gradient( center top, #629edb 5%, #003f7f 100% );" +
                "filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=\"#629edb\", endColorstr=\"#003f7f\"); background: -o-linear-gradient(top,#629edb,003f7f);" +
                "background-color:#629edb;" +
                "border:0px solid #07214f;" +
                "text-align:center;" +
                "border-width:0px 0px 1px 1px;" +
                "font-size:15px;" +
                "font-family:Arial;" +
                "font-weight:bold;" +
                "color:#ffffff;" +
                "}" +
                ".TableStyle tr:first-child:hover td{" +
                "background:-o-linear-gradient(bottom, #629edb 5%, #003f7f 100%);   background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #629edb), color-stop(1, #003f7f) );" +
                "background:-moz-linear-gradient( center top, #629edb 5%, #003f7f 100% );" +
                "filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=\"#629edb\", endColorstr=\"#003f7f\"); background: -o-linear-gradient(top,#629edb,003f7f);" +
                "background-color:#629edb;" +
                "}" +
                ".TableStyle tr:first-child td:first-child{" +
                "border-width:0px 0px 1px 0px;" +
                "}" +
                ".TableStyle tr:first-child td:last-child{" +
                "border-width:0px 0px 1px 1px;" +
                "}" +
                "</style>";
        return cssString;
    }


}
