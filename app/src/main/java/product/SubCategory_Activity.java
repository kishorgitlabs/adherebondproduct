package product;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.Login_Activity;
import com.brainmagic.adherebond.ProjectActivity;
//import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.Welcome_Activity;

import java.util.ArrayList;

import Logout.logout;
import about.About_Activity;
import adapter.Catagory_Adapter;
import adapter.ImageAdapter;
import alertbox.Alertbox;
import alertbox.LoginAlert;
import alertbox.PasswordChangeAlert;
import contact.Contact_Activity;
import database.DBHelper;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import offers.Offers_Activity;
import search.Search_Activity;
import whatsnew.Whatsnew_Activity;

public class SubCategory_Activity extends AppCompatActivity {


    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    String usertype;
    NetworkConnection network = new NetworkConnection(SubCategory_Activity.this);
    Alertbox alert = new Alertbox(SubCategory_Activity.this);
    ArrayList<String> catagoryList, imageList;
    ProgressDialog progressDialog;
    private ArrayList<String> IDList;
    TextView title;
    String id, s,sub;
    ListView grid_view;
    ImageView back, home, menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category_);

        myshare = getSharedPreferences("Registration", SubCategory_Activity.this.MODE_PRIVATE);
        editor = myshare.edit();

        catagoryList = new ArrayList<String>();
        imageList = new ArrayList<String>();
        IDList = new ArrayList<String>();
        grid_view = (ListView) findViewById(R.id.gridView);
        title = (TextView) findViewById(R.id.title_text);
        s = getIntent().getStringExtra("Category");
        sub = getIntent().getStringExtra("Category");
        title.setText(capitalizeString(s));
        id = getIntent().getStringExtra("ID");
        new GetCatagory().execute();

        grid_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent a = new Intent(SubCategory_Activity.this, ProductList_Activity.class);
                a.putExtra("ID", IDList.get(i));
                a.putExtra("Category", catagoryList.get(i));
                a.putExtra("MainID", id);
                startActivity(a);
            }
        });

        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();

            }
        });

        home=(ImageView)findViewById(R.id.home);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(SubCategory_Activity.this, Home_Activity.class);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(mainIntent);
                finish();
            }
        });

        menu = (ImageView) findViewById(R.id.menubar);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(SubCategory_Activity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub


                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(SubCategory_Activity.this, About_Activity.class);
                                startActivity(about);
                                return true;

                            case R.id.expense:
                                PasswordChangeAlert alerts = new PasswordChangeAlert(SubCategory_Activity.this);// new saleslogin().execute();
                                alerts.showLoginbox();
                                return true;

                            case R.id.searchmenu:
                                Intent search = new Intent(SubCategory_Activity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(SubCategory_Activity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(SubCategory_Activity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.watsnew:
                                if (network.CheckInternet()) {
                                    Intent f = new Intent(SubCategory_Activity.this, ProjectActivity.class);
                                    startActivity(f);}
                                else{
                                    Alertbox alert = new Alertbox(SubCategory_Activity.this);
                                    alert.showAlertbox("Kindly check your Internet Connection");
                                }
                                return true;
                            case R.id.offers:
                                usertype=myshare.getString("Usertype","");
                                if(usertype.equals("Cera Employee"))
                                {
                                    if (!myshare.getBoolean("islogin",false))
                                    {
                                        CheckInternet();
                                        Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                        Log.v("is  Login ",Boolean.toString(myshare.getBoolean("alertlogin",false)));
                                    }
                                    else
                                    {
                                        if (network.CheckInternet()) {
                                            Intent f = new Intent(SubCategory_Activity.this, Offers_Activity.class);
                                            startActivity(f);
                                        }
                                        else {

                                            alert.showAlertbox("Kindly check your Internet Connection");
                                        }

                                    }
                                }
                                else
                                {
                                    Intent f = new Intent(SubCategory_Activity.this, Offers_Activity.class);
                                    startActivity(f);
                                }return true;


                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {
                                    //menu.findViewById(R.id.loginmenu).setVisibility(View.VISIBLE);
                                    Intent b = new Intent(SubCategory_Activity.this, Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {

                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                    ///menu.findViewById(R.id.loginmenu).setVisibility(View.VISIBLE);
                                    Intent b = new Intent(SubCategory_Activity.this, Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(SubCategory_Activity.this).log_out();
                                return true;

                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popupmenu_others);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });


    }

    public static String capitalizeString(String string) {
        try

        {
            char[] chars = string.toLowerCase().toCharArray();
            boolean found = false;
            for (int i = 0; i < chars.length; i++) {
                if (!found && Character.isLetter(chars[i])) {
                    chars[i] = Character.toUpperCase(chars[i]);
                    found = true;
                } else if (Character.isWhitespace(chars[i]) || chars[i] == '.' || chars[i] == '&') { // You can add other chars here
                    found = false;
                }
            }
            return String.valueOf(chars);
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
    }

    @Override
    public void onBackPressed() {
        if(s.equals(""))
        {
            Intent gomain = new Intent(SubCategory_Activity.this,Product_Activity.class);
            // gomain.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            startActivity(gomain);
        }
        else {
            super.onBackPressed();
        }

    }

    class GetCatagory extends AsyncTask<String, Void, String> {

        private String productquery;
        private SQLiteDatabase db;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SubCategory_Activity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... strings) {

            try

            {
                DBHelper dbhelper = new DBHelper(SubCategory_Activity.this);
                db = dbhelper.readDataBase();
                productquery = "select distinct MProdsub_name,MProdsub_Img,MProdsub_ID from Product_Sub_Category where MProd_ID='" + id + "' and deleteflag = 'notdelete' order by MProdsub_priority asc";
                Cursor cursor = db.rawQuery(productquery, null);

                if (cursor.moveToFirst()) {
                    do
                    {
                        catagoryList.add(cursor.getString(cursor.getColumnIndex("MProdsub_name")));
                        imageList.add(cursor.getString(cursor.getColumnIndex("MProdsub_Img")));
                        IDList.add(cursor.getString(cursor.getColumnIndex("MProdsub_ID")));
                    }

                    while (cursor.moveToNext());

                }
                Log.v("sub category",catagoryList.get(0));
                Log.v("sub category codition",Boolean.toString(catagoryList.get(0).equals("No sub category")));

                if (catagoryList.get(0).equals("No sub category"))
                {
                    return "empty";
                }
                else
                {
                    return "success";
                }
            }


            catch(Exception e)
            {
                e.printStackTrace();
                return "error";
            }


        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if (s.equals("success")) {
                grid_view.setAdapter(new Catagory_Adapter(SubCategory_Activity.this, catagoryList, imageList));
            } else if (s.equals("empty"))
            {
                Intent a = new Intent(SubCategory_Activity.this, ProductList_Activity.class);
                a.putExtra("ID", "");
                a.putExtra("SubCategories", "");
                a.putExtra("SubCategories1", sub);
                a.putExtra("MainID", id);
                startActivity(a);
            } else {

            }


        }
    }

    private void CheckInternet() {
        if (network.CheckInternet()) {
            LoginAlert alert= new LoginAlert(SubCategory_Activity.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {

            alert.showAlertbox("Kindly check your Internet Connection");
        }
    }

}
