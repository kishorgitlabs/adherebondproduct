package offers;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;

import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.Login_Activity;
//import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.Sales_segments_Activity;
import com.brainmagic.adherebond.Welcome_Activity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import Logout.logout;
import about.About_Activity;
import adapter.Offers_Adapter;
import alertbox.AlertDialogue;
import alertbox.Alertbox;
import alertbox.PasswordChangeAlert;
import contact.Contact_Activity;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import persistency.ServerConnection;
import product.Product_Activity;
import search.Search_Activity;

public class Offers_Activity extends AppCompatActivity {
    private ArrayList<String> SchemeList,DescriptionList,DateList;;
    ImageView home,back;
    ListView gridview;
    HorizontalScrollView horizontalScrollView;
    String usertype,query,query1;
    ImageView menu;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    NetworkConnection network = new NetworkConnection(Offers_Activity.this);
    private AlertDialogue alert = new AlertDialogue(Offers_Activity.this);
   private String formattedDate;
    private ProgressDialog progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers_);
        myshare = getSharedPreferences("Registration",Offers_Activity.this.MODE_PRIVATE);
        editor = myshare.edit();

        gridview = (ListView) findViewById(R.id.gridView1);
        horizontalScrollView = (HorizontalScrollView)findViewById(R.id.horilist1);
        SchemeList= new ArrayList<String>();
        DescriptionList = new ArrayList<String>();
        DateList = new ArrayList<String>();
        home=(ImageView)findViewById(R.id.home);
        back=(ImageView)findViewById(R.id.back);
        //date
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        formattedDate = df.format(c.getTime());
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(Offers_Activity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });
        checkInternet();
        menu=(ImageView)findViewById(R.id.menubar);

        menu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                final PopupMenu popupMenu = new PopupMenu(Offers_Activity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }

                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub




                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(Offers_Activity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(Offers_Activity.this, Product_Activity.class);
                                startActivity(product);
                                return true;
                            case R.id.expense:
                                PasswordChangeAlert alert = new PasswordChangeAlert(Offers_Activity.this);// new saleslogin().execute();
                                alert.showLoginbox();
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(Offers_Activity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(Offers_Activity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(Offers_Activity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {

                                    Intent b = new Intent(Offers_Activity.this, Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));

                                    Intent b = new Intent(Offers_Activity.this, Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(Offers_Activity.this).log_out();
                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_offer);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });
    }
    private void checkInternet() {
        NetworkConnection net = new NetworkConnection(Offers_Activity.this);
        if (net.CheckInternet()) {
            new whatsnew().execute();
        } else {
            alert.showAlertbox("Please check your network connection and try again!");
            alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    finish();
                }
            });
        }
    }
    class whatsnew extends AsyncTask<String, Void, String> {

        //String currentDateTimeString;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressbar = new ProgressDialog(Offers_Activity.this);
            progressbar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressbar.setMessage("Loading....");
            progressbar.setCancelable(false);
            progressbar.show();
           /* Calendar c = Calendar.getInstance();
            System.out.println("Current time => "+c.getTime());

            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            Date = c.getTime();
            currentDateTimeString = df.format(c.getTime());*/
            //currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date("MM/dd/yyyy"));
            // Log.v("Dtte",currentDateTimeString);
            myshare=getSharedPreferences("Registration",MODE_PRIVATE);
            usertype=myshare.getString("Usertype","");


        }

        @Override
        protected String doInBackground(String... strings) {
            //int i = 0;
            ServerConnection server = new ServerConnection();
            try {
                Connection connection = server.getConnection();
                Statement stmt = connection.createStatement();

                   // query = "select Schemesname,Description,ShemeExpiryDate from SchemesPromotion where ActiveMessage = 'Active'";
                    query1="select Schemesname,Description,convert(datetime,ShemeExpiryDate,103) as ShemeExpiryDate  from SchemesPromotion where ActiveMessage = 'Active' and convert(datetime,ShemeExpiryDate,103)>= convert(datetime,'29/12/2018',103)";
                //String query = "select Schemesname,Description,ShemeExpiryDate from SchemesPromotion where  and ShemeExpiryDate >= '"+currentDateTimeString+"'";
                Log.v("Scheme Query", query1);
                ResultSet rset = stmt.executeQuery(query1);
                while (rset.next())
                {
                    SchemeList.add(rset.getString("Schemesname"));
                    DescriptionList.add(rset.getString("Description"));
                    DateList.add(rset.getString("ShemeExpiryDate"));
                }
                if (SchemeList.isEmpty())
                {
                    return "nodata";
                }
                else
                {
                    connection.close();
                    stmt.close();
                    rset.close();
                    return "success";
                }
            } catch (Exception e) {
                Log.v("list",e.getMessage());
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressbar.dismiss();
            if (s.equals("success")) {
                if(!SchemeList.isEmpty()){
                    horizontalScrollView.setVisibility(View.VISIBLE);
                    Offers_Adapter adapter;
                    adapter = new Offers_Adapter(Offers_Activity.this,SchemeList,DescriptionList,DateList);
                    gridview.setAdapter(adapter);
                }
            }
            else if(s.equals("nodata"))
            {
                alert.showAlertbox("Sorry! No Offers/Schemes available");
                alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        finish();
                    }
                });

            }
            else
            {
                alert.showAlertbox("Please check your network connection and try again.");
                alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        finish();
                    }
                });
            }
        }
    }
}
