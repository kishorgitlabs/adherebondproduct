package com.brainmagic.adherebond;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;


import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;

import com.brainmagic.adherebond.R;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import Logout.logout;
import about.About_Activity;
import adapter.CuslistAdapter;
import adapter.OutStandingAdapter;
import alertbox.Alertbox;
import api.models.master.Customer_list;
import api.models.master.PAl_list;
import contact.Contact_Activity;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import product.Product_Activity;
import roomdb.database.AppDatabase;
import search.Search_Activity;

import static roomdb.database.AppDatabase.getAppDatabase;

public class Sales_Outstanding_Activity extends AppCompatActivity {
    private AppDatabase db;
    private static final int PERMISSION_REQUEST = 100;
    private Spinner Dealerspinner;
    private List<String> DealerNamesList,DealerCodeList,DealerEmailList,DealerMobileList,
            DealerCityList,CustomerCode,CustomernameList;
    private HorizontalScrollView outstanding_horizontal;
    private ListView Outlistview;
    Button Search;
    //private TextView DealerCity,DealerEmail,DealerMobile;
    private ArrayList<String> DateList,AmountList,DocumentNumberList,DealercodeList,TrailList;
    private Alertbox box = new Alertbox(Sales_Outstanding_Activity.this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    String salesID,SalesName,TableName,Customername;
    private String CustomerName,Dealername;
    ImageView home,back,menu;
    TextView Text2,Amount;
    FrameLayout footerLayout;
    private List<PAl_list> mPa_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales__outstanding_);
        outstanding_horizontal = ((HorizontalScrollView)findViewById(R.id.outstanding_horizontal));
        Outlistview = (ListView) findViewById(R.id.Outstanding_list);
        Dealerspinner  = (Spinner) findViewById(R.id.customer_spinner);
            footerLayout = (FrameLayout) getLayoutInflater().inflate(R.layout.footerview_outstanding,null);
        db = getAppDatabase(Sales_Outstanding_Activity.this);
        Amount = (TextView) footerLayout.findViewById(R.id.amount);
        Outlistview.addFooterView(footerLayout, null, false);
        menu=(ImageView)findViewById(R.id.menubar);
        Search =(Button)findViewById(R.id.search);
        DealerNamesList = new ArrayList<String>();
        DealerCodeList = new ArrayList<String>();
        DealerEmailList = new ArrayList<String>();
        DealerMobileList = new ArrayList<String>();
        CustomerCode = new ArrayList<String>();
        DealercodeList = new ArrayList<String>();;
        TrailList = new ArrayList<String>();;
        DealerCityList = new ArrayList<String>();
        CustomernameList = new ArrayList<String>();
        DocumentNumberList = new ArrayList<String>();
        DateList = new ArrayList<String>();
        AmountList = new ArrayList<String>();
        home = (ImageView) findViewById(R.id.home);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(Sales_Outstanding_Activity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                final PopupMenu popupMenu = new PopupMenu(Sales_Outstanding_Activity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(Sales_Outstanding_Activity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(Sales_Outstanding_Activity.this, Product_Activity.class);
                                startActivity(product);
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(Sales_Outstanding_Activity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(Sales_Outstanding_Activity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.watsnew:
                                Intent contact1 = new Intent(Sales_Outstanding_Activity.this, com.brainmagic.adherebond.ProjectActivity.class);
                                startActivity(contact1);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(Sales_Outstanding_Activity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {

                                    Intent b = new Intent(Sales_Outstanding_Activity.this, com.brainmagic.adherebond.Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));

                                    Intent b = new Intent(Sales_Outstanding_Activity.this, com.brainmagic.adherebond.Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(Sales_Outstanding_Activity.this).log_out();
                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_login_inner);
                //Whatsnew_Activity.super.onPrepareOptionsMenu(popupMenu);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });


        Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Customername = Dealerspinner.getSelectedItem().toString();
                if (Customername.equals("All")) {
                    customers();
                } else {
                    selectedcustomers(Customername);
                }

            }
        });

        Getpermissionforsms();

    }

    private void Getpermissionforsms() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.CALL_PHONE)) {
                    Snackbar.make(findViewById(R.id.activity_outstanding), "You need to give permission", Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public void onClick(View v) {
                            requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE},PERMISSION_REQUEST);
                        }
                    }).show();
                } else {
                    requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE},PERMISSION_REQUEST);
                }
            } else {
                GetDealerNamesForSpinner();
            }
        } else
        {
            GetDealerNamesForSpinner();

        }


    }
    private void GetDealerNamesForSpinner() {

        DealerNamesList =  db.adhereDao().GetCustomer_name_list();
        DealerNamesList.add(0,"All");
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(Sales_Outstanding_Activity.this,R.layout.simple_spinner_item,DealerNamesList);
        spinnerAdapter.setNotifyOnChange(true);
        spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
        Dealerspinner.setAdapter(spinnerAdapter);

    }

    private void selectedcustomers(String customername) {
         mPa_list =  db.adhereDao().GetPailist(customername);
        OutStandingAdapter adapter = new OutStandingAdapter(Sales_Outstanding_Activity.this, mPa_list);
        Outlistview.setAdapter(adapter);
        totalamout();
    }

    private void totalamout() {
        {
            double sum = 0;
            for(int j=0; j< mPa_list.size(); j++){
                sum += Double.parseDouble(mPa_list.get(j).getPendingAmount());
            }
            Amount.setText(String.format("%.2f",  + sum));

        }
    }

    private void customers() {
        mPa_list =  db.adhereDao().GetPailist();
        OutStandingAdapter adapter = new OutStandingAdapter(Sales_Outstanding_Activity.this, mPa_list);
        Outlistview.setAdapter(adapter);
        totalamout();

    }
}
