package com.brainmagic.adherebond;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import androidx.annotation.NonNull;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.adherebond.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

import Logout.logout;
import SharedPreference.Shared;
import about.About_Activity;
import alertbox.AlertDialogue;
import alertbox.Alertbox;
import contact.Contact_Activity;
import database.DBHelper;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import persistency.ServerConnection;
import product.Product_Activity;
import pub.devrel.easypermissions.EasyPermissions;
import roomdb.database.AppDatabase;
import search.Search_Activity;

import static roomdb.database.AppDatabase.getAppDatabase;

public class DailyOrderActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks, ResultCallback<LocationSettingsResult>,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {
    private static final int PERMISSION_REQUEST = 100;
    private AppDatabase db;
    private SQLiteDatabase db1;
    NetworkConnection network = new NetworkConnection(DailyOrderActivity.this);
    private Alertbox box = new Alertbox(DailyOrderActivity.this);
    private Spinner dealerSpinner, ordertypeSpinner, Packagespinner, Partnumberspinner;
    private AutoCompleteTextView Partnameedittext;
    private Spinner Remark;
    private ArrayList<String>remarkslist;
    private Spinner orderedit;
    private EditText Quantityedittext;
    private EditText Discountedittext, placeofvisit;
    private Button Order, Additems, Previewbutton;
    private boolean checkNetwork;
    private String is_online;
    private List<String> dealerNameList;
    private EditText mobilenumber;
    public Connection connection;
    public Statement stmt;
    public ResultSet reset;
    private String username;
    private int S_id;
    public ArrayList<Integer> dealerID;
    private String CustomerName, ProductName, PartNO, Package, string_mail, Customercode, BranchCode;
    private ArrayList<String> partsNameList;
    private ArrayList<String> partsNumberList;
    private String partsRateList, Segment, MobileNO;
    protected String total;
    public ProgressDialog progressDialog;
    protected String unit;
    protected String quantity, Remarks;
    protected String amount, unitprice;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    protected String discount;
    public ArrayList<String> msgName;
    private ProgressDialog loading;
    public List<String> msgmobileno;
    public List<String> msgemail;
    public String orderNumber;
    public String cmpnymsg;
    public String emailad = "";
    private ArrayList<String> orderTypeList, pagagelist, ordermodelist;
    protected String OrderType;
    public String orderNo;
    protected String PartName, salesID, callmode,SalesName, SalesEmial, SalesPass, SalesBranch, SalesEmailSign, SalesOrdermail, SalesOrdermobile, SalesPaymentemail, SalesPaymentmobile;
    public String ProductNo;
    public ArrayList<String> dealerCodeList, PackageList, productlist, mobilelist;
    protected String customers, productname, pagage, ordertype;
    private String formattedDate, customertype, formattedDate1;
    private Button add, order;
    private static TextView visitdate;
   private String remarks1;
    double latitude;
    double longitude;
    com.brainmagic.adherebond.LocationHelper locationHelper;


    // location
    private Location mLastLocation;
    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    // boolean flag to toggle periodic location updates
    private boolean mRequestingLocationUpdates = false;
    private LocationRequest mLocationRequest;
    private static final String TAG = "";
    private int REQUEST_CHECK_SETTINGS = 100;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private AlertDialogue alert = new AlertDialogue(DailyOrderActivity.this);

    // Table Order
    public static final String TABLE_PartsOrder = "PartsOrder";
    public static final String COLUMN_executive = "SalesName";
    public static final String COLUMN_customername = "CustomerName";
    public static final String COLUMN_orderID = "orderNo";
    public static final String COLUMN_productname = "ProductName";
    public static final String COLUMN_package = "Package";
    public static final String COLUMN_partno = "PartNO";
    public static final String COLUMN_discount = "discount";
    public static final String COLUMN_quantity = "quantity";
    public static final String COLUMN_unitprice = "unit";
    public static final String COLUMN_amount = "amount";
    public static final String COLUMN_currentdate = "formattedDate";
    public static final String COLUMN_OrderType = "OrderType";
    public static final String COLUMN_cutomermobile = "MobileNO";
    public static final String COLUMN_customeremail = "string_mail";


    // Table Additems
    private static final String TABLE_Additems = "Orderpreview";
    public static final String COLUMN_Addexecutive = "SalesName";
    public static final String COLUMN_Addcustomername = "CustomerName";
    public static final String COLUMN_Addproductname = "ProductName";
    public static final String COLUMN_Addpackage = "Package";
    public static final String COLUMN_Addquantity = "quantity";
    public static final String COLUMN_Adddiscount = "discount";
    public static final String COLUMN_AddOrderType = "OrderType";
    public static final String COLUMN_Addcustomercode = "Customercode";
    public static final String COLUMN_Addcurrentdate = "formattedDate";
    public static final String COLUMN_AddMobileNO = "MobileNO";
    public static final String COLUMN_Addstring_mail = "string_mail";
    public static final String COLUMN_Addstring_ordermode = "order_mode";
    private static final String COLUMN_AddBranchCode = "BranchCode";
    private static final String COLUMN_AddRemarks = "Remarks";
    private String ordermode;
    private String ordermode1;
    private String mobilestr;
    private boolean mobilesr;
    private String mobile1;
    static String fromstring;
    DecimalFormat mFormat = new DecimalFormat("00");
    ImageView home, back, menu;
    String address, address1, city, state, country, postalCode;
Spinner calltype;
ArrayList<String> callstring;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_order);
        locationHelper = new com.brainmagic.adherebond.LocationHelper(this);
        locationHelper.checkpermission();
        myshare = getSharedPreferences("Registration", DailyOrderActivity.this.MODE_PRIVATE);
        editor = myshare.edit();
        home = (ImageView) findViewById(R.id.home);
        back = (ImageView) findViewById(R.id.back);
        calltype=findViewById(R.id.calltype);

        callstring=new ArrayList<>();
        callstring.add("Select Call Type");
        callstring.add("New");
        callstring.add("Repeat");

        ArrayAdapter adapter=new ArrayAdapter(DailyOrderActivity.this,R.layout.simple_spinner_item,callstring);
        calltype.setAdapter(adapter);
        visitdate = (TextView) findViewById(R.id.visitdate);
        menu = (ImageView) findViewById(R.id.menubar);
        db = getAppDatabase(DailyOrderActivity.this);
        loading = new ProgressDialog(DailyOrderActivity.this);
        Date d = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        formattedDate = df.format(c.getTime());
        back = (ImageView) findViewById(R.id.back);
        home = (ImageView) findViewById(R.id.home);
        //date format1
        Date d1 = new Date();
        Calendar c1 = Calendar.getInstance();
        c.setTime(d1);
        SimpleDateFormat df1 = new SimpleDateFormat("dd/MM/yyyy");
        formattedDate1 = df1.format(c.getTime());
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(DailyOrderActivity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });


        // location
        //Check If Google Services Is Available
        if (getServicesAvailable()) {
            // Building the GoogleApi client
            buildGoogleApiClient();
            createLocationRequest();
            //  Toast.makeText(this, "Google Service Is Available!!", Toast.LENGTH_SHORT).show();
        }
        isGpsOn();

        SalesName = myshare.getString(Shared.K_Sales_name, "");
        dealerSpinner = (Spinner) findViewById(R.id.dealerspinner);
        mobilenumber = findViewById(R.id.mobilenumber);
        //mobileno=(EditText)findViewById(R.id.mobilenumber);
        placeofvisit = (EditText) findViewById(R.id.placeofvisit);
        Remark = (Spinner) findViewById(R.id.remark);
        add = (Button) findViewById(R.id.add);
        order = (Button) findViewById(R.id.order);
        dealerNameList = new ArrayList<String>();
        mobilelist = new ArrayList<>();
        remarkslist=new ArrayList<>();
        remarkslist.add("Select Remarks");
        remarkslist.add("Stock (0)");
        remarkslist.add("Stock (a)");
        remarkslist.add("Poor Pay");
        remarkslist.add("Product Compalint");
        remarkslist.add("Price Different");
        remarkslist.add("Next Visit");
        remarkslist.add("Not Intrested");
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(DailyOrderActivity.this, R.layout.simple_spinner_item, remarkslist);
        spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
        Remark.setAdapter(spinnerAdapter);

        Remark.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                remarks1=parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

      calltype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
          @Override
          public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
              callmode=parent.getItemAtPosition(position).toString();
          }

          @Override
          public void onNothingSelected(AdapterView<?> parent) {

          }
      });
        menu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                final PopupMenu popupMenu = new PopupMenu(DailyOrderActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(DailyOrderActivity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(DailyOrderActivity.this, Product_Activity.class);
                                startActivity(product);
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(DailyOrderActivity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(DailyOrderActivity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.watsnew:
                                Intent contact1 = new Intent(DailyOrderActivity.this, com.brainmagic.adherebond.ProjectActivity.class);
                                startActivity(contact1);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(DailyOrderActivity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin", false)) {

                                    Intent b = new Intent(DailyOrderActivity.this, com.brainmagic.adherebond.Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ", Boolean.toString(myshare.getBoolean("islogin", false)));
                                } else {
                                    Log.v("is  Login ", Boolean.toString(myshare.getBoolean("islogin", false)));

                                    Intent b = new Intent(DailyOrderActivity.this, com.brainmagic.adherebond.Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(DailyOrderActivity.this).log_out();
                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_login_inner);
                //Whatsnew_Activity.super.onPrepareOptionsMenu(popupMenu);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin", false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });


        visitdate.setText(formattedDate1);
       /* visitdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DailyOrderActivity.FromDatePickerFragment1();
                newFragment.show(getSupportFragmentManager(),"datePicker");
            }
        });*/
        Getpermissionforsms();

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (dealerSpinner.equals("")) {
                    Toast.makeText(getApplicationContext(), "Select Customer Name", Toast.LENGTH_LONG).show();
                } else if (mobilenumber.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Select Mobilenumber", Toast.LENGTH_LONG).show();
                } else if (placeofvisit.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Select Visit", Toast.LENGTH_LONG).show();
                } else if (Remark.equals("")) {
                    Toast.makeText(getApplicationContext(), "Select Remarks", Toast.LENGTH_LONG).show();
                } else {
                    new Additems1().execute();
                }
            }
        });

       /* placeofvisit.setText(address);
        if (locationHelper.checkPlayServices()) {

            // Building the GoogleApi client
            locationHelper.buildGoogleApiClient();
        }
        mLastLocation = locationHelper.getLocation();
        if (mLastLocation != null) {

            latitude = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();
            getAddress();

        } else {


            Toast.makeText(DailyOrderActivity.this, "Couldn't get the location. Make sure location is enabled on the device", Toast.LENGTH_LONG).show();
            //showToast("Couldn't get the location. Make sure location is enabled on the device");
        }*/
    }

    /**
     * Method to check if GPS is on or not
     */
    private void isGpsOn() {

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        startLocationUpdates();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(DailyOrderActivity.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.e(TAG, "Exception : " + e);
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.e(TAG, "Location settings are not satisfied.");
                        break;
                }
            }
        });

    }

    public boolean getServicesAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(isAvailable)) {

            Dialog dialog = api.getErrorDialog(this, isAvailable, 0);
            dialog.show();
            return false;
        } else {
            Toast.makeText(this, "Cannot Connect To Play Services", Toast.LENGTH_SHORT).show();
            return false;
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getServicesAvailable()) {
            buildGoogleApiClient();
            if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
                // mGoogleApiClient.stopAutoManage(this);
                startLocationUpdates();
            }
        }


    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null)
            if (mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
/*
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //  stopService(new Intent(PlacePickerActivity.this, GPSTracker.class));
        stopLocationUpdates();


    }*/

    // Creating google api client object
    protected synchronized void buildGoogleApiClient() {
        try {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .enableAutoManage(this, this).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Creating location request object
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(com.brainmagic.adherebond.AppConstants.UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(com.brainmagic.adherebond.AppConstants.FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(com.brainmagic.adherebond.AppConstants.DISPLACEMENT);
    }


//    //Stopping location updates
//    protected void stopLocationUpdates() {
//           LocationServices.FusedLocationApi.removeLocationUpdates(
//                mGoogleApiClient, this);
//    }


    //Ask permissions
//    public void AskLocationPermission() {
//        if (EasyPermissions.hasPermissions(DailyOrderActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
//            // Have permission, do the thing!
//            if (CheckLocationIsEnabled()) {
//                // if location is enabled show place picker activity to use
//                startLocationUpdates();
//
//            } else {
//
//                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
//                        .addLocationRequest(mLocationRequest);
//
//                PendingResult<LocationSettingsResult> result =
//                        LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
//                result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
//                    @Override
//                    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
//                        final Status status = locationSettingsResult.getStatus();
//                        switch (status.getStatusCode()) {
//                            case LocationSettingsStatusCodes.SUCCESS:
//                                startLocationUpdates();
//                                break;
//                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                                try {
//                                    status.startResolutionForResult(
//                                            DailyOrderActivity.this,
//                                            REQUEST_CHECK_SETTINGS);
//                                } catch (IntentSender.SendIntentException e) {
//                                    Log.e(TAG, "Exception : " + e);
//                                }
//                                break;
//                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                                Log.e(TAG, "Location settings are not satisfied.");
//                                break;
//                        }
//                    }
//                });
//
//
//            }
//        } else {
//            // Request one permission
//            EasyPermissions.requestPermissions(this, getString(R.string.rationale_location), MY_PERMISSIONS_REQUEST_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION);
//
//        }
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {

        if (CheckLocationIsEnabled()) {
            // if location is enabled show place picker activity to user
            startLocationUpdates();
        } else {
            // if location is not enabled show request to enable location to user
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest);
            builder.setAlwaysShow(true);
            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(
                            mGoogleApiClient,
                            builder.build()

                    );
            result.setResultCallback(this);
        }

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Some permissions have been denied
        // if location is enabled show place picker activity to user
        // AskLocationPermission();
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {


        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                // NO need to show the dialog;
                ConvertToAddress();
                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                //  Location settings are not satisfied. Show the user a dialog
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(DailyOrderActivity.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    //failed to show
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are unavailable so not possible to show any dialog now
                break;
        }
    }

    private void ConvertToAddress() {
        new GeocodeAsyncTask().execute(mLastLocation.getLatitude(), mLastLocation.getLongitude());


    }

    private class GeocodeAsyncTask extends AsyncTask<Double, Void, Address> {

        String errorMessage = "";


        @Override
        protected Address doInBackground(Double... latlang) {
            Geocoder geocoder = new Geocoder(DailyOrderActivity.this, Locale.getDefault());
            List<Address> addresses = null;
            if (geocoder.isPresent()) {
                try {
                    addresses = geocoder.getFromLocation(latlang[0], latlang[1], 1);
                } catch (IOException ioException) {
                    errorMessage = "Service Not Available";
                    Log.e(TAG, errorMessage, ioException);
                } catch (IllegalArgumentException illegalArgumentException) {
                    errorMessage = "Invalid Latitude or Longitude Used";
                    Log.e(TAG, errorMessage + ". " +
                            "Latitude = " + latlang[0] + ", Longitude = " +
                            latlang[1], illegalArgumentException);
                }

                if (addresses != null && addresses.size() > 0)
                    return addresses.get(0);
            } else {
                new GetGeoCodeAPIAsynchTask().execute(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            }


            return null;
        }

        protected void onPostExecute(Address addresss) {

            if (addresss == null) {
                new GetGeoCodeAPIAsynchTask().execute(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            } else {
                String address = addresss.getAddressLine(0); //0 to obtain first possible address
                String city = addresss.getLocality();
                String state = addresss.getAdminArea();
                //create your custom title
                String title = city + "-" + state;
                placeofvisit.setText(address +
                                    "\n"
                                      + title);

            }
        }
    }

    private class GetGeoCodeAPIAsynchTask extends AsyncTask<Double, Void, String[]> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String[] doInBackground(Double... latlang) {
            String response;
            try {
                String URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlang[0] + "," + latlang[1];
                Log.v("URL", URL);
                response = getLatLongByURL(URL);
                return new String[]{response};
            } catch (Exception e) {
                return new String[]{"error"};
            }
        }

        @Override
        protected void onPostExecute(String... result) {
            try {
                JSONObject jsonObject = new JSONObject(result[0]);

                String address = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(0).getString("long_name");

                String city = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(2).getString("long_name");

                String state = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(4).getString("long_name");

                String title = city + "-" + state;


                placeofvisit.setText(address + "\n" + title);

                Log.d("Address", "" + address);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public String getLatLongByURL(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    //Starting the location updates
    protected void startLocationUpdates() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                ConvertToAddress();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
                Log.d(TAG, "Permission Not Granted");

            }

        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            ConvertToAddress();

        }
    }

    private boolean CheckLocationIsEnabled() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }
        if (mGoogleApiClient != null)
            mLastLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);
        if (mLastLocation == null) {
            return false;
        } else {

            return false;
        }
    }


//    private void getAddress() {
//        Address locationAddress;
//
//        locationAddress = locationHelper.getAddress(latitude, longitude);
//
//        if (locationAddress != null) {
//
//            String address = locationAddress.getAddressLine(0);
//
//
//            String currentLocation;
//
//            if (!TextUtils.isEmpty(address)) {
//                currentLocation = address;
//
//                if (!TextUtils.isEmpty(address1))
//                    currentLocation += "\n" + address1;
//
//                if (!TextUtils.isEmpty(city)) {
//                    currentLocation += "\n" + city;
//
//                    if (!TextUtils.isEmpty(postalCode))
//                        currentLocation += " - " + postalCode;
//                } else {
//                    if (!TextUtils.isEmpty(postalCode))
//                        currentLocation += "\n" + postalCode;
//                }
//
//                if (!TextUtils.isEmpty(state))
//                    currentLocation += "\n" + state;
//
//                if (!TextUtils.isEmpty(country))
//                    currentLocation += "\n" + country;
//
//
//            }
//
//        } else
//            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
//        //showToast("Something went wrong");
//
//    }





  /*  private void getAddress() {
        Address locationAddress;

        locationAddress=locationHelper.getAddress(latitude,longitude);

        if(locationAddress!=null)
        {

            String address = locationAddress.getAddressLine(0);
            String address1 = locationAddress.getAddressLine(1);
            String city = locationAddress.getLocality();
            String state = locationAddress.getAdminArea();
            String country = locationAddress.getCountryName();
            String postalCode = locationAddress.getPostalCode();


            String currentLocation;

            if(!TextUtils.isEmpty(address))
            {
                currentLocation=address;

                if (!TextUtils.isEmpty(address1))
                    currentLocation+="\n"+address1;

                if (!TextUtils.isEmpty(city))
                {
                    currentLocation+="\n"+city;

                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation+=" - "+postalCode;
                }
                else
                {
                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation+="\n"+postalCode;
                }

                if (!TextUtils.isEmpty(state))
                    currentLocation+="\n"+state;

                if (!TextUtils.isEmpty(country))
                    currentLocation+="\n"+country;


            }

        }
        else
            Toast.makeText(getApplicationContext(), "Some Went Wrong", Toast.LENGTH_LONG).show();
    }*/


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onLocationChanged(Location location) {

    }


    public static class FromDatePickerFragment1 extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        DecimalFormat mFormat = new DecimalFormat("00");

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);


            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            visitdate.setText(mFormat.format(day) + "/" + mFormat.format(month + 1) + "/" + year);
            //visitdate.setText(String.format("%d/%d/%d",mFormat.format(day) ,(month + 1) , year));
            fromstring = year + "-" + (month + 1) + "-" + day;
            visitdate.clearFocus();

        }
    }


    private void Getpermissionforsms() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.CALL_PHONE)) {
                    Snackbar.make(findViewById(R.id.activity_view_cusomers), "You need to give permission", Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public void onClick(View v) {
                            requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
                        }
                    }).show();
                } else {
                    requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
                }
            } else {
                GetDealerNamesForSpinner1();
            }
        } else {
            GetDealerNamesForSpinner1();

        }


        dealerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                customers = parent.getItemAtPosition(position).toString();
                if(!customers.equals("Select Customer name"))
                new getmobilenumber().execute();
                else{
                    Toast.makeText(getApplicationContext(), "Select Customer Name", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void GetDealerNamesForSpinner1() {
        dealerNameList = db.adhereDao().GetCustomer_name_list();
        dealerNameList.add(0, "Select Customer name");
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(DailyOrderActivity.this, R.layout.simple_spinner_item, dealerNameList);
        spinnerAdapter.setNotifyOnChange(true);
        spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
        dealerSpinner.setAdapter(spinnerAdapter);
    }


    private class getmobilenumber extends AsyncTask<String, Void, String> {
        private String mobile, mobilequery;
        private int a;

        //private SQLiteDatabase db;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... strings) {
            try

            {
                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                stmt = connection.createStatement();
                mobilequery = "Select * From Customer where Name= '" + customers + "'";
                reset = stmt.executeQuery(mobilequery);
                while (reset.next()) {
                    mobilelist.add(reset.getString("Mobile"));

                }
                if (!mobilelist.isEmpty()) {
                    stmt.close();
                    connection.close();
                    reset.close();
                    return "success";
                } else {
                    stmt.close();
                    connection.close();
                    reset.close();
                    return "empty";
                }


            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            switch (s) {
                case "success":
                    mobilenumber.setText(mobilelist.get(0));
                    break;
                case "empty":
                    break;
                default:
                    break;
            }

        }
    }


    private class Additems1 extends AsyncTask<String, Void, String> {
        String visit, remarks, cus, mobile2, visitdate1;
        private int a;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            loading = ProgressDialog.show(DailyOrderActivity.this, "Adherebonds", "Please wait...", false, false);
            visit = placeofvisit.getText().toString();
            visitdate1 = visitdate.getText().toString();
            customertype = "Existingcustomer";
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            try {
                try {
                    String query = "insert into DailyReport(calltype,SalesPersonName,PlaceOfVisit,CustomerName,CustMobileNo,Remark,VisitedDate,Time,CustomerType)" +
                            " values('" + callmode + "','" + SalesName + "','" + visit + "','" + customers + "','" + mobilenumber.getText().toString() + "','" + remarks1 + "','" + formattedDate1 + "','" + formattedDate + "','" + customertype + "')";
                    ServerConnection server = new ServerConnection();
                    connection = server.getConnection();
                    stmt = connection.createStatement();
                    Log.v("On Line", "SERVER Payment Table");
                    a = stmt.executeUpdate(query);


                    if (a == 0) {

                        stmt.close();
                        connection.close();
                        return "notinserted";
                    }

                    stmt.close();
                    connection.close();
                    return "inserted";

                } catch (Exception e) {
                    Log.v("Error in inserted", "Payment ");
                    Log.v("Error in Payment", e.getMessage());
                    return "notsuccess";
                }


            } catch (Exception e) {
                e.printStackTrace();
                return "notsuccess";

            }

        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            loading.dismiss();
            if (result.equals("inserted")) {

                db.close();
                //box.showAlertbox("Payment updated successfully");
                alert.showAlertbox("Visit Report  Successfully");
                alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        finish();
                    }
                });
                //  Toast.makeText(getApplicationContext(), "Payment Updated", Toast.LENGTH_LONG).show();
                //     new sendemail().execute();

            } else {
                box.showAlertbox("Error in Updating Payment! Try again!");
            }
        }

    }
}
