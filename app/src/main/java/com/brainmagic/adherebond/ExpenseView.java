package com.brainmagic.adherebond;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import Expense.ExpenseActivity;
import Logout.logout;
import about.About_Activity;
import adapter.ExpenseViewAdapter;
import alertbox.AlertDialogue;
import alertbox.Alertbox;
import alertbox.PasswordChangeAlert;
import api.models.Expense.ExpesneResponse;
import api.models.ExpenseModel;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import contact.Contact_Activity;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import offers.Offers_Activity;
import product.Product_Activity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import roomdb.database.AppDatabase;
import search.Search_Activity;

public class ExpenseView extends AppCompatActivity {

    private NetworkConnection network = new NetworkConnection(ExpenseView.this);
    private Alertbox alert = new Alertbox(ExpenseView.this);
    private AppDatabase db;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private PopupWindow mPopupWindow;
    ImageView home, back, menu;
    TextView sno,mode,ticamt,foodamt,openkm,closekm,fuel,otheramt,date,total;
    ListView gridView1;
    private AlertDialogue alerts = new AlertDialogue(ExpenseView.this);
  String mobileno;
    Alertbox box = new Alertbox(ExpenseView.this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expense_view);

        myshare = getSharedPreferences("Registration", Context.MODE_PRIVATE);
        mobileno=myshare.getString("mobile","");
        editor = myshare.edit();
    editor.commit();
        back = (ImageView) findViewById(R.id.back);
        home = (ImageView) findViewById(R.id.home);
        menu=(ImageView)findViewById(R.id.menubar);

        sno=findViewById(R.id.sno);
        mode=(TextView) findViewById(R.id.mode);
        ticamt=(TextView) findViewById(R.id.ticamt);
        foodamt=(TextView) findViewById(R.id.foodamt);
        openkm=(TextView) findViewById(R.id.openkm);
        closekm=(TextView) findViewById(R.id.closekm);
        otheramt=(TextView) findViewById(R.id.otheramt);
        date=(TextView) findViewById(R.id.date);
        total=(TextView) findViewById(R.id.total);
        gridView1=findViewById(R.id.gridView1);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(ExpenseView.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(ExpenseView.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }

        });

        menu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                final PopupMenu popupMenu = new PopupMenu(ExpenseView.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(ExpenseView.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(ExpenseView.this, Product_Activity.class);
                                startActivity(product);
                                return true;

                            case R.id.searchmenu:
                                Intent search = new Intent(ExpenseView.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(ExpenseView.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.expense:
                                PasswordChangeAlert alerts = new PasswordChangeAlert(ExpenseView.this);// new saleslogin().execute();
                                alerts.showLoginbox();
                                return true;

                            case R.id.watsnew:
                                Intent contact1 = new Intent(ExpenseView.this, com.brainmagic.adherebond.ProjectActivity.class);
                                startActivity(contact1);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(ExpenseView.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {

                                    Intent b = new Intent(ExpenseView.this, com.brainmagic.adherebond.Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));

                                    Intent b = new Intent(ExpenseView.this, com.brainmagic.adherebond.Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(ExpenseView.this).log_out();

                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_login_inner);
                //Whatsnew_Activity.super.onPrepareOptionsMenu(popupMenu);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });

        checkInternet();
    }


    private void checkInternet() {
        NetworkConnection net = new NetworkConnection(ExpenseView.this);
        if (net.CheckInternet()) {
           getViewDetails();

        } else {
            alerts.showAlertbox("Please check your network connection and try again!");
            alerts.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    finish();
                }
            });
        }
    }

    private  void getViewDetails(){


        try {

            final ProgressDialog loading = ProgressDialog.show(ExpenseView.this, getResources().getString(R.string.app_launcher_name), "Loading...", false, false);
            APIService service = RetroClient.getApiService();
            Call<ExpenseModel> call = service.expense(mobileno);

            call.enqueue(new Callback<ExpenseModel>() {
                @Override
                public void onResponse(Call<ExpenseModel> call, Response<ExpenseModel> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {

                            ExpenseViewAdapter adapter=new ExpenseViewAdapter(ExpenseView.this,response.body().getData());
gridView1.setAdapter(adapter);
                        }
                       else if (response.body().getResult().equals("NotSuccess")) {

                            box.showAlertboxwithback("No Record Found");
                        }
                        else
                        {
                            box.showAlertbox(getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }

                @Override
                public void onFailure(Call<ExpenseModel> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));

                }
            });



        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}