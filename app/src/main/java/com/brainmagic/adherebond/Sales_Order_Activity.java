package com.brainmagic.adherebond;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;


import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import Logout.logout;
import SQL_Fields.SQLITE;
import about.About_Activity;
import alertbox.AlertDialogue;
import alertbox.Alertbox;
import api.models.Order.SalesOrderAdd;
import api.models.colors.ColorsData;
import api.models.login.Login;
import api.models.packages.PackageData;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import contact.Contact_Activity;
import database.DBHelper;
import database.DBHelpersync;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import persistency.ServerConnection;
import product.Product_Activity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import roomdb.database.AppDatabase;
import search.Search_Activity;

import static roomdb.database.AppDatabase.getAppDatabase;

public class  Sales_Order_Activity extends AppCompatActivity {
    private static final int PERMISSION_REQUEST = 100;
    ImageView home,back,menu;
    private AppDatabase db;
    private SQLiteDatabase db1;
    NetworkConnection network = new NetworkConnection(Sales_Order_Activity.this);
    private Alertbox box = new Alertbox(Sales_Order_Activity.this);
    private Spinner dealerSpinner,ordertypeSpinner,Partnumberspinner;
    private AutoCompleteTextView Packagespinner,colours;
    private AutoCompleteTextView Partnameedittext;
    private EditText Remark,numbers;
    private Spinner  orderedit;
    private EditText Quantityedittext,packageedit,basicedittext,discountedittext,totaledittext;
    private EditText Discountedittext,packagespinner2;
    private Button Order,Additems,Previewbutton;
    private boolean checkNetwork;
    private String is_online;
    private List<String> dealerNameList;
    public Connection connection;
    public Statement stmt;
    public ResultSet reset;
    private String username;
    private int S_id;
    public ArrayList<Integer> dealerID;
    private String CustomerName,ProductName,PartNO,Package,string_mail,Customercode,BranchCode;
    private ArrayList<String> partsNameList;
    private ArrayList<String> partsNumberList;
    private String partsRateList,Segment,MobileNO;
    protected String total;
    public ProgressDialog progressDialog;
    protected String unit;
    protected String quantity,Remarks,mobile_S;
    protected String amount,unitprice;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    protected String discount;
    public ArrayList<String> msgName;
    private ProgressDialog loading;
    public List<String> msgmobileno;
    public List<String> msgemail;
    public String orderNumber;
    public String cmpnymsg;
    public String emailad="";
    private ArrayList<String> orderTypeList,pagagelist,ordermodelist;
    protected String OrderType;
    public String orderNo;
    protected String PartName,salesID,SalesName,SalesEmial,SalesPass,SalesBranch,SalesEmailSign,SalesOrdermail,SalesOrdermobile,SalesPaymentemail,SalesPaymentmobile;
    public String ProductNo;
    public ArrayList<String> dealerCodeList,PackageList,productlist;
    public ArrayList<String>colourlist;
    private List<String>  colors1,package1;
    protected String customers,productname,pagage,ordertype;
    private String formattedDate;
    private AlertDialogue alert = new AlertDialogue(Sales_Order_Activity.this);
String customerName_S,productName_S,packageName_S,packageEdit_S,qty_S,numer_S,color_S,basiPrice_S,discount_S,total_S,orderMode_S,orderType_S,remarks_S;
    // Table Order
    public static final String TABLE_PartsOrder = "PartsOrder";
    public static final String COLUMN_executive = "SalesName";
    public static final String COLUMN_customername = "CustomerName";
    public static final String COLUMN_orderID = "orderNo";
    public static final String COLUMN_productname = "ProductName";
    public static final String COLUMN_package = "Package";
    public static final String COLUMN_partno = "PartNO";
    public static final String COLUMN_discount = "discount";
    public static final String COLUMN_quantity = "quantity";
    public static final String COLUMN_unitprice = "unit";
    public static final String COLUMN_amount = "amount";
    public static final String COLUMN_currentdate="formattedDate";
    public static final String COLUMN_OrderType="OrderType";
    public static final String COLUMN_cutomermobile = "MobileNO";
    public static final String COLUMN_customeremail = "string_mail";


    // Table Additems
    private static final String TABLE_Additems = "Orderpreview";
    public static final String COLUMN_Addexecutive = "SalesName";
    public static final String COLUMN_Addcustomername = "CustomerName";
    public static final String COLUMN_Addproductname = "ProductName";
    public static final String COLUMN_Addpackage = "Package";
    public static final String COLUMN_Addquantity = "quantity";
    public static final String COLUMN_Adddiscount = "discount";
    public static final String COLUMN_AddOrderType="OrderType";
    public static final String COLUMN_Addcustomercode="Customercode";
    public static final String COLUMN_Addcurrentdate="formattedDate";
    public static final String COLUMN_AddMobileNO="MobileNO";
    public static final String COLUMN_Addstring_mail="string_mail";
    public static final String COLUMN_Addstring_ordermode="order_mode";
    private static final String COLUMN_AddBranchCode="BranchCode";
    private static final String COLUMN_AddRemarks="Remarks";
    private static final String COLUMN_Addstring_ordernumber="numbers";
    private static final String COLUMN_Addstring_ordercolors="colours";
    private static final String COLUMN_Unpackage="Unpackage";

    private static final String COLUMN_basic="basicprice";
    private static final String COLUMN_discountprice="discountprice";
    private static final String COLUMN_total="totalprice";

    private String ordermode;
    private String ordermode1,colours1,Numbers;
    String basicint,discountint;
  int a,b,totalamt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales__order_);

        Quantityedittext = (EditText) findViewById(R.id.quantityedittext);
        totaledittext=findViewById(R.id.totaledittext);
        basicedittext=findViewById(R.id.basicedittext);
        discountedittext=findViewById(R.id.discountedittext);
        myshare = getSharedPreferences("Registration", Sales_Order_Activity.this.MODE_PRIVATE);
        editor = myshare.edit();
        home = (ImageView) findViewById(R.id.home);
        back = (ImageView) findViewById(R.id.back);
        db = getAppDatabase(Sales_Order_Activity.this);
        loading=new ProgressDialog(Sales_Order_Activity.this);
        Date d = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedDate = df.format(c.getTime());
        mobile_S=myshare.getString("mobile","");

        editor.commit();
        basicedittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

basicint=s.toString();
if (basicint.length() >0){
    a=Integer.parseInt(basicint);
    totalamt=a*b;
    totaledittext.setText(String.valueOf(totalamt));
    totaledittext.setEnabled(false);
}
            }
        });



        Quantityedittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {



            }

            @Override
            public void afterTextChanged(Editable s) {
           discountint=s.toString();
           if (discountint.length()>0){
               b=Integer.parseInt(discountint);

           }
            }
        });


        productlist=new ArrayList<>();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(Sales_Order_Activity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });
        dealerSpinner = (Spinner) findViewById(R.id.dealerspinner);
        ordertypeSpinner = (Spinner) findViewById(R.id.ordertypespinner);
        Packagespinner  = (AutoCompleteTextView) findViewById(R.id.packagespinner1);
       // packagespinner2  = (EditText) findViewById(R.id.packagespinner2);
        packageedit=(EditText)findViewById(R.id.packageedit);
        orderedit=(Spinner)findViewById(R.id.orderedit);
        Partnameedittext = (AutoCompleteTextView) findViewById(R.id.productnameautoedittext);

        Discountedittext = (EditText) findViewById(R.id.discountedittext);
        numbers=(EditText)findViewById(R.id.numbersedittext);
        colours=(AutoCompleteTextView) findViewById(R.id.coloursedittext);
        Remark = (EditText) findViewById(R.id.remark);
        menu=(ImageView)findViewById(R.id.menubar);
        Order = (Button) findViewById(R.id.order);
        Additems = (Button) findViewById(R.id.add);
        Previewbutton = (Button) findViewById(R.id.preview);
        dealerNameList = new ArrayList<String>();
        pagagelist=new ArrayList<>();


        ordermodelist=new ArrayList<>();
        ordermodelist.add("Select Order Mode");
        ordermodelist.add("phone");
        ordermodelist.add("Direct");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Sales_Order_Activity.this,R.layout.simple_spinner_item,ordermodelist);
        orderedit.setAdapter(adapter);
        orderTypeList=new ArrayList<>();
        orderTypeList.add("Select Order Type");
        orderTypeList.add("Credit Bill");
        orderTypeList.add("Cash Bill");
        orderTypeList.add("Sample");
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(Sales_Order_Activity.this,R.layout.simple_spinner_item,orderTypeList);
        ordertypeSpinner.setAdapter(adapter1);

        Getpermissionforsms();

       orderedit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               ordermode1=parent.getItemAtPosition(position).toString();
               orderMode_S=parent.getItemAtPosition(position).toString();
           }

           @Override
           public void onNothingSelected(AdapterView<?> parent) {

           }
       });
       Packagespinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               pagage=parent.getItemAtPosition(position).toString();
               packageName_S=parent.getItemAtPosition(position).toString();
           }
       });
        colours.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                colours1=parent.getItemAtPosition(position).toString();
                color_S=parent.getItemAtPosition(position).toString();
            }
        });

        Order.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                // TODO Auto-generated method stub
                if(isTableExists())
                {
                    startActivity(new Intent(Sales_Order_Activity.this, com.brainmagic.adherebond.Preview_Activity.class));
                }
                else
                {
                    box.showAlertbox("No Orders Found");
                }
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                final PopupMenu popupMenu = new PopupMenu(Sales_Order_Activity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(Sales_Order_Activity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(Sales_Order_Activity.this, Product_Activity.class);
                                startActivity(product);
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(Sales_Order_Activity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(Sales_Order_Activity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.watsnew:
                                Intent contact1 = new Intent(Sales_Order_Activity.this, com.brainmagic.adherebond.ProjectActivity.class);
                                startActivity(contact1);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(Sales_Order_Activity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {

                                    Intent b = new Intent(Sales_Order_Activity.this, com.brainmagic.adherebond.Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));

                                    Intent b = new Intent(Sales_Order_Activity.this, com.brainmagic.adherebond.Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(Sales_Order_Activity.this).log_out();
                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_login_inner);
                //Whatsnew_Activity.super.onPrepareOptionsMenu(popupMenu);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });
        Partnameedittext.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                productname=parent.getItemAtPosition(position).toString();
                productName_S=parent.getItemAtPosition(position).toString();
            }
        });

        ordertypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ordertype=parent.getItemAtPosition(position).toString();
                orderType_S=parent.getItemAtPosition(position).toString();
            }


            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Additems.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {


                if(customers.equals("Select Customer name")){
                    Toast.makeText(getApplicationContext(), "Select Customer Name", Toast.LENGTH_LONG).show();
                }else if(productname.equals("")){
                    Toast.makeText(getApplicationContext(), "Select Customer Name", Toast.LENGTH_LONG).show();
                }
//                else if(pagage.equals("")){
//                    Toast.makeText(getApplicationContext(), "Select Pacage Name", Toast.LENGTH_LONG).show();
//                }
                else if(Quantityedittext.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Select Quantity Name", Toast.LENGTH_LONG).show();
                }else if(Discountedittext.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Select Discount Name", Toast.LENGTH_LONG).show();
                } else if(ordertype.equals("")){
                    Toast.makeText(getApplicationContext(), "Select Ordertype Name", Toast.LENGTH_LONG).show();
                }else if(colours1.equals("")){
                    Toast.makeText(getApplicationContext(), "Select Colour", Toast.LENGTH_LONG).show();
                }else if(numbers.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Select Number", Toast.LENGTH_LONG).show();
                }else  if(Remark.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Select Remarks", Toast.LENGTH_LONG).show();
                }else{

//                    addItems();

                    new GetDealerMobileNo().execute();  // OLD Local DB
//                    //check();
                    new Additems1().execute();
                }
            }
        });

        dealerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                customers=parent.getItemAtPosition(position).toString();
                customerName_S=parent.getItemAtPosition(position).toString();
            }


            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        new Getproduct().execute();
        NetworkConnection isnet = new NetworkConnection(Sales_Order_Activity.this);
        if (isnet.CheckInternet()) {

            Getpackages();
            GetColours();
        } else {
            box.showAlertbox(getResources().getString(R.string.no_internet));
        }

    }



    private  void addItems(){

        packageEdit_S=packageedit.getText().toString();
        qty_S=Quantityedittext.getText().toString();
        numer_S=numbers.getText().toString();
        basiPrice_S=basicedittext.getText().toString();
        discount_S=discountedittext.getText().toString();
        total_S=totaledittext.getText().toString();
        remarks_S=Remark.getText().toString();

        try {

            final ProgressDialog loading = ProgressDialog.show(Sales_Order_Activity.this, getResources().getString(R.string.app_launcher_name), "Loading...", false, false);
            APIService service = RetroClient.getApiService();
            Call<SalesOrderAdd> call = service.orderadd(mobile_S,"Existing",customerName_S,productName_S,packageName_S,packageEdit_S,qty_S,numer_S,color_S,basiPrice_S,discount_S,total_S,orderMode_S,orderType_S,remarks_S);
            call.enqueue(new Callback<SalesOrderAdd>() {
                @Override

                public void onResponse(Call<SalesOrderAdd> call, Response<SalesOrderAdd> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                           box.showAlertbox("Prodct Item Added Succesfully");
                            Partnameedittext.setText("");
                            Quantityedittext.setText("");
                            Discountedittext.setText("");
                            Remark.setText("");
                        }

                        else
                        {
                            box.showAlertbox(getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }

                @Override
                public void onFailure(Call<SalesOrderAdd> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void GetColours() {

        try {

            APIService service = RetroClient.getApiService();
            Call<ColorsData> call;
            call = service.colors();
            call.enqueue(new Callback<ColorsData>() {
                @Override
                public void onResponse(Call<ColorsData> call, Response<ColorsData> response) {
                    colors1 = response.body().getData();
                    colours.setAdapter(new ArrayAdapter<String>(Sales_Order_Activity.this, android.R.layout.simple_dropdown_item_1line, colors1));
                    colours.setThreshold(1);
                }

                @Override
                public void onFailure(Call<ColorsData> call, Throwable t) {

                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Getpackages() {

        try {

            APIService service = RetroClient.getApiService();
            Call<PackageData> call;
            call = service.package1();
            call.enqueue(new Callback<PackageData>() {
                @Override
                public void onResponse(Call<PackageData> call, Response<PackageData> response) {
                    package1 = response.body().getData();
                    Packagespinner.setAdapter(new ArrayAdapter<String>(Sales_Order_Activity.this, android.R.layout.simple_dropdown_item_1line, package1));
                    Packagespinner.setThreshold(1);
                }

                @Override
                public void onFailure(Call<PackageData> call, Throwable t) {

                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private boolean isTableExists() {
        SQLiteDatabase mDatabase;
        DBHelpersync dbHelp = new DBHelpersync(Sales_Order_Activity.this);
        mDatabase = dbHelp.getWritableDatabase();
        String query = "select DISTINCT tbl_name from sqlite_master where tbl_name = '"+ SQLITE.TABLE_Additems+"'";
        //String query ="select * from "+ SQLITE.TABLE_NewPartsOrder+" order by date asc";
        Cursor cursor = mDatabase.rawQuery(query, null);
        Log.v("preview",query);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                mDatabase.close();
                return true;
            }
            cursor.close();
            mDatabase.close();
        }
        return false;

    }

    private void check() {
        NetworkConnection isnet = new NetworkConnection(Sales_Order_Activity.this);
        if (isnet.CheckInternet()) {
            new Orders().execute();

        } else {

            box.showAlertbox(getResources().getString(R.string.nointernetmsg));
        }
    }


    private void Getpermissionforsms() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.CALL_PHONE)) {
                    Snackbar.make(findViewById(R.id.activity_view_cusomers), "You need to give permission", Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public void onClick(View v) {
                            requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
                        }
                    }).show();
                } else {
                    requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
                }
            } else {
                GetDealerNamesForSpinner1();
            }
        } else
        {
            GetDealerNamesForSpinner1();

        }
    }

    private void GetDealerNamesForSpinner1() {
        dealerNameList =  db.adhereDao().GetCustomer_name_list();
        dealerNameList.add(0,"Select Customer name");
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(Sales_Order_Activity.this,R.layout.simple_spinner_item,dealerNameList);
        spinnerAdapter.setNotifyOnChange(true);
        spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
        dealerSpinner.setAdapter(spinnerAdapter);
    }

    private class Getproduct  extends AsyncTask<String, Void, String>{
        private String productname,productquery;
        private SQLiteDatabase db;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... strings) {

            try

            {
                DBHelper dbhelper = new DBHelper(Sales_Order_Activity.this);
                db = dbhelper.readDataBase();
                productquery = "select distinct Prod_Name from product_details";
                //select distinct Mprod_name,MProd_Img,MProd_ID,Mprod_priority,deleteflag from Mprodcategory where deleteflag = 'notdelete' order by Mprod_priority asc
                Cursor cursor = db.rawQuery(productquery, null);

                if (cursor.moveToFirst()) {
                    do {

                        productlist.add(cursor.getString(cursor.getColumnIndex("Prod_Name")));
                    }
                    while (cursor.moveToNext());
                    cursor.close();
                    return "success";
                } else {
                    return "empty";
                }



            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }

        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            switch (s) {
                case "success":
                    Partnameedittext.setAdapter(new ArrayAdapter<String>(Sales_Order_Activity.this, android.R.layout.simple_dropdown_item_1line, productlist));
                    Partnameedittext.setThreshold(1);

                    break;
                case "empty":
                    break;
                default:
                    break;
            }

        }
    }

    private class Orders extends AsyncTask<String, Void, String> {
        String qty, discount, remarks, query;
        private int a;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            loading = ProgressDialog.show(Sales_Order_Activity.this, "Insert", "Please wait...", false, false);
            qty = Quantityedittext.getText().toString();
            discount = Discountedittext.getText().toString();
            remarks = Remark.getText().toString();

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            try {
                try {
                    query = "insert into mobile_orders(dealername,productname,package,quantity,discount,orderType,remarks,date)" +
                            " values('" + customers + "','" + productname + "','" + pagage + "','" + qty + "','" + discount + "','" + ordertype + "','" + remarks + "','" + formattedDate + "')";
                    ServerConnection server = new ServerConnection();
                    connection = server.getConnection();
                    stmt = connection.createStatement();
                    Log.v("On Line", "SERVER Payment Table");
                    a = stmt.executeUpdate(query);


                    if (a == 0) {

                        stmt.close();
                        connection.close();
                        return "notinserted";
                    }

                    stmt.close();
                    connection.close();
                    return "inserted";

                } catch (Exception e) {
                    Log.v("Error in inserted", "Payment ");
                    Log.v("Error in Payment", e.getMessage());
                    return "notsuccess";
                }


            } catch (Exception e) {
                e.printStackTrace();
                return "notsuccess";

            }

        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            loading.dismiss();
            if (result.equals("inserted")) {

                db.close();
                //box.showAlertbox("Payment updated successfully");
                Toast.makeText(getApplicationContext(), "Item added to Cart", Toast.LENGTH_LONG).show();
                //  Toast.makeText(getApplicationContext(), "Payment Updated", Toast.LENGTH_LONG).show();
                //     new sendemail().execute();

            } else {
                box.showAlertbox("Error in Updating Payment! Try again!");
            }
        }
    }

    private class Additems1 extends AsyncTask<String, Void, String>
     {
         ContentValues values = new ContentValues();
         SQLiteDatabase db;
         private double success;
         private int a;
         private String orderNo;
         public String qty,discount,remarks,unpckage,basics,discounts,totals;

         @SuppressLint("InflateParams")
         @Override
         protected void onPreExecute() {
             super.onPreExecute();
             // instantiate new progress dialog
             progressDialog = new ProgressDialog(Sales_Order_Activity.this);
             // spinner (wheel) style dialog
             progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
             progressDialog.setCancelable(false);
             progressDialog.setMessage("Adding data...");
             progressDialog.show();
             qty = Quantityedittext.getText().toString();
             discount = Discountedittext.getText().toString();
             remarks = Remark.getText().toString();
             Numbers=numbers.getText().toString();
             unpckage=packageedit.getText().toString();
             basics=basicedittext.getText().toString();
             discounts=discountedittext.getText().toString();
             totals=totaledittext.getText().toString();

         }

         @Override
         protected String doInBackground(String... args0) {
             try {
                 values.put( COLUMN_Addexecutive,username);
                 values.put(COLUMN_Addcustomername, customers);
                 values.put(COLUMN_Addproductname, productname);
                 values.put(COLUMN_Addpackage, pagage);
                 values.put(COLUMN_Addquantity, qty);
                 values.put(COLUMN_Adddiscount, discount);
                 values.put(COLUMN_AddOrderType, ordertype);
                 values.put(COLUMN_Addcurrentdate, formattedDate);
                 values.put(COLUMN_AddMobileNO, MobileNO);
                 values.put(COLUMN_Addstring_mail, string_mail);
                 values.put(COLUMN_Addstring_ordermode, ordermode1);
                 values.put(COLUMN_AddRemarks, remarks);
                 values.put(COLUMN_Addstring_ordernumber,Numbers);
                 values.put(COLUMN_Addstring_ordercolors,colours1);


                 values.put(COLUMN_basic,basics);
                 values.put(COLUMN_discountprice,discounts);
                 values.put(COLUMN_total,totals);
                 values.put(COLUMN_Unpackage,unpckage);
                 DBHelpersync dbHelper = new DBHelpersync(Sales_Order_Activity.this);
                 db = dbHelper.getWritableDatabase();
                 dbHelper.CreateAddorderTable(db);
                 success = db.insert(TABLE_Additems, null, values);
                 return "received";
             } catch (Exception e) {
                 Log.v("Error in Add local db ", "catch is working");
                 Log.v("Error in Add local db", e.getMessage());
                 return "notsuccess";
             }
         }

         @Override
         protected void onPostExecute(String result) {
             super.onPostExecute(result);
             progressDialog.dismiss();
             if (result.equals("received")) {
                 Log.v("success", Double.toString(success));
                /*startActivity(new Intent(Sales_Order_Activity.this,Sales_Order_Activity.class));
                finish();*/
                 //box.showAlertbox("Item Added to Cart");
                 Partnameedittext.setText("");
                 Quantityedittext.setText("");
                 Discountedittext.setText("");
                 Remark.setText("");

                 Toast.makeText(getApplicationContext(), "Item added to Cart", Toast.LENGTH_LONG).show();
             } else {
                 box.showAlertbox("Error in adding ");
             }
         }
    }

    private  class GetDealerMobileNo extends AsyncTask<String, Void, String>
    {

        @Override
        protected String doInBackground(String... params) {
            try
            {
                String query ="select * from Customer where CustomerCode ='"+ Customercode +"'";;
                Log.v("DealerNo",query);
                Cursor cursor = db1.rawQuery(query, null);
                if(cursor.moveToFirst())
                {
                    do
                    {
                        MobileNO = cursor.getString(cursor.getColumnIndex("Mobile"));
                        string_mail = cursor.getString(cursor.getColumnIndex("EmailId"));
                        BranchCode = cursor.getString(cursor.getColumnIndex("BranchCode"));

                    } while (cursor.moveToNext());

                }
                cursor.close();

                Log.v("OFF Line", "SQLITE Table");

                return "received";

            }catch (Exception e)
            {
                Log.v("Error in get date","catch is working");
                Log.v("Error in Incentive", e.getMessage());
                return "notsuccess";
            }
        }
        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            //Priceedittext.setText(getResources().getString(R.string.Rs).toString() +" "+partsRateList);
        }

    }


}
