package com.brainmagic.adherebond;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;


import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;

import com.brainmagic.adherebond.R;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import Logout.logout;
import about.About_Activity;
import adapter.CuslistAdapter;
import adapter.CuslistAdapter2;
import alertbox.Alertbox;
import api.models.master.Customer_list;
import contact.Contact_Activity;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import product.Product_Activity;
import roomdb.database.AppDatabase;
import search.Search_Activity;
import static roomdb.database.AppDatabase.getAppDatabase;

public class View_Customers_Activity extends AppCompatActivity {


    private static final int PERMISSION_REQUEST = 100;
    private List<String> Namelist,CuscodeList,BranchcodeList,EmailList,MobileList,CusNamelist,CustomercodeList;
    ImageView home,back,menu;
    ListView gridview;
    HorizontalScrollView horizontalScrollView;

    Spinner Customer_spinner;
    Button Search;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    String SalesName,salesID,Customername,Code;
    Integer Position;
    NetworkConnection network = new NetworkConnection(View_Customers_Activity.this);
    Alertbox alert = new Alertbox(View_Customers_Activity.this);
    private AppDatabase db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view__customers_);

        myshare = getSharedPreferences("Registration", View_Customers_Activity.this.MODE_PRIVATE);
        editor = myshare.edit();
        salesID = myshare.getString("salesID", "");
        SalesName = myshare.getString("name", "");

        gridview = (ListView) findViewById(R.id.customers_listview);
        Customer_spinner = (Spinner)findViewById(R.id.customer_spinner);
        Search = (Button)findViewById(R.id.search);
        // initial views
        home = (ImageView) findViewById(R.id.home);
        back = (ImageView) findViewById(R.id.back);
        db = getAppDatabase(View_Customers_Activity.this);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(View_Customers_Activity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);

            }
        });
        //checkInternet();
        //menu=(ImageView)findViewById(R.id.menubar);



        Namelist = new ArrayList<String>();
        CuscodeList = new ArrayList<String>();
        BranchcodeList = new ArrayList<String>();
        EmailList = new ArrayList<String>();
        MobileList = new ArrayList<String>();
        CusNamelist = new ArrayList<String>();
        CustomercodeList = new ArrayList<String>();





        //checkInternet();
        menu=(ImageView)findViewById(R.id.menubar);

        menu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                final PopupMenu popupMenu = new PopupMenu(View_Customers_Activity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(View_Customers_Activity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(View_Customers_Activity.this, Product_Activity.class);
                                startActivity(product);
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(View_Customers_Activity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(View_Customers_Activity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.watsnew:
                                Intent contact1 = new Intent(View_Customers_Activity.this, com.brainmagic.adherebond.ProjectActivity.class);
                                startActivity(contact1);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(View_Customers_Activity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {

                                    Intent b = new Intent(View_Customers_Activity.this, com.brainmagic.adherebond.Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));

                                    Intent b = new Intent(View_Customers_Activity.this, com.brainmagic.adherebond.Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(View_Customers_Activity.this).log_out();
                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_login_inner);
                //Whatsnew_Activity.super.onPrepareOptionsMenu(popupMenu);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });
        Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Customername = Customer_spinner.getSelectedItem().toString();
                if (Customername.equals("All")) {
                     customers();
                } else {
                    selectedcustomers(Customername);
                }

            }
        });

        Getpermissionforsms();

    }

    private void selectedcustomers(String customername) {
        List<Customer_list> customer_lists =  db.adhereDao().GetCustomer_list_Name(customername);
        CuslistAdapter2 adapter = new CuslistAdapter2(View_Customers_Activity.this, customer_lists);
        gridview.setAdapter(adapter);
    }

    private void customers() {
        List<Customer_list> customer_lists =  db.adhereDao().GetCustomer_list();
        CuslistAdapter2 adapter = new CuslistAdapter2(View_Customers_Activity.this, customer_lists);
        gridview.setAdapter(adapter);
    }

    private void Getpermissionforsms() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.CALL_PHONE)) {
                    Snackbar.make(findViewById(R.id.activity_view_cusomers), "You need to give permission", Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public void onClick(View v) {
                            requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
                        }
                    }).show();
                } else {
                    requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
                }
            } else {
                 GetDealerNamesForSpinner();
            }
        } else
        {
             GetDealerNamesForSpinner();

        }

    }

    private void GetDealerNamesForSpinner() {

        Namelist =  db.adhereDao().GetCustomer_name_list();
        Namelist.add(0,"All");
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(View_Customers_Activity.this,R.layout.simple_spinner_item,Namelist);
        spinnerAdapter.setNotifyOnChange(true);
        spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
        Customer_spinner.setAdapter(spinnerAdapter);

    }
}
