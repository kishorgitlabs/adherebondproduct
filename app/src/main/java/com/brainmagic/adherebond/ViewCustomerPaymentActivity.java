package com.brainmagic.adherebond;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;


import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;

import com.brainmagic.adherebond.R;
import com.google.android.material.snackbar.Snackbar;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Logout.logout;
import about.About_Activity;
import adapter.CuslistAdapter;
import alertbox.Alertbox;
import api.models.master.Customer_list;
import contact.Contact_Activity;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import persistency.ServerConnection;
import product.Product_Activity;
import roomdb.database.AppDatabase;
import search.Search_Activity;

import static android.net.wifi.WifiConfiguration.Status.strings;
import static roomdb.database.AppDatabase.getAppDatabase;

public class ViewCustomerPaymentActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUEST = 100;
//    private List<String> Namelist, CuscodeList, BranchcodeList, EmailList, MobileList, CusNamelist, CustomercodeList;
    ImageView home, back, menu;
    ListView gridview;
    HorizontalScrollView horizontalScrollView;

    Spinner Customer_spinner;
    Button Search;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    String SalesName, salesID, Customername, Code,query;
    Integer Position;
    NetworkConnection network = new NetworkConnection(ViewCustomerPaymentActivity.this);
    Alertbox alert = new Alertbox(ViewCustomerPaymentActivity.this);
    private AppDatabase db;
    private ProgressDialog loading;
    private Connection connection;
    private Statement statement;
    private SQLiteDatabase db1;
    private ResultSet resultSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_customer_payment);
        myshare = getSharedPreferences("Registration", ViewCustomerPaymentActivity.this.MODE_PRIVATE);
        editor = myshare.edit();
        salesID = myshare.getString("salesID", "");
        SalesName = myshare.getString("name", "");

        gridview = (ListView) findViewById(R.id.customers_listview);
        Customer_spinner = (Spinner) findViewById(R.id.customer_spinner);
        Search = (Button) findViewById(R.id.search);
        // initial views
        home = (ImageView) findViewById(R.id.home);
        back = (ImageView) findViewById(R.id.back);
        db = getAppDatabase(ViewCustomerPaymentActivity.this);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(ViewCustomerPaymentActivity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);

            }
        });
        //checkInternet();
        //menu=(ImageView)findViewById(R.id.menubar);

//
//        Namelist = new ArrayList<String>();
//        CuscodeList = new ArrayList<String>();
//        BranchcodeList = new ArrayList<String>();
//        EmailList = new ArrayList<String>();
//        MobileList = new ArrayList<String>();
//        CusNamelist = new ArrayList<String>();
//        CustomercodeList = new ArrayList<String>();


        //checkInternet();
        menu = (ImageView) findViewById(R.id.menubar);

        menu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                final PopupMenu popupMenu = new PopupMenu(ViewCustomerPaymentActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(ViewCustomerPaymentActivity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(ViewCustomerPaymentActivity.this, Product_Activity.class);
                                startActivity(product);
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(ViewCustomerPaymentActivity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(ViewCustomerPaymentActivity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.watsnew:
                                Intent contact1 = new Intent(ViewCustomerPaymentActivity.this, com.brainmagic.adherebond.ProjectActivity.class);
                                startActivity(contact1);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(ViewCustomerPaymentActivity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin", false)) {

                                    Intent b = new Intent(ViewCustomerPaymentActivity.this, com.brainmagic.adherebond.Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ", Boolean.toString(myshare.getBoolean("islogin", false)));
                                } else {
                                    Log.v("is  Login ", Boolean.toString(myshare.getBoolean("islogin", false)));

                                    Intent b = new Intent(ViewCustomerPaymentActivity.this, com.brainmagic.adherebond.Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(ViewCustomerPaymentActivity.this).log_out();
                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_login_inner);
                //Whatsnew_Activity.super.onPrepareOptionsMenu(popupMenu);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin", false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });
        /*Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Customername = Customer_spinner.getSelectedItem().toString();
                if (Customername.equals("All")) {
                    customers();
                } else {
                    selectedcustomers(Customername);
                }

            }
        });*/
        Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Customername = Customer_spinner.getSelectedItem().toString();
                if (Customername.equals("All")) {
                    new customers().execute();
                } else {
                     new SelectedCustomer().execute("Select Name,Mobile,EmailId From Customer where Name = '"+Customername+"'");
                }
            }
        });
        Getpermissionforsms();

    }

   /* private void selectedcustomers(String customername) {
        List<Customer_list> customer_lists = db.adhereDao().GetCustomer_list_Name(customername);
        CuslistAdapter adapter = new CuslistAdapter(ViewCustomerPaymentActivity.this, customer_lists);
        gridview.setAdapter(adapter);
    }

    private void customers() {
        List<Customer_list> customer_lists = db.adhereDao().GetCustomer_list();
        CuslistAdapter adapter = new CuslistAdapter(ViewCustomerPaymentActivity.this, customer_lists);
        gridview.setAdapter(adapter);
    }*/

    private void Getpermissionforsms() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.CALL_PHONE)) {
                    Snackbar.make(findViewById(R.id.activity_view_cusomers), "You need to give permission", Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public void onClick(View v) {
                            requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
                        }
                    }).show();
                } else {
                    requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
                }
            } else {
                GetDealerNamesForSpinner();
            }
        } else {
            GetDealerNamesForSpinner();

        }

    }

    private void GetDealerNamesForSpinner() {
             new Names().execute();
       // Namelist = db.adhereDao().GetCustomer_name_list();


    }

    private class customers extends AsyncTask<String, Void, String> {
        private int a;
        private List<String> Namelist, CuscodeList, BranchcodeList, EmailList, MobileList, CusNamelist, CustomercodeList;
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            Namelist = new ArrayList<String>();
            CuscodeList = new ArrayList<String>();
            BranchcodeList = new ArrayList<String>();
            EmailList = new ArrayList<String>();
            MobileList = new ArrayList<String>();
            CusNamelist = new ArrayList<String>();
            CustomercodeList = new ArrayList<String>();

            loading = ProgressDialog.show(ViewCustomerPaymentActivity.this, "Adherebonds", "Please wait...", false, false);

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            try {
                try {
                    ServerConnection server = new ServerConnection();
                    connection = server.getConnection();
                    statement = connection.createStatement();
                    query = "Select Name,Mobile,EmailId From Customer";
                    Log.v("customer ", query);
                    Log.v("On Line", "SERVER Payment Table");
                    resultSet = statement.executeQuery(query);
                    while (resultSet.next()){
                        Namelist.add(resultSet.getString("Name"));
                        MobileList.add(resultSet.getString("Mobile"));
                        EmailList.add(resultSet.getString("EmailId"));
                    }
                    a = statement.executeUpdate(query);


                    if (!Namelist.isEmpty()) {
                        statement.close();
                        connection.close();
                        resultSet.close();
                        return "success";
                    } else {
                        statement.close();
                        connection.close();
                        resultSet.close();
                        return "empty";
                    }
                } catch (Exception e) {
                    Log.v("Error in inserted", "Payment ");
                    Log.v("Error in Payment", e.getMessage());
                    return "notsuccess";
                }


            } catch (Exception e) {
                e.printStackTrace();
                return "notsuccess";

            }


        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            loading.dismiss();
            CuslistAdapter adapter = new CuslistAdapter(ViewCustomerPaymentActivity.this, Namelist,MobileList,EmailList);
            gridview.setAdapter(adapter);

        }
    }



    private class SelectedCustomer extends AsyncTask<String, Void, String>{


        private int a;
        private List<String> Namelist, CuscodeList, BranchcodeList, EmailList, MobileList, CusNamelist, CustomercodeList;
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            Namelist = new ArrayList<String>();
            CuscodeList = new ArrayList<String>();
            BranchcodeList = new ArrayList<String>();
            EmailList = new ArrayList<String>();
            MobileList = new ArrayList<String>();
            CusNamelist = new ArrayList<String>();
            CustomercodeList = new ArrayList<String>();
            loading = ProgressDialog.show(ViewCustomerPaymentActivity.this, "Adherebonds", "Please wait...", false, false);

        }
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            try {
                try {
                    ServerConnection server = new ServerConnection();
                    connection = server.getConnection();
                    statement = connection.createStatement();
                    String query=params[0];
                    resultSet = statement.executeQuery(query);
                    while (resultSet.next()){
                        Namelist.add(resultSet.getString("Name"));
                        MobileList.add(resultSet.getString("Mobile"));
                        EmailList.add(resultSet.getString("EmailId"));
                    }


                    if (!Namelist.isEmpty()) {
                        statement.close();
                        connection.close();
                        resultSet.close();
                        return "success";
                    } else {
                        statement.close();
                        connection.close();
                        resultSet.close();
                        return "empty";
                    }
                } catch (Exception e) {
                    Log.v("Error in inserted", "Payment ");
                    Log.v("Error in Payment", e.getMessage());
                    return "notsuccess";
                }


            } catch (Exception e) {
                e.printStackTrace();
                return "notsuccess";

            }


        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            loading.dismiss();
            CuslistAdapter adapter = new CuslistAdapter(ViewCustomerPaymentActivity.this, Namelist,MobileList,EmailList);
            gridview.setAdapter(adapter);

        }
    }

    private class Names extends AsyncTask<String, Void ,String> {
        private int a;
        private List<String> Namelist;
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            loading = ProgressDialog.show(ViewCustomerPaymentActivity.this, "Adherebonds", "Please wait...", false, false);
            Namelist = new ArrayList<String>();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            try {
                try {
                    ServerConnection server = new ServerConnection();
                    connection = server.getConnection();
                    statement = connection.createStatement();
                    query = "Select Name,Mobile,EmailId From Customer";
                    Log.v("customer ", query);
                    Log.v("On Line", "SERVER Payment Table");
                    resultSet = statement.executeQuery(query);
                    while (resultSet.next()) {
                        Namelist.add(resultSet.getString("Name"));

                    }
                    a = statement.executeUpdate(query);


                    if (!Namelist.isEmpty()) {
                        statement.close();
                        connection.close();
                        resultSet.close();
                        return "success";
                    } else {
                        statement.close();
                        connection.close();
                        resultSet.close();
                        return "empty";
                    }
                } catch (Exception e) {
                    Log.v("Error in inserted", "Payment ");
                    Log.v("Error in Payment", e.getMessage());
                    return "notsuccess";
                }


            } catch (Exception e) {
                e.printStackTrace();
                return "notsuccess";

            }

        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            loading.dismiss();
            if(result.equals("notsuccess")){
                Namelist.add(0, "All");
                ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(ViewCustomerPaymentActivity.this, R.layout.simple_spinner_item, Namelist);
                spinnerAdapter.setNotifyOnChange(true);
                spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
                Customer_spinner.setAdapter(spinnerAdapter);
            }


        }
    }
}

