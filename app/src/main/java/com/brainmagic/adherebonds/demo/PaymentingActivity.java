package com.brainmagic.adherebond;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.StrictMode;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.adherebond.R;
import com.google.android.material.snackbar.Snackbar;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import Logout.logout;
import SharedPreference.Shared;
import about.About_Activity;
import alertbox.AlertDialogue;
import alertbox.Alertbox;
import api.models.payment.Payment;
import api.models.payment.PaymentData;
import api.models.registration.Register;
import api.models.registration.RegisterData;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import contact.Contact_Activity;
import database.DBHelpersync;
import email.CeraMail;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import persistency.ServerConnection;
import product.Product_Activity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import roomdb.database.AppDatabase;
import search.Search_Activity;

import static com.brainmagic.adherebond.Preview_Activity.capitalizeString;
import static roomdb.database.AppDatabase.getAppDatabase;

public class PaymentingActivity extends AppCompatActivity {
    private static final int PERMISSION_REQUEST = 100;
    ImageView home, back;
    NetworkConnection network = new NetworkConnection(PaymentingActivity.this);
    private Alertbox box = new Alertbox(PaymentingActivity.this);
    private AlertDialogue alert = new AlertDialogue(PaymentingActivity.this);
    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;
    public Statement stmt;
    private TableLayout Duelayout;
    public ResultSet reset;
    public String MobileNO;
    private ImageView menu;
    public String amount = "";
    public String emailad, CustomerName, string_mail;
    public String cmpnymsg;
    private Spinner customerspinner, paymentspinner,duespinner;
    private  EditText chbankspinner;
    private EditText cashedittext, billedittext, paidedittext, cpaidhedittext, chcashedittext;
    private Spinner  remaedit;
    private ArrayList<String>reanrklist;
    private static EditText chnumberedittext, chdateedittext, dueedittext;
    private TableLayout chequelayout, cashlayout, paidlayout;
    private TableRow  duetype1;

    private List<String> dealerNameList, dealerID, DealerMobile, DelearEmail;
    private ArrayList<String> paymenttypeList, banknamelist,duelist;
    private String formattedDate;
    private Button Submit;
    public static String item_date;
    private String dealerName;
    public String dcode;
    public String item,remarks1;
    private AppDatabase db;
    private SQLiteDatabase db1;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String customername, bankName, SalesEmailSign, SalesPaymentemail, SalesPaymentmobile, ReplyEmial, SalesEmial, SalesPass, SalesName,selectdue;
    private ProgressDialog loading;
    private static  String fromstring;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paymenting);
        myshare = getSharedPreferences(Shared.MyPREFERENCES, PaymentingActivity.this.MODE_PRIVATE);
        editor = myshare.edit();
        customerspinner = (Spinner) findViewById(R.id.customerspinner);
        paymentspinner = (Spinner) findViewById(R.id.paymentspinner);
        duespinner = (Spinner) findViewById(R.id.duespinner);
        cashedittext = (EditText) findViewById(R.id.cashedittext);
        billedittext = (EditText) findViewById(R.id.billedittext);
        paidedittext = (EditText) findViewById(R.id.paidedittext);
        dueedittext = (EditText) findViewById(R.id.dueedittext);
        remaedit = (Spinner) findViewById(R.id.remaedit);
        chnumberedittext = (EditText) findViewById(R.id.chnumberedittext);
        chdateedittext = (EditText) findViewById(R.id.chdateedittext);
        chcashedittext = (EditText) findViewById(R.id.chcashedittext);
        cpaidhedittext = (EditText) findViewById(R.id.cpaidhedittext);
        chbankspinner = (EditText) findViewById(R.id.chbankspinner);
        chequelayout = (TableLayout) findViewById(R.id.chequelayout);
        cashlayout = (TableLayout) findViewById(R.id.cashlayout);
        paidlayout = (TableLayout) findViewById(R.id.paidlayout);
        Duelayout = (TableLayout) findViewById(R.id.Duelayout);
        duetype1 = (TableRow) findViewById(R.id.duetype1);
        Submit = (Button) findViewById(R.id.submit);
        menu=(ImageView)findViewById(R.id.menubar);
        db = getAppDatabase(PaymentingActivity.this);
        SalesName = myshare.getString(Shared.K_Sales_name, "");
        ReplyEmial = myshare.getString("email", "");
        //SalesEmial = myshare.getString("email", "");
        //SalesPass = myshare.getString("emailpass", "");
        SalesEmailSign = myshare.getString("signature", "");

        SalesPaymentemail = myshare.getString("paymentemail", "");
        SalesPaymentmobile = myshare.getString("paymentsms", "");
        SalesEmial = "karthikeyan@brainmagic.info";
        SalesPass = "Ntq4p#$00";
        myshare = getSharedPreferences("Registration", PaymentingActivity.this.MODE_PRIVATE);
        editor = myshare.edit();

        SalesName = myshare.getString("name", "");
        paymenttypeList = new ArrayList<>();
        paymenttypeList.add("Select Payment type");
        paymenttypeList.add("Cash");
        paymenttypeList.add("Cheque");
        ArrayAdapter<String> spinnerAdapter2 = new ArrayAdapter<String>(PaymentingActivity.this, R.layout.simple_spinner_item, paymenttypeList);
        spinnerAdapter2.setDropDownViewResource(R.layout.simple_spinner_item);
        paymentspinner.setAdapter(spinnerAdapter2);

        reanrklist=new ArrayList<>();
        reanrklist.add("Select Remarks");
        reanrklist.add("Collection");
        reanrklist.add("Next Visit");
        reanrklist.add("Material Return");
        reanrklist.add("Complaint");
        ArrayAdapter<String> spinnerAdapter1 = new ArrayAdapter<String>(PaymentingActivity.this, R.layout.simple_spinner_item, reanrklist);
        spinnerAdapter1.setDropDownViewResource(R.layout.simple_spinner_item);
        remaedit.setAdapter(spinnerAdapter1);

        duelist=new ArrayList<>();
        duelist.add("Select Due");
        duelist.add("NO DUE");
        duelist.add("DUE");
        ArrayAdapter<String>spinneradapter5=new ArrayAdapter<String>(PaymentingActivity.this,R.layout.simple_spinner_item,duelist);
        spinneradapter5.setDropDownViewResource(R.layout.simple_spinner_item);
        duespinner.setAdapter(spinneradapter5);



       duespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               selectdue= parent.getItemAtPosition(position).toString();
               if(selectdue.equals("Select Due")){

               }else if(selectdue.equals("NO DUE")){
                   Duelayout.setVisibility(View.GONE);
               }else if(selectdue.equals("DUE")) {
                   Duelayout.setVisibility(View.VISIBLE);
               }
            /*   switch (selectdue){
                   case "NO DUE":
                       Duelayout.setVisibility(View.GONE);
                   case "DUE":
                       Duelayout.setVisibility(View.VISIBLE);
                   default:
                       Duelayout.setVisibility(View.GONE);


               }*/
           }

           @Override
           public void onNothingSelected(AdapterView<?> adapterView) {

           }

       });


/*
        banknamelist = new ArrayList<>();
        banknamelist.add("Select Bank Name");
        banknamelist.add("Karur vysya Bank");
        banknamelist.add("Allahabad bank");
        banknamelist.add("Indian Bank");
        banknamelist.add("Tamilnadu Mercantile bank");
        banknamelist.add("South Indian Bank");
        banknamelist.add("City Union Bank ");
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(PaymentingActivity.this, R.layout.simple_spinner_item, banknamelist);
        spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
        chbankspinner.setAdapter(spinnerAdapter);*/

        dealerNameList = new ArrayList<String>();
        dealerID = new ArrayList<String>();
        dealerNameList.add("Select Customer Name");

        Date d = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(d);

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        formattedDate = df.format(c.getTime());
        DBHelpersync dbHelper = new DBHelpersync(PaymentingActivity.this);
        db1 = dbHelper.getWritableDatabase();

        Submit = (Button) findViewById(R.id.submit);

        home = (ImageView) findViewById(R.id.home);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(PaymentingActivity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });
        remaedit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                remarks1=parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        menu.setOnClickListener(new View.OnClickListener() {


            public void onClick(View view) {


                final PopupMenu popupMenu = new PopupMenu(PaymentingActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(PaymentingActivity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(PaymentingActivity.this, Product_Activity.class);
                                startActivity(product);
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(PaymentingActivity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(PaymentingActivity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(PaymentingActivity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {

                                    Intent b = new Intent(PaymentingActivity.this, com.brainmagic.adherebond.Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));

                                    Intent b = new Intent(PaymentingActivity.this, com.brainmagic.adherebond.Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(PaymentingActivity.this).log_out();
                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_login_inner);
                //Whatsnew_Activity.super.onPrepareOptionsMenu(popupMenu);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });
      /*  chbankspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bankName = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/
        menu = (ImageView) findViewById(R.id.menubar);
        menu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                final PopupMenu popupMenu = new PopupMenu(PaymentingActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(PaymentingActivity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(PaymentingActivity.this, Product_Activity.class);
                                startActivity(product);
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(PaymentingActivity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(PaymentingActivity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.watsnew:
                                Intent contact1 = new Intent(PaymentingActivity.this, com.brainmagic.adherebond.ProjectActivity.class);
                                startActivity(contact1);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(PaymentingActivity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin", false)) {

                                    Intent b = new Intent(PaymentingActivity.this, com.brainmagic.adherebond.Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ", Boolean.toString(myshare.getBoolean("islogin", false)));
                                } else {
                                    Log.v("is  Login ", Boolean.toString(myshare.getBoolean("islogin", false)));

                                    Intent b = new Intent(PaymentingActivity.this, com.brainmagic.adherebond.Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(PaymentingActivity.this).log_out();
                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_login_inner);
                //Whatsnew_Activity.super.onPrepareOptionsMenu(popupMenu);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin", false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });
        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!customername.equals("Select Customer Name")) {
                    if (item.equals("Select Payment type")) {
                        Toast.makeText(PaymentingActivity.this, "Select Payment type ", Toast.LENGTH_LONG).show();
                    }
                    if (item.equals("Cash")) {
                        if (cashedittext.length() <= 0) {
                            cashedittext.setError("Enter Cash Amount");
                        } else if (billedittext.getText().toString().equals("")) {
                            billedittext.setError("Enter Bill Amount");
                        } else if (paidedittext.getText().toString().equals("")) {
                            paidedittext.setError("Enter paid Amount");
                        } else if (dueedittext.getText().toString().equals("")) {
                            paidedittext.setError("Enter due date");
                        } else if (remaedit.equals("")) {
                            Toast.makeText(getApplicationContext(), "Enter remarks", Toast.LENGTH_LONG).show();
                        } else {
                            CustomerName = customerspinner.getSelectedItem().toString();
                           // CheckNetworkforsave();
                            checkinternet();
                        }
                    } else {
                        if (chnumberedittext.length() <= 0) {
                            chnumberedittext.setError("Enter Check Number");
                        } else if (chdateedittext.getText().toString().equals("")) {
                            chdateedittext.setError("Enter Check Date");
                        } else if (cpaidhedittext.getText().toString().equals("")) {
                            cpaidhedittext.setError("Enter check paid Amount");
                        } else if (chcashedittext.getText().toString().equals("")) {
                            chcashedittext.setError("Enter Cheque Amount");
                        } else if (remaedit.equals("")) {
                            Toast.makeText(getApplicationContext(), "Enter remarks", Toast.LENGTH_LONG).show();
                        }  else {
                            CustomerName = customerspinner.getSelectedItem().toString();
                           // CheckNetworkforsave();
                            checkinternet();
                        }
                    }
                } else {
                    Toast.makeText(PaymentingActivity.this, "Select Customer Name", Toast.LENGTH_LONG).show();
                }

            }
        });
        customerspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long item) {
                customername = parent.getItemAtPosition(position).toString();

                if (customername.equals("Select Customer Name")) {
                    //Toast.makeText(getApplicationContext(), "Select Customer Name", Toast.LENGTH_LONG).show();
                } else {
                    // dcode = dealerID.get(position - 1);
                    new GetDealerMobileNo().execute();
                    //	Log.v("Mobile no", MobileNO);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        dueedittext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new FromDatePickerFragment1();
                newFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });
        chdateedittext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new FromDatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });


        paymentspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1,
                                       int position, long arg3) {
                // TODO Auto-generated method stub

                item = parent.getItemAtPosition(position).toString();
                item_date = parent.getItemAtPosition(position).toString();
                if (item.equals("Select Payment type")) {

                } else if (item.equals("Cash")) {
                    customerspinner.setVisibility(View.VISIBLE);
                    paymentspinner.setVisibility(View.VISIBLE);
                    chequelayout.setVisibility(View.GONE);
                    cashedittext.setVisibility(View.VISIBLE);
                    billedittext.setVisibility(View.VISIBLE);
                    paidedittext.setVisibility(View.VISIBLE);
                    duetype1.setVisibility(View.GONE);
                    //dueedittext.setVisibility(View.VISIBLE);
                    remaedit.setVisibility(View.VISIBLE);
                } else if (item.equals("Cheque")) {
                    customerspinner.setVisibility(View.VISIBLE);
                    paymentspinner.setVisibility(View.VISIBLE);
                    chequelayout.setVisibility(View.VISIBLE);
                    cashlayout.setVisibility(View.GONE);
                    paidlayout.setVisibility(View.GONE);
                    billedittext.setVisibility(View.VISIBLE);
                    paidedittext.setVisibility(View.GONE);
                    dueedittext.setVisibility(View.VISIBLE);
                    remaedit.setVisibility(View.VISIBLE);
                    duetype1.setVisibility(View.VISIBLE);
                    //new GetBankNames().execute();
                } else {
                    customerspinner.setVisibility(View.GONE);
                    paymentspinner.setVisibility(View.GONE);
                    chequelayout.setVisibility(View.GONE);
                    cashlayout.setVisibility(View.GONE);
                    paidlayout.setVisibility(View.GONE);
                    billedittext.setVisibility(View.GONE);
                    paidedittext.setVisibility(View.GONE);
                    dueedittext.setVisibility(View.GONE);
                    remaedit.setVisibility(View.GONE);
                    duetype1.setVisibility(View.GONE);
                    //new GetBankNames().execute();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });


        Getpermissionforsms();

     /*   customerspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1,
                                       int position, long arg3) {
                // TODO Auto-generated method stub
                dealerName = parent.getItemAtPosition(position).toString();

                if (dealerName.equals("Select Customer Name")) {
                    //Toast.makeText(getApplicationContext(), "Select Customer Name", Toast.LENGTH_LONG).show();
                } else {
                    dcode = dealerID.get(position - 1);
                    new GetDealerMobileNo().execute();
                    //	Log.v("Mobile no", MobileNO);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });*/
        new GetDealerNamesForSpinner().execute();


    }

    private void checkinternet() {
        NetworkConnection network = new NetworkConnection(PaymentingActivity.this);
        if (network.CheckInternet()) {
            // new verifylogin().execute();
            //askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, WRITE_EXST);
            paymentcollection();
        } else {
            Alertbox alert = new Alertbox(PaymentingActivity.this);
            alert.showAlertbox("Kindly check your Internet Connection");
        }
    }

    private void paymentcollection() {
        try {

            final ProgressDialog loading = ProgressDialog.show(PaymentingActivity.this, getResources().getString(R.string.app_launcher_name), "Loading...", false, false);
            APIService service = RetroClient.getApiService();
            Call<PaymentData> call = service.payment(
                    SalesName,
                    customername,
                    item,
                    remarks1,
                    billedittext.getText().toString(),
                    cashedittext.getText().toString(),
                    paidedittext.getText().toString(),
                    chnumberedittext.getText().toString(),
                    chdateedittext.getText().toString(),
                    chbankspinner.getText().toString(),
                    dueedittext.getText().toString()
                    );
            call.enqueue(new Callback<PaymentData>() {
                @Override

                public void onResponse(Call<PaymentData> call, Response<PaymentData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            SuccessRegister(response.body().getData());
                        } else if(response.body().getResult().equals("NotSuccess")) {
                            box.showAlertbox(getString(R.string.server_error));
                        }else {
                            box.showAlertbox(getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }

                @Override
                public void onFailure(Call<PaymentData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }





    }

    private void SuccessRegister(Payment data) {

        final AlertDialog alertDialogbox = new AlertDialog.Builder(
                PaymentingActivity.this).create();

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertbox, null);
        alertDialogbox.setView(dialogView);

        TextView log = (TextView) dialogView.findViewById(R.id.textView1);
        Button okay = (Button) dialogView.findViewById(R.id.okay);
        log.setText("Payment Successfully");
        okay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent go = new Intent(PaymentingActivity.this, com.brainmagic.adherebond.Payments_Activity.class);
                go.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(go);
                alertDialogbox.dismiss();
                finish();
            }
        });
        alertDialogbox.show();
    }





    private void CheckNetworkforsave() {

        NetworkConnection isnet = new NetworkConnection(PaymentingActivity.this);
        if (isnet.CheckInternet()) {
            new Insertpayment().execute();


        } else {

            box.showAlertbox(getResources().getString(R.string.nointernetmsg));
        }
    }


    private void Getpermissionforsms() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.CALL_PHONE)) {
                    Snackbar.make(findViewById(R.id.activity_view_cusomers), "You need to give permission", Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public void onClick(View v) {
                            requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
                        }
                    }).show();
                } else {
                    requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
                }
            } else {
                GetDealerNamesForSpinner1();
            }
        } else {
            GetDealerNamesForSpinner1();

        }

    }

    private void GetDealerNamesForSpinner1() {
        dealerNameList = db.adhereDao().GetCustomer_name_list();
        dealerNameList.add(0, "Select Customer name");
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(PaymentingActivity.this, R.layout.simple_spinner_item, dealerNameList);
        spinnerAdapter.setNotifyOnChange(true);
        spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
        customerspinner.setAdapter(spinnerAdapter);
    }


    @SuppressLint("ValidFragment")
    public static class FromDatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {
        DecimalFormat mFormat= new DecimalFormat("00");


        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);


            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            chdateedittext.setText(mFormat.format(day) + "/" + mFormat.format(month + 1) + "/" + year);
            //visitdate.setText(String.format("%d/%d/%d",mFormat.format(day) ,(month + 1) , year));
            fromstring = year + "-" + (month + 1) + "-" + day;
            chdateedittext.clearFocus();

        }
    }

    @SuppressLint("ValidFragment")
    public static class FromDatePickerFragment1 extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {
        DecimalFormat mFormat= new DecimalFormat("00");


        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);


            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            dueedittext.setText(mFormat.format(day) + "/" + mFormat.format(month + 1) + "/" + year);
            //visitdate.setText(String.format("%d/%d/%d",mFormat.format(day) ,(month + 1) , year));
            fromstring = year + "-" + (month + 1) + "-" + day;
            dueedittext.clearFocus();

        }
    }

    private class GetDealerMobileNo extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                String query = "select * from Customer where Name ='" + customername + "'";
                Log.v("DealerNo", query);
                Cursor cursor = db1.rawQuery(query, null);
                if (cursor.moveToFirst()) {
                    do {
                        MobileNO = cursor.getString(cursor.getColumnIndex("Mobile"));
                        string_mail = cursor.getString(cursor.getColumnIndex("EmailId"));
                    } while (cursor.moveToNext());

                }
                cursor.close();

                Log.v("OFF Line", "SQLITE Table");

                return "received";

            } catch (Exception e) {
                Log.v("Error in get date", "catch is working");
                Log.v("Error in Incentive", e.getMessage());
                return "notsuccess";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            //Priceedittext.setText(getResources().getString(R.string.Rs).toString() +" "+partsRateList);
        }

    }

    public class GetDealerNamesForSpinner extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            try {
                Log.v("OFF Line", "SQLITE Table");

                String query = "select distinct Name,CustomerCode from Customer where SalesPersonName='" + SalesName + "'";
                Log.v("Dealer Name", query);
                Cursor cursor = db1.rawQuery(query, null);
                if (cursor.moveToFirst()) {
                    do {
                        dealerNameList.add(capitalizeString(cursor.getString(cursor.getColumnIndex("Name"))));
                        //  dealerID.add(cursor.getString(cursor.getColumnIndex("CustomerCode")));

                    } while (cursor.moveToNext());
                    cursor.close();
                }
                return "received";

            } catch (Exception e) {
                Log.v("Error in get date", "catch is working");
                Log.v("Error in Incentive", e.getMessage());
                return "notsuccess";
            }

        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(PaymentingActivity.this, R.layout.simple_spinner_item, dealerNameList);
            spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);

        }
    }


   /* private class GetDealerNamesForSpinner extends AsyncTask {
        private ProgressDialog progressDialog;
        private SQLiteDatabase db;

        @Override
        protected Object doInBackground(Object[] objects) {
            ServerConnection serverConnection = new ServerConnection();

            try {
                ServerConnection server = new ServerConnection();
                connection = server.getConnection();
                statement = connection.createStatement();

                String query = "SELECT distinct Name  FROM Customer";

                Log.v("insert query", query);
                int i = statement.executeUpdate(query);

                if (i == 0) {
                    connection.close();
                    statement.close();
                    return "notinseted";
                } else {
                    connection.close();
                    statement.close();
                    return "inserted";
                }

            } catch (Exception e) {
                e.printStackTrace();
                Log.v("Errorr +++ ", e.getMessage());
                return "notsuccess";


            }
        }

        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(PaymentingActivity.this, R.layout.simple_spinner_item, dealerNameList);
            spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);



        }
    }*/

    public class Insertpayment extends AsyncTask<String, Void, String> {

        String casham, billcash, paidcash, remarks, chequenumb, chequedate, cheqamount, query, due, chequepaid,SalesName;
        private int a;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            loading = ProgressDialog.show(PaymentingActivity.this, "Registration", "Please wait...", false, false);
            casham = cashedittext.getText().toString();
            billcash = billedittext.getText().toString();
            paidcash = paidedittext.getText().toString();
            due = dueedittext.getText().toString();
            chequenumb = chnumberedittext.getText().toString();
            chequedate = chdateedittext.getText().toString();
            cheqamount = chcashedittext.getText().toString();
            chequepaid = cpaidhedittext.getText().toString();

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            try {
                try {

                    switch (item) {
                        case "Cash":
                            query = "insert into payments(SalesPersonName,Partyname,Paymenttype,BillNo,Amount,PaidAmount,DueDate,Remarks)" +
                                    " values('" + SalesName + "','" + customername + "','" + item + "','" + billcash + "','" + casham + "','" + paidcash + "','" + due + "','" + remarks + "')";
                            cmpnymsg = "Dear Customer, \nWe acknowledge the receipt of cash of Rs. " + casham + ".00 on dtd. " + formattedDate + " (towards the invoice no. ___dated___) . \nThank you, \nRegards, \n" + SalesEmailSign;
                            Log.v("in Cash ", "Cash is visible");
                            Log.v("in Cash ", query);
                            break;
                        case "Cheque":
                            query = "insert into payments(SalesPersonName,Partyname,Paymenttype,BillNo,Amount,ChqueNumber,ChequeDate,ChequePaid,BankName,DueDate,Remarks)" +
                                    " values('" + SalesName + "','" + customername + "','" + item + "','" + billcash + "','" + cheqamount + "','" + chequenumb + "','" + chequedate + "','" + chequepaid + "','" + bankName + "','" + due + "','" + remarks + "')";
                            cmpnymsg = "Dear Customer, \nWe acknowledge the receipt of your cheque issued from " + bankName + " with Cheque no. " + chequenumb + " dtd. " + chequedate + " for an amount of Rs. " + cheqamount + ".00 on dtd. " + formattedDate + "  \nThank you, \nRegards, \n" + SalesEmailSign;
                            Log.v("in Cheque ", "Cheque is visible");
                            Log.v("in Cheque ", query);

                            break;
                        default:
                    }

                    ServerConnection server = new ServerConnection();

                    connection = server.getConnection();
                    stmt = connection.createStatement();
                    Log.v("On Line", "SERVER Payment Table");
                    a = stmt.executeUpdate(query);


                    if (a == 0) {

                        stmt.close();
                        connection.close();
                        return "notinserted";
                    }

                    stmt.close();
                    connection.close();
                    sendemail();
                    return "inserted";

                } catch (Exception e) {
                    Log.v("Error in inserted", "Payment ");
                    Log.v("Error in Payment", e.getMessage());
                    return "notsuccess";
                }


            } catch (Exception e) {
                e.printStackTrace();
                return "notsuccess";

            }


        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            loading.dismiss();
            if (result.equals("inserted")) {

                db.close();
                //box.showAlertbox("Payment updated successfully");
                alert.showAlertbox("Payment updated successfully");
                alert.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        finish();
                    }
                });
                //  Toast.makeText(getApplicationContext(), "Payment Updated", Toast.LENGTH_LONG).show();
                //     new sendemail().execute();

            } else {
                box.showAlertbox("Error in Updating Payment! Try again!");
            }
        }

        void sendemail() {
            try {
                List<String> msgmobileno, msgemail;
                //cmpnymsg = "Your order : " + orderNo + "\n" + ProductPagequantity + "\n has been taken from Customer : " + DealerName + "\n By " + SalesName;
                //  cmpnymsg = "Dear Customer, Your order : \n" + orderNo + "\n" + ProductPagequantity + "\n has been taken. We thank you for your valuable order. \nRegards, \nCera-Chem";
                String Finalmail = SalesPaymentemail;
                //if(!string_mail.equals(""))
                if (string_mail != null) {
                    Finalmail = SalesPaymentemail + "," + string_mail;
                    Log.v("Finalmail", Finalmail);
                }

                Log.v("Finalmail", Finalmail);
                msgemail = Arrays.asList(Finalmail.split("\\s*,\\s*"));
                msgmobileno = Arrays.asList(SalesPaymentmobile.split("\\s*,\\s*"));

                for (int i = 0; i < msgmobileno.size(); i++) {
                    sendSMS(msgmobileno.get(i), cmpnymsg);
                }
                //SMS for Client(Customer)
                if (!MobileNO.equals(""))
                    sendSMS(MobileNO, cmpnymsg);
                emailad = "";
                for (int i = 0; i < msgemail.size(); i++) {
                    emailad += msgemail.get(i) + ",";
                }

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                String sub = "Payment Collection from " + SalesName + " for " + dealerName;
                CeraMail localMail1 = new CeraMail(SalesEmial, SalesPass);
                String[] arrayOfString = emailad.split(",");

                localMail1.setTo(arrayOfString);
                localMail1.setFrom(SalesEmial);
                localMail1.setSubject(sub);
                localMail1.setBody(cmpnymsg);
                localMail1.set_replyto(ReplyEmial);

                if (localMail1.send()) {
                    //progressDialog.dismiss();
                    // box.showAlertbox("Order placed Successfully");
                    Log.v("Email  send", "sent");


                } else {
                    Log.v("Email not send", "not sent");
                    //alert.showAlertbox("Error in placing order");
                    Toast.makeText(getApplicationContext(), "Email not sent", Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                //progressDialog.dismiss();
                Log.v("Email not send", e.getMessage());
                e.printStackTrace();

            }

        }

        void sendSMS(String phoneNumber, String message) {
            try {
                String strrr = "http://sms.brainmagic.info/sendunicodesms?uname=cera_chemical&pwd=Cera@chemical&senderid=CCPIND&msg=" + URLEncoder.encode(message, "UTF-8") +"&to=" +URLEncoder.encode(phoneNumber, "UTF-8")+"&route=T";
                // String strrr = "http://bhashsms.com/api/sendmsg.php?user=success&pass=123456&sender=TSUPOR&text=" + URLEncoder.encode(message, "UTF-8") + "&phone=" + URLEncoder.encode(phoneNumber, "UTF-8") + "&priority=ndnd&stype=normal";

                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet(strrr);
                HttpResponse response = client.execute(request);
            } catch (Exception e) {
                //e.printStackTrace();
                Log.v("SMS Error ", e.getMessage());
            }

        }

     /*   public static String capitalizeString(String string) {
            try

            {
                char[] chars = string.toLowerCase().toCharArray();
                boolean found = false;
                for (int i = 0; i < chars.length; i++) {
                    if (!found && Character.isLetter(chars[i])) {
                        chars[i] = Character.toUpperCase(chars[i]);
                        found = true;
                    } else if (Character.isWhitespace(chars[i]) || chars[i] == '.' || chars[i] == '&') { // You can add other chars here
                        found = false;
                    }
                }
                return String.valueOf(chars);
            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }
        }*/
    }
    }

