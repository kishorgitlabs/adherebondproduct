package com.brainmagic.adherebond;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.Toast;

import com.brainmagic.adherebond.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import Logout.logout;
import SQL_Fields.SQLITE;
import about.About_Activity;
import alertbox.Alertbox;
import api.models.Order.SalesOrderAdd;
import api.models.colors.ColorsData;
import api.models.packages.PackageData;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import contact.Contact_Activity;
import database.DBHelper;
import database.DBHelpersync;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import product.Product_Activity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import search.Search_Activity;

public class Sales_OrderNew_Activity extends AppCompatActivity {
    private static final int PERMISSION_REQUEST = 100;
    ImageView home,back,menu;
    NetworkConnection network = new NetworkConnection(Sales_OrderNew_Activity.this);
    private Alertbox box = new Alertbox(Sales_OrderNew_Activity.this);
    private Spinner dealerSpinner,ordertypeSpinner,Partnumberspinner,orderedit;

    private AutoCompleteTextView Partnameedittext,package2,colours;
    private EditText Remarks,basicedittext,discountedittext,totaledittext;
    private EditText Quantityedittext,numbers;
    private EditText Discountedittext,packageedit;
    private Button Order,Additems,Previewbutton;
    private boolean checkNetwork;
    private String is_online;
    private ArrayList<String> dealerNameList,pagagelist,colourlist;
    private SQLiteDatabase db,db2;
    public Connection connection;
    public Statement stmt;
    public ResultSet reset;
    private String username,Adressssss;
    private int S_id;
    public ArrayList<Integer> dealerID;
    private String CustomerName,ProductName,Package,string_mail,Customercode;
    private String Cus_Address,Cus_city,Cus_state,Cus_pincode,Cus_MobileNO,Cus_contactname,Cus_email,Cus_gst,Cus_pan,Cus_custype,Cus_delivery,Modelist;
    private ArrayList<String> partsNameList;
    private ArrayList<String> partsNumberList;
    private String partsRateList,Segment,MobileNO;
    protected String total;
    public ProgressDialog progressDialog;
    protected String unit;
    protected String quantity;
    protected String amount,unitprice;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;


    // Checking Network WIFI & Mobile Network
    NetworkConnection checking = new NetworkConnection(
            Sales_OrderNew_Activity.this);
    protected String discount,remark,Ordermode,unpckage;
    private String formattedDate;
    public ArrayList<String> msgName;
    //public ArrayList<String> msgmobileno;
    //public ArrayList<String> msgemail;
    public List<String> msgmobileno;
    public List<String> msgemail;

    public String cmpnymsg,colours1,customerName_S,productName_S,packageName_S,packageEdit_S,qty_S,numer_S,color_S,basiPrice_S,discount_S,total_S,orderMode_S,orderType_S,remarks_S;
    public String emailad="";
    private ArrayList<String> orderTypeList,ordermodelist;
    protected String OrderType;
    public String orderNo;
    protected String PartName,salesID,SalesName,SalesEmial,SalesPass,SalesBranch,SalesEmailSign,SalesOrdermail,SalesOrdermobile,SalesPaymentemail,SalesPaymentmobile;
    private List<String> package1,colors1;
    public ArrayList<String> PackageList;
    protected String dcode,numbers1;
    private String ordermode1;
   String basic,discountprice,mobile_S;
   int a,b,c;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales__order_new_);

        dealerSpinner = (Spinner) findViewById(R.id.dealerspinner);
        ordertypeSpinner = (Spinner) findViewById(R.id.ordertypespinner);
        package2  = (AutoCompleteTextView) findViewById(R.id.packagespinner);
        packageedit=(EditText)findViewById(R.id.packageedit);
        Partnameedittext = (AutoCompleteTextView) findViewById(R.id.productnameautoedittext);
        Quantityedittext = (EditText) findViewById(R.id.quantityedittext);
        Discountedittext = (EditText) findViewById(R.id.discountedittext);
        Remarks = (EditText) findViewById(R.id.remark);
        orderedit=(Spinner) findViewById(R.id.orderedit);
        numbers=(EditText)findViewById(R.id.numbersedittext);
        colours=(AutoCompleteTextView) findViewById(R.id.coloursedit);
        Order = (Button) findViewById(R.id.order);
        Additems = (Button) findViewById(R.id.add);
        Previewbutton = (Button) findViewById(R.id.preview);

        myshare = getSharedPreferences("Registration", Sales_OrderNew_Activity.this.MODE_PRIVATE);
        editor = myshare.edit();
        mobile_S=myshare.getString("mobile","");

        editor.commit();
        ordertypeSpinner=findViewById(R.id.ordertypespinner);
        salesID = myshare.getString("salesID", "");
        SalesName = myshare.getString("name", "");
        SalesEmial = myshare.getString("email", "");
        SalesPass = myshare.getString("emailpass", "");
        SalesEmailSign = myshare.getString("signature", "");
        SalesBranch = myshare.getString("salesbranch", "");
        SalesOrdermail = myshare.getString("salesemail", "");
        SalesOrdermobile = myshare.getString("salessms", "");
        basicedittext=findViewById(R.id.basicedittext);
        discountedittext=findViewById(R.id.discountedittext);
        totaledittext=findViewById(R.id.totaledittext);

        home = (ImageView) findViewById(R.id.home);
        back = (ImageView) findViewById(R.id.back);
        menu=(ImageView)findViewById(R.id.menubar);

        basicedittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
basic=s.toString();
if (basic.length()>0){
    a=Integer.parseInt(basic);
    c=a*b;
    totaledittext.setText(String.valueOf(c));
    totaledittext.setEnabled(false);
}
            }
        });



ordertypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        orderType_S= parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
});


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(Sales_OrderNew_Activity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });


        pagagelist =  new ArrayList<String>();
        dealerNameList = new ArrayList<String>();
        orderTypeList = new ArrayList<String>();
        dealerID = new ArrayList<Integer>();
        partsNameList = new ArrayList<String>();
        partsNumberList = new ArrayList<String>();


        msgmobileno = new ArrayList<String>();
        msgName = new ArrayList<String>();
        msgemail = new ArrayList<String>();


        orderTypeList.add("Select Order Type");
        orderTypeList.add("Credit Bill");
        orderTypeList.add("Cash Bill");
        orderTypeList.add("Sample");

        ordermodelist=new ArrayList<>();
        ordermodelist.add("Select Order Mode");
        ordermodelist.add("Phone");
        ordermodelist.add("Direct");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Sales_OrderNew_Activity.this,R.layout.simple_spinner_item,ordermodelist);
        orderedit.setAdapter(adapter);
        package2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Package= parent.getItemAtPosition(position).toString();
                packageEdit_S=parent.getItemAtPosition(position).toString();
            }
        });
        Quantityedittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                discountprice=s.toString();
                if (discountprice.length()>0){
                    b=Integer.parseInt(discountprice);

                }
            }
        });
        colours.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                color_S=parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        colours.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                colours1= parent.getItemAtPosition(position).toString();
                color_S=parent.getItemAtPosition(position).toString();
            }
        });

       package2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               packageEdit_S=parent.getItemAtPosition(position).toString();
           }

           @Override
           public void onNothingSelected(AdapterView<?> parent) {

           }
       });

        Partnameedittext.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                productName_S=parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Date d = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(d);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedDate = df.format(c.getTime());

        DBHelpersync dbHelper = new DBHelpersync(Sales_OrderNew_Activity.this);
        db = dbHelper.getWritableDatabase();


        Partnameedittext.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int position,
                                    long arg3) {
                // TODO Auto-generated method stub

                PartName = parent.getItemAtPosition(position).toString();
                productName_S = parent.getItemAtPosition(position).toString();
                if (PartName.equals("")) {

                } else {
                    Log.v("called ", "Part NO");
                   // new GetPartPackage().execute();
                }
            }
        });

        Order.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

//                startActivity(new Intent(Sales_OrderNew_Activity.this, com.brainmagic.adherebond.Preview_New_Activity.class));

                if(isTableExists())
                {
                    startActivity(new Intent(Sales_OrderNew_Activity.this, com.brainmagic.adherebond.Preview_New_Activity.class));
                }
                else
                {
                    box.showAlertbox("No Orders Found");
                }


            }
        });
        orderedit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ordermode1=parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        menu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                final PopupMenu popupMenu = new PopupMenu(Sales_OrderNew_Activity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(Sales_OrderNew_Activity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(Sales_OrderNew_Activity.this, Product_Activity.class);
                                startActivity(product);
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(Sales_OrderNew_Activity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(Sales_OrderNew_Activity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.watsnew:
                                Intent contact1 = new Intent(Sales_OrderNew_Activity.this, ProjectActivity.class);
                                startActivity(contact1);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(Sales_OrderNew_Activity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {

                                    Intent b = new Intent(Sales_OrderNew_Activity.this, com.brainmagic.adherebond.Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));

                                    Intent b = new Intent(Sales_OrderNew_Activity.this, com.brainmagic.adherebond.Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(Sales_OrderNew_Activity.this).log_out();
                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_login_inner);
                //Whatsnew_Activity.super.onPrepareOptionsMenu(popupMenu);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });


        dealerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                customerName_S=parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Additems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                CustomerName = dealerSpinner.getSelectedItem().toString();
                ProductName = Partnameedittext.getText().toString();
                OrderType = ordertypeSpinner.getSelectedItem().toString();
                if (CustomerName.equals("Select Customer Name")) {
                    Toast.makeText(getApplicationContext(), "Select Customer Name", Toast.LENGTH_LONG).show();
                } else if (ProductName.equals("")) {
                    Toast.makeText(getApplicationContext(), "Select Product", Toast.LENGTH_LONG).show();
                } else if(numbers.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Select Number", Toast.LENGTH_LONG).show();
                }else if (OrderType.equals("Select Order Type")) {
                    Toast.makeText(getApplicationContext(), "Select Order Type", Toast.LENGTH_LONG).show();
                } else {



                    new GetCustomerInfo().execute();
                    new Additems().execute();
//                    addItems();
                }
            }
        });

        new GetDealerNamesForSpinner().execute();
        new GetPartsForAutotext().execute();
        NetworkConnection isnet = new NetworkConnection(Sales_OrderNew_Activity.this);
        if (isnet.CheckInternet()) {

            Getpackages();
            GetColours();
        } else {
            box.showAlertbox(getResources().getString(R.string.no_internet));
        }

    }


    private  void addItems(){


        quantity = Quantityedittext.getText().toString();
        discount = Discountedittext.getText().toString();
        unpckage=packageedit.getText().toString();
        remark = Remarks.getText().toString();
        numbers1=numbers.getText().toString();
        basiPrice_S=basicedittext.getText().toString();
        total_S=totaledittext.getText().toString();
        try {

            final ProgressDialog loading = ProgressDialog.show(Sales_OrderNew_Activity.this, getResources().getString(R.string.app_launcher_name), "Loading...", false, false);

            APIService service = RetroClient.getApiService();
            Call<SalesOrderAdd> call = service.orderadd(mobile_S,"New",customerName_S,productName_S,packageEdit_S,unpckage,quantity,numbers1,color_S,basiPrice_S,discount,total_S,ordermode1,orderType_S,remark);
            call.enqueue(new Callback<SalesOrderAdd>() {
                @Override

                public void onResponse(Call<SalesOrderAdd> call, Response<SalesOrderAdd> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            box.showAlertbox("Prodct Item Added Succesfully");
                            Partnameedittext.setText("");
                            Quantityedittext.setText("");
                            Discountedittext.setText("");
                            Remarks.setText("");
                        }

                        else
                        {
                            box.showAlertbox(getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }

                @Override
                public void onFailure(Call<SalesOrderAdd> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private void GetColours() {
        try {

            APIService service = RetroClient.getApiService();
            Call<ColorsData> call;
            call = service.colors();
            call.enqueue(new Callback<ColorsData>() {
                @Override
                public void onResponse(Call<ColorsData> call, Response<ColorsData> response) {
                    colors1 = response.body().getData();
                    colours.setAdapter(new ArrayAdapter<String>(Sales_OrderNew_Activity.this, android.R.layout.simple_dropdown_item_1line, colors1));
                    colours.setThreshold(1);
                }

                @Override
                public void onFailure(Call<ColorsData> call, Throwable t) {

                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void Getpackages() {
        try {

            APIService service = RetroClient.getApiService();
            Call<PackageData> call;
            call = service.package1();
            call.enqueue(new Callback<PackageData>() {
                @Override
                public void onResponse(Call<PackageData> call, Response<PackageData> response) {
                    package1 = response.body().getData();
                    package2.setAdapter(new ArrayAdapter<String>(Sales_OrderNew_Activity.this, android.R.layout.simple_dropdown_item_1line, package1));
                    package2.setThreshold(1);
                }

                @Override
                public void onFailure(Call<PackageData> call, Throwable t) {

                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private class Additems extends AsyncTask<String, Void, String> {

        ContentValues values = new ContentValues();
        SQLiteDatabase db;
        private double success;
        private int a;
        private String orderNo,basics,discounts,totals;

        @SuppressLint("InflateParams")
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // instantiate new progress dialog
            progressDialog = new ProgressDialog(Sales_OrderNew_Activity.this);
            // spinner (wheel) style dialog
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Adding data...");
            progressDialog.show();

            basics=basicedittext.getText().toString();
            discounts=discountedittext.getText().toString();
            totals=totaledittext.getText().toString();

            quantity = Quantityedittext.getText().toString();

            unpckage=packageedit.getText().toString();
            remark = Remarks.getText().toString();
            numbers1=numbers.getText().toString();

        }


        @Override
        protected String doInBackground(String... args0) {
            try {
                values.put(SQLITE.COLUMN_executivename, SalesName);
                values.put(SQLITE.COLUMN_dealername, CustomerName);
                values.put(SQLITE.COLUMN_prodname, ProductName);
                values.put(SQLITE.COLUMN_pack, Package);
                values.put(SQLITE.COLUMN_quant, quantity);
                values.put(SQLITE.COLUMN_discounts, discount);
                values.put(SQLITE.COLUMN_orderType, OrderType);
                values.put(SQLITE.COLUMN_date, formattedDate);
                values.put(SQLITE.COLUMN_address, Cus_Address);
                values.put(SQLITE.COLUMN_city, Cus_city);
                values.put(SQLITE.COLUMN_state, Cus_state);
                values.put(SQLITE.COLUMN_pincode, Cus_pincode);
                values.put(SQLITE.COLUMN_contact, Cus_MobileNO);
                values.put(SQLITE.COLUMN_contactname, Cus_contactname);
                values.put(SQLITE.COLUMN_email, Cus_email);
                values.put(SQLITE.COLUMN_gst, Cus_gst);
                values.put(SQLITE.COLUMN_pan, Cus_pan);
                values.put(SQLITE.COLUMN_custype, Cus_custype);
                values.put(SQLITE.COLUMN_ordermode, ordermode1);
                values.put(SQLITE.COLUMN_remark, remark);
                values.put(SQLITE.COLUMN_deliveryaddress,Cus_delivery);
                values.put(SQLITE.COLUMN_ordernumber,numbers1);
                values.put(SQLITE.COLUMN_ordercolours,colours1);
                values.put(SQLITE.COLUMN_quantity,quantity);

                values.put(SQLITE.COLUMN_basic,basics);
                values.put(SQLITE.COLUMN_discount,discounts);
                values.put(SQLITE.COLUMN_total,totals);
                values.put(SQLITE.Coloumn_umpackage,unpckage);


                DBHelpersync dbHelper = new DBHelpersync(Sales_OrderNew_Activity.this);
                db = dbHelper.getWritableDatabase();
                dbHelper.Createsalesordernewtable(db);
                success = db.insert(SQLITE.TABLE_NewPartsOrder, null, values);
                return "received";
            } catch (Exception e) {
                Log.v("Error in Add local db ", "catch is working");
                Log.v("Error in Add local db", e.getMessage());
                return "notsuccess";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("received")) {
                Log.v("success", Double.toString(success));
                Partnameedittext.setText("");
                Quantityedittext.setText("");
                Discountedittext.setText("");
                Remarks.setText("");
               /* ArrayAdapter<String> adapter = new ArrayAdapter<String>(Sales_OrderNew_Activity.this,R.layout.simple_spinner_item,orderTypeList);
                ordertypeSpinner.setAdapter(adapter);
                Packagespinner.setAdapter(null);*/
                Toast.makeText(getApplicationContext(), "Item added to Cart", Toast.LENGTH_LONG).show();
            } else {
                box.showAlertbox("Error in adding ");
            }
        }
    }

    private class GetDealerNamesForSpinner extends AsyncTask<String, Void, String>
    {

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            try
            {
                dealerNameList.add("Select Customer Name");
                getDealerNamesForSpinner();
                Log.v("OFF Line", "SQLITE Table");
                return "received";

            }catch (Exception e)
            {
                Log.v("Error in get date","catch is working");
                Log.v("Error in Incentive", e.getMessage());
                return "notsuccess";
            }

        }
        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(Sales_OrderNew_Activity.this,R.layout.simple_spinner_item,dealerNameList);
            spinnerAdapter.setNotifyOnChange(true);
            spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
            dealerSpinner.setAdapter(spinnerAdapter);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(Sales_OrderNew_Activity.this,R.layout.simple_spinner_item,orderTypeList);
            ordertypeSpinner.setAdapter(adapter);
        }

        private void getDealerNamesForSpinner()
        {
            // TODO Auto-generated method stub
            String query ="select distinct "+SQLITE.COLUMN_Create_cusname+ " from "+SQLITE.TABLE_Create_Profile+" order by "+SQLITE.COLUMN_Create_cusname+" asc";
            Log.v("Name from createcus",query);
            Cursor cursor = db.rawQuery(query, null);
            if(cursor.moveToFirst())
            {
                do
                {
                    dealerNameList.add(cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_cusname)));
                } while (cursor.moveToNext());
                cursor.close();
            }

        }


    }


    private class GetPartsForAutotext extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            try
            {
                DBHelper dbHelper = new DBHelper(Sales_OrderNew_Activity.this);
                db2 = dbHelper.readDataBase();

                String query ="select distinct Prod_Name from product_details";
                Log.v("productname for autotex",query);
                Cursor cursor = db2.rawQuery(query, null);
                if(cursor.moveToFirst())
                {
                    do
                    {
                        partsNameList.add(cursor.getString(cursor.getColumnIndex("Prod_Name")));

                    } while (cursor.moveToNext());
                    db2.close();
                    cursor.close();
                }
                Log.v("OFF Line", "SQLITE Table");
                return "received";

            }catch (Exception e)
            {
                Log.v("Error in get date","catch is working");
                Log.v("Error in Incentive", e.getMessage());
                return "notsuccess";
            }

        }
        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            Partnameedittext.setAdapter(new ArrayAdapter<String>(Sales_OrderNew_Activity.this, android.R.layout.simple_dropdown_item_1line, partsNameList));
            Partnameedittext.setThreshold(1);

        }

    }


    private  class GetCustomerInfo extends AsyncTask<String, Void, String>
    {

        @Override
        protected String doInBackground(String... params) {
            try
            {
                String query ="select * from "+SQLITE.TABLE_Create_Profile+" where cusname ='"+ CustomerName +"'";
                //  String query ="select distinct cusname from CreateCProfile order by cusname asc";
                Log.v("DealerNo",query);
                Cursor cursor = db.rawQuery(query, null);
                if(cursor.moveToNext())
                {
                    do
                    {
                        Cus_Address = cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_address));
                        Cus_city = cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_city));
                        Cus_state = cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_state));
                        Cus_pincode = cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_pincode));
                        Cus_MobileNO = cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_contact));
                        Cus_contactname = cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_contactname));
                        Cus_email = cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_email));
                        Cus_gst = cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_gst));
                        Cus_pan = cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_pan));
                        Cus_custype = cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_custype));
                        Cus_delivery = cursor.getString(cursor.getColumnIndex(SQLITE.COLUMN_Create_deliveryaddress));


                    } while (cursor.moveToNext());
                    Log.v("OFF Line", "SQLITE Table");
                    Log.v("Project", Cus_Address+Cus_email);
                    return "received";
                }
                else
                {
                    cursor.close();
                    return "error";
                }




            }catch (Exception e)
            {
                Log.v("Error in get cusinfo","catch is working");
                Log.v("Error in cusinfo", e.getMessage());
                return "notsuccess";
            }
        }
        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

        }

    }






   /* public class SaveBackground extends AsyncTask<String, Void, String> {
        double success;
        @SuppressLint("InflateParams")
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // instantiate new progress dialog
            progressDialog = new ProgressDialog(Sales_OrderNew_Activity.this);
            // spinner (wheel) style dialog
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Saving data...");
            progressDialog.show();

        }
        @Override
        protected String doInBackground(String... args0)
        {
            int i = 0;
            try {
                Log.v("ONLINE Line", "Server Table");
                ServerConnection ser = new ServerConnection();
                connection = ser.getConnection();
                stmt = connection.createStatement();
                String insertquery ="insert into mobile_orders(executivename,dealername,orderid,productname,orderType,package,unitprice,quantity,discount,amount,date) " +
                        "values('"+SalesName+"','"+CustomerName+"','"+orderNo+"','"+ProductName+"','"+OrderType+"','"+Package+"','','"+quantity+"','"+discount+"','','"+formattedDate+"')";
                Log.v("Query", insertquery);
                i = stmt.executeUpdate(insertquery);
                if (i == 0)
                {
                    connection.close();
                    stmt.close();
                    return "notreceived";
                }
                else{
                    connection.close();
                    stmt.close();
                    return "received";
                }


            } catch (Exception e)
            {
                Log.v("Error Save Online db ","catch is working");
                Log.v("Error in Save Online db", e.getMessage());
                return "notsuccess";
            }


        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if(result.equals("received"))
            {
                partsRateList="";
                unitprice="";
                Log.v("success", Double.toString(success));
                Quantityedittext .setText("");
                Discountedittext.setText("");
                Toast.makeText(getApplicationContext(), "Order placed", Toast.LENGTH_LONG).show();
                new sendemail().execute();
            }
            else
            {
                box.showAlertbox("Error in placing Order! Try again!");
            }
        }



    }*/

    public boolean isTableExists() {
        SQLiteDatabase mDatabase;
        DBHelpersync dbHelp = new DBHelpersync(Sales_OrderNew_Activity.this);

        mDatabase = dbHelp.getWritableDatabase();
        //String query = "select DISTINCT tbl_name from sqlite_master where tbl_name = 'NewOrderpreview'";
        String query = "select DISTINCT tbl_name from sqlite_master where tbl_name = '"+ SQLITE.TABLE_NewPartsOrder+"'";
        Cursor cursor = mDatabase.rawQuery(query, null);
        Log.v("preview",query);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                mDatabase.close();
                return true;
            }
            cursor.close();
            mDatabase.close();
        }
        return false;
    }
    public static String capitalizeString(String string )
    {
        try
        {
            char[] chars = string.toLowerCase().toCharArray();
            boolean found = false;
            for (int i = 0; i < chars.length; i++) {
                if (!found && Character.isLetter(chars[i])) {
                    chars[i] = Character.toUpperCase(chars[i]);
                    found = true;
                } else if (Character.isWhitespace(chars[i]) || chars[i] == '.' || chars[i] == '&') { // You can add other chars here
                    found = false;
                }
            }
            return String.valueOf(chars);
        }catch (Exception e)
        {
            e.printStackTrace();
            return "error";
        }
    }


}

