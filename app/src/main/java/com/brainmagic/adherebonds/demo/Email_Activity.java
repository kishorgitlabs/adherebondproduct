package com.brainmagic.adherebond;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.provider.ContactsContract;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.brainmagic.adherebond.R;

import java.util.ArrayList;

import HideKeyboard.HideSoftKeyboard;
import alertbox.Alertbox;
import email.Mail;

public class Email_Activity extends AppCompatActivity {

    private static final int REQUEST_CHOOSER = 1234;
    Alertbox alert=new Alertbox(Email_Activity.this);
    ImageView back,Attachment;
    EditText to_email,cc_email,Subject,body_text;
    private static final int FILE_SELECT_CODE = 0;
    String sub,to,cc="",bod;
    Button send,clear;
    ImageView Contact,Contact2;
    String salesID,SalesName,SalesEmial,SalesPass,SalesBranch,SalesEmailSign,ReplyEmial;
    private ProgressDialog progress;
    private static final int CONTACT_PICKER_RESULT = 1;
    private static final int CC_PICKER_RESULT = 2;
    private static final String DEBUG_TAG = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_);
        new HideSoftKeyboard().setupUI(findViewById(R.id.activity_email),this);

        new HideSoftKeyboard().setupUI(findViewById(R.id.activity_email),this);

        to_email=(EditText)findViewById(R.id.to_email);
        cc_email=(EditText)findViewById(R.id.cc_email);
        Subject=(EditText)findViewById(R.id.subject);
        body_text=(EditText)findViewById(R.id.body);
        send=(Button)findViewById(R.id.send);
        back=(ImageView)findViewById(R.id.back);
        Contact = (ImageView)findViewById(R.id.contact);
        Contact2 = (ImageView)findViewById(R.id.contact2);
        Subject.setText(getIntent().getStringExtra("subject"));
        body_text.setText(getIntent().getStringExtra("body"));
        SharedPreferences myshare;
        myshare=getSharedPreferences("Registration",MODE_PRIVATE);
        salesID = myshare.getString("salesID", "");
        SalesName = myshare.getString("name", "");
        /*SalesEmial = myshare.getString("email", "");
        SalesPass = myshare.getString("emailpass", "");*/
        ReplyEmial = myshare.getString("email", "");
        SalesEmailSign = myshare.getString("signature", "");
        SalesBranch = myshare.getString("salesbranch", "");

        SalesEmial = "tradeadherebonds@gmail.com";
        SalesPass = "gopi1234";


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        Contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent contactIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                contactIntent.setType(ContactsContract.CommonDataKinds.Email.CONTENT_TYPE);
                startActivityForResult(contactIntent, CONTACT_PICKER_RESULT);
                //ContentResolver cr = getContentResolver();


            }
        });


        Contact2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent contactIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                contactIntent.setType(ContactsContract.CommonDataKinds.Email.CONTENT_TYPE);
                startActivityForResult(contactIntent, CC_PICKER_RESULT);
                //ContentResolver cr = getContentResolver();


            }
        });


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sub=Subject.getText().toString();
                to=to_email.getText().toString();
                cc=cc_email.getText().toString();
                bod=body_text.getText().toString();
                if (to.equals(""))
                {
                    to_email.setFocusable(true);
                    to_email.setError("Enter email");
                } else
                if (bod.equals(""))
                {
                    body_text.setFocusable(true);
                    body_text.setError("Enter body");
                } else if (sub.equals(""))
                {
                    Subject.setFocusable(true);
                    Subject.setError("Enter subject");
                } else if (!isValidEmail(to.trim()))
                {
                    to_email.setFocusable(true);
                    to_email.setError("Enter valid Email ID");
                    Toast toast = Toast.makeText(Email_Activity.this,"Email is not valid", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
                else {
                    if (!cc.equals("")) {
                        if (!isValidEmail(cc_email.getText())) {
                            cc_email.setFocusable(true);
                            cc_email.setError("Enter valid Email ID");
                            Toast toast = Toast.makeText(Email_Activity.this,"Email is not valid", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                        else
                        {
                            new send_mail().execute();
                        }
                    }
                    else
                    {
                        new send_mail().execute();
                    }

                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String phoneNo = null ;
        String ccNo = null ;
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CONTACT_PICKER_RESULT:
                    Cursor cursor = null;
                    String name = "";
                    try {

                        //String name = null;
                        Uri uri = data.getData();
                        cursor = getContentResolver().query(uri, null, null, null, null);
                        cursor.moveToFirst();
                        int  phoneIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA);
                        phoneNo = cursor.getString(phoneIndex);

                        //textView2.setText(phoneNo);
                    } catch (Exception e) {
                        Log.e(DEBUG_TAG, "Failed to get name", e);
                    } finally {
                        if (cursor != null) {
                            cursor.close();
                        }

                        if(to_email.getText().toString().equals(""))
                        {
                            to_email.setText(phoneNo);
                            to_email.setSelection(to_email.getText().length());
                        }else {
                            String toemail = to_email.getText().toString();
                            toemail  +=  "," + phoneNo;
                            to_email.setText(toemail);
                            to_email.setSelection(to_email.getText().length());
                        }

                        if (phoneNo.length() == 0) {
                            Toast.makeText(this, "Name not found for contact.",
                                    Toast.LENGTH_LONG).show();
                        }

                    }

                    break;

                case CC_PICKER_RESULT:
                    Cursor cursor2 = null;
                    String name2 = "";
                    try {

                        Uri uri = data.getData();
                        cursor = getContentResolver().query(uri, null, null, null, null);
                        cursor.moveToFirst();
                        int  phoneIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA);
                        ccNo = cursor.getString(phoneIndex);

                        //textView2.setText(phoneNo);
                    } catch (Exception e) {
                        Log.e(DEBUG_TAG, "Failed to get name", e);
                    } finally {
                        if (cursor2 != null) {
                            cursor2.close();
                        }

                        if(cc_email.getText().toString().equals(""))
                        {
                            cc_email.setText(ccNo);
                            cc_email.setSelection(to_email.getText().length());
                        }else {
                            String ccemail = cc_email.getText().toString();
                            ccemail  +=  "," + ccNo;
                            cc_email.setText(ccemail);
                            cc_email.setSelection(cc_email.getText().length());
                        }

                        if (ccNo.length() == 0) {
                            Toast.makeText(this, "Name not found for contact.",
                                    Toast.LENGTH_LONG).show();
                        }

                    }

                    break;

            }

        } else {
            Log.w(DEBUG_TAG, "Warning: activity result not ok");
        }
    }
    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }
    public static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target))
        {
            return false;
        }
        else
        {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }


    private class send_mail extends AsyncTask<String,Void,String>
    {

        // need to chane after
        //String SalesEmial="cerachemicalsapp@gmail.com",SalesPass="cerachemicals";



        protected void onPreExecute() {
            super.onPreExecute();
            progress = new ProgressDialog(Email_Activity.this);
            progress.setMessage("Loading...");
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setCancelable(false);
            progress.show();
            //bod = bod + "\nRegards\n" + SalesEmailSign;
            //bod = bod + "\nRegards\n" + SalesEmailSign;

        }
        @Override
        protected String doInBackground(String[] objects)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Mail localMail1 = new Mail(SalesEmial, SalesPass);
            if(cc.equals(""))
            {
                String[] arrayOfString = to.split(",");
                Log.v("mail id", to);
                localMail1.setTo(arrayOfString);
                localMail1.setFrom(SalesEmial);
                localMail1.setSubject(sub);
                localMail1.setBody(bod+"\n"+SalesEmailSign);
                localMail1.set_replyto(ReplyEmial);
            }
            else{
                String[] arrayOfString1 = cc.split(",");
                String[] arrayOfString = to.split(",");
                Log.v("mail id", cc);
                Log.v("mail id", to);
                localMail1.setCC(arrayOfString1);
                localMail1.setTo(arrayOfString);
                localMail1.setFrom(SalesEmial);
                localMail1.setSubject(sub);
                localMail1.setBody(bod+"\n"+SalesEmailSign);
                localMail1.set_replyto(ReplyEmial);
            }

            try {
                if (localMail1.send())
                {
                    return "sent";
                }
                else {
                    return "mail not sent";
                }
            } catch (Exception e) {

                e.printStackTrace();
                return "error";
            }

            /*EmailSender sender = news EmailSender(emailId,spass);
            try {
                sender.sendMail(sub,bod,emailId,to);
            } catch (Exception e) {
                Log.e("SendMail", e.getMessage(),e);
            }*/

        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if(s.equals("sent"))
            {
                alert.showAlertbox("Email sent Sucessfully");
                to_email.setText("");
                cc_email.setText("");
                body_text.setText("");
                Subject.setText("");

            }
            else if(s.equals("mail not sent"))
            {
                alert.showAlertbox("Email not sent");
            }
            else
            {
                alert.showAlertbox("Some problem ocuured Email not sent ");
            }

        }


    }


    //get the emails from contact
    public ArrayList<String> getNameEmailDetails(){
        ArrayList<String> names = new ArrayList<String>();
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,null, null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                Cursor cur1 = cr.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                        new String[]{id}, null);
                while (cur1.moveToNext()) {
                    //to get the contact names
                    String name=cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    Log.e("Name :", name);
                    String email = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    Log.e("Email", email);
                    if(email!=null){
                        names.add(name);
                    }
                }
                cur1.close();
            }
        }
        return names;
    }

}
