package com.brainmagic.adherebond;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;

import com.brainmagic.adherebond.R;

import java.util.ArrayList;

import about.About_Activity;
import adapter.ImageAdapter;
import alertbox.Alertbox;
import contact.Contact_Activity;
import database.DBHelper;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import product.ProductDetails_Activity;
import product.Product_Activity;
import search.Search_Activity;

public class SearchList_Activity extends AppCompatActivity {
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    ProgressDialog progressDialog;
    private ListView listview;
    GridView grid_view;
    private ArrayList<String> IDList,imageList,productList;
    String search,searchtype;
    ImageView home,back,menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_list_);
        myshare = getSharedPreferences("Registration",SearchList_Activity.MODE_PRIVATE);
        editor = myshare.edit();

        //catagoryList = news ArrayList<String>();
        imageList = new ArrayList<>();
        IDList = new ArrayList<>();
        productList=new ArrayList<>();
        //listview = (ListView) findViewById(R.id.items);
        grid_view = (GridView) findViewById(R.id.gridView);

        //title=(TextView)findViewById(R.id.title);
        //s=getIntent().getStringExtra("Category");
        //title.setText(capitalizeString(s));
        back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home=(ImageView)findViewById(R.id.home);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a=new Intent(SearchList_Activity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });
        menu=(ImageView)findViewById(R.id.menubar);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(SearchList_Activity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(SearchList_Activity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(SearchList_Activity.this, Product_Activity.class);
                                product.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                product.putExtra("a","");
                                startActivity(product);
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(SearchList_Activity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(SearchList_Activity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.watsnew:
                                Intent contact1 = new Intent(SearchList_Activity.this, ProjectActivity.class);
                                startActivity(contact1);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(SearchList_Activity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {
                                    Intent b = new Intent(SearchList_Activity.this, Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));

                                    Intent b = new Intent(SearchList_Activity.this, Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }return true;

                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popupmenu_others);
                popupMenu.show();

            }
        });

        search=getIntent().getStringExtra("Searchcontent");
        searchtype=getIntent().getStringExtra("Searchtype");

        /*listview.setOnItemClickListener(news AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent a=news Intent(SearchList_Activity.this,ProductDetails_Activity.class);
                a.putExtra("ID",IDList.get(i));
                //a.putExtra("Productname",.get(i));
                startActivity(a);
            }
        });*/
        grid_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent a=new Intent(SearchList_Activity.this, ProductDetails_Activity.class);
                a.putExtra("ID",IDList.get(i));
                startActivity(a);
            }
        });

        new SearchList_Activity.GetCatagory().execute();
    }
    class  GetCatagory extends AsyncTask<String,Void,String>
    {

        private String productquery;
        private SQLiteDatabase db;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SearchList_Activity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... strings) {

            try

            {
                DBHelper dbhelper = new DBHelper(SearchList_Activity.this);
                db = dbhelper.readDataBase();
                //productquery = "SELECT distinct Prod_Name,Prodexc_img,Prod_ID FROM Product_details WHERE Prod_Name LIKE '%"+search+"%' OR Prod_ID LIKE '%"+search+"%' or Prod_Priority LIKE '%"+search+"%' or Prodshort_desc LIKE '%"+search+"%' or Prodlong_desc LIKE '%"+search+"%' or Highlights LIKE '%"+search+"%' or Prodexc_img LIKE '%"+search+"%' or Prodvid_url LIKE '%"+search+"%' or Prod_brocher LIKE '%"+search+"%' or Prod_key LIKE '%"+search+"%' or MProdsub_name LIKE '%"+search+"%' or MProdsub_ID LIKE '%"+search+"%' or MProd_ID LIKE '%"+search+"%' or Mprod_name LIKE '%"+search+"%'";
                /*productquery = "SELECT  Prod_Name,Prodexc_img,Prod_ID FROM Product_details WHERE Prod_Name LIKE '%"+search+"%'  or Prodshort_desc LIKE '%"+search+"%' or Prodlong_desc LIKE '%"+search+"%' or Highlights LIKE '%"+search+"%'";*/
                /*productquery = "SELECT  distinct Prod_Name,Prodexc_img,Prod_ID FROM Product_details WHERE Prod_Name LIKE '%"+search+"%' OR Prodshort_desc LIKE '%"+search+"%'";*/
                //productquery = "SELECT  distinct Prod_Name,Prodexc_img,Prod_ID FROM Product_details WHERE Prod_Name LIKE ?";
                //productquery = "SELECT  distinct Prod_Name,Prodexc_img,Prod_ID FROM Product_details WHERE Prod_Name LIKE UPPER('%"+search+"%')";
                if(searchtype.equals("Others")) {
                    productquery = "SELECT  distinct Prod_Name,Prodexc_img,Prod_ID FROM Product_details WHERE Prod_Name LIKE '%" + search + "%' OR Prodlong_desc LIKE '%" + search + "%' OR Highlights LIKE '%" + search + "%' OR MProdsub_name LIKE '%" + search + "%' OR Mprod_name LIKE '%" + search + "%'";
                    //MProdsub_name, Mprod_name
                }
                else
                    productquery = "SELECT  distinct Prod_Name,Prodexc_img,Prod_ID FROM Product_details WHERE Prod_Name LIKE '%" + search + "%'";

                Log.v("search",productquery);
                Cursor cursor = db.rawQuery(productquery, null);
                // Defines a variable for the search string
                //String mSearchString=search;
                // Defines the array to hold values that replace the ?
                //String[] mSelectionArgs = { mSearchString };
                //mSelectionArgs[0] = "%" + mSearchString + "%";
                //Cursor cursor=db.rawQuery(productquery, mSelectionArgs);

                if (cursor.moveToFirst())
                {
                    do
                    {
                        productList.add(capitalizeString(cursor.getString(cursor.getColumnIndex("Prod_Name"))));
                        imageList.add(cursor.getString(cursor.getColumnIndex("Prodexc_img")));
                        IDList.add(cursor.getString(cursor.getColumnIndex("Prod_ID")));

                    }
                    while (cursor.moveToNext());
                    db.close();
                    cursor.close();
                    return "success";
                }
                else
                {
                    return "empty";
                }


              /*  ServerConnection serverConnection = news ServerConnection();
                connection = serverConnection.getConnection();
                statement = connection.createStatement();
                resultSet = statement.executeQuery("select * from Mprodcategory");

                while (resultSet.next())
                {
                    catagoryList.add(resultSet.getString("Mprod_name"));
                    IDList.add(resultSet.getString("MProd_ID"));
                    imageList.add("http://ceracms.brainmagicllc.com/Partsupload/"+resultSet.getString("MProd_Img"));
                }



               if(catagoryList.isEmpty())
               {
                   return "empty";
               }
                else
               {
                   return "success";
               }*/

            }
            catch (Exception e)
            {
                e.printStackTrace();
                return "error";
            }

        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            if(s.equals("success"))
            {
                grid_view.setAdapter(new ImageAdapter(SearchList_Activity.this,productList,imageList));
                /*listview.setAdapter(news CategoryAdapter(SearchList_Activity.this,productList,imageList));*/
            }
            else if(s.equals("empty"))
            {

                Alertbox alert = new Alertbox(SearchList_Activity.this);
                alert.showAlertbox("No Products Found");
                //finish();

            }
            else
            {

            }


        }
    }
    public static String capitalizeString(String string )
    {
        try

        {
            char[] chars = string.toLowerCase().toCharArray();
            boolean found = false;
            for (int i = 0; i < chars.length; i++) {
                if (!found && Character.isLetter(chars[i])) {
                    chars[i] = Character.toUpperCase(chars[i]);
                    found = true;
                } else if (Character.isWhitespace(chars[i]) || chars[i] == '.' || chars[i] == '&') { // You can add other chars here
                    found = false;
                }
            }
            return String.valueOf(chars);
        }catch (Exception e)
        {
            e.printStackTrace();
            return "error";
        }
    }
    }

