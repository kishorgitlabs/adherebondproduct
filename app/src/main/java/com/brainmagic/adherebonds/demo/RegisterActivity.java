package com.brainmagic.adherebond;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.adherebond.R;

import HideKeyboard.HideSoftKeyboard;
import SharedPreference.Shared;
import alertbox.Alertbox;
import api.models.registration.Register;
import api.models.registration.RegisterData;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import home.Home_Activity;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {


    EditText name, email, mobileno, city, country;
    Button okay;
    Spinner usertype, satespinner;
    String Name, Email = "", Mobno, Usertype, SelectedState;
    String[] types = {"Type Of User", "Distributor", "Dealer / Retailer", "Customer", "Adhere Employee", "Others"};
    String status = "Active";
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    String Dbstatus = "";
    String select_email, select_phno;
    private ArrayAdapter<CharSequence> adapter;
    Alertbox box = new Alertbox(RegisterActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_register);

        // Ui
        new HideSoftKeyboard().setupUI(findViewById(R.id.activity_register), this);
        name = (EditText) findViewById(R.id.nameedittext);
        email = (EditText) findViewById(R.id.emailedittext);
        mobileno = (EditText) findViewById(R.id.phoneedittext);
        city = (EditText) findViewById(R.id.citytext);
        country = (EditText) findViewById(R.id.countryetext);
        usertype = (Spinner) findViewById(R.id.usertypespinner);
        satespinner = (Spinner) findViewById(R.id.satetspinner);
        okay = (Button) findViewById(R.id.okay);
        ImageView home = (ImageView) findViewById(R.id.home);
        ImageView  back = (ImageView) findViewById(R.id.back);
        TextView mTitle_text = findViewById(R.id.title_text);
        mTitle_text.setVisibility(View.GONE);
        home.setVisibility(View.GONE);
        back.setVisibility(View.GONE);

        ImageView menu = (ImageView) findViewById(R.id.menubar);
        menu.setVisibility(View.INVISIBLE);

        myshare = getSharedPreferences(Shared.MyPREFERENCES, MODE_PRIVATE);
        editor = myshare.edit();

        adapter = ArrayAdapter.createFromResource(this, R.array.state_array, R.layout.simple_spinner_item);
        //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        satespinner.setAdapter(adapter);


        ArrayAdapter<String> xxx = new ArrayAdapter<String>(RegisterActivity.this, R.layout.simple_spinner_item, types);
        usertype.setAdapter(xxx);


        usertype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                Usertype = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        satespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                SelectedState = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Name = name.getText().toString();
                Mobno = mobileno.getText().toString();
                Email = email.getText().toString();

                String PhoneNo = mobileno.getText().toString();
                String Regex = "[^\\d]";
                String PhoneDigits = PhoneNo.replaceAll(Regex, "");

                if (Name.equals("")) {
                    name.setFocusable(true);
                    name.setError("Enter your name");
                } /*else if(Email.equals("")) {
                        email.setFocusable(true);
                        email.setError("Enter Email");
                    }*/ else if (PhoneDigits.length() != 10) {
                    //Snackbar.make(findViewById(R.id.parentPanel), "Mobile number must be 10 digits", Snackbar.LENGTH_SHORT).show();
                    mobileno.setError("Enter mobile number");
                }/* else if(!isValidEmail(Email.trim())) {
                        email.setFocusable(true);
                        email.setError("Enter Valid Email");
                        Toast toast = Toast.makeText(Register_Activity.this,"Email is not valid", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }*//*else if (SelectedState.equals("Select sate")) {
                        Snackbar.make(findViewById(R.id.parentPanel), "Select state", Snackbar.LENGTH_SHORT).show();
                    }*/ else if (Usertype.equals("Type Of User")) {
                    Toast.makeText(RegisterActivity.this, "Please select user type", Toast.LENGTH_SHORT).show();
                    //Snackbar.make(findViewById(R.id.parentPanel), "Select user type", Snackbar.LENGTH_LONG).show();
                }


                else {

                    checkinternet();
                }
            }
        });

    }

    public static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    private void checkinternet() {
        NetworkConnection connection = new NetworkConnection(RegisterActivity.this);

        if (connection.CheckInternet()) {
            insertUser();
        } else {
            // Toast.makeText(FirstRegistration.this,"Not unable to connect please check your Internet connection ! ", Toast.LENGTH_SHORT).show();
            box.showAlertbox(getString(R.string.no_internet));
        }
    }

    private void insertUser() {


        try {

            final ProgressDialog loading = ProgressDialog.show(RegisterActivity.this, getResources().getString(R.string.app_launcher_name), "Loading...", false, false);
            APIService service = RetroClient.getApiService();
            Call<Register> call = service.register(
                    Name,
                    Mobno,
                    Email,
                    Usertype,
                    city.getText().toString(),
                    SelectedState,
                    country.getText().toString());
            call.enqueue(new Callback<Register>() {
                @Override

                public void onResponse(Call<Register> call, Response<Register> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            SuccessRegister(response.body().getData());
                        } else if(response.body().getResult().equals("NotSuccess")) {
                            box.showAlertbox(getString(R.string.server_error));
                        }else {
                            box.showAlertbox(getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                                box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }

                @Override
                public void onFailure(Call<Register> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }





    }

    private void SuccessRegister(RegisterData data) {

        final AlertDialog alertDialogbox = new AlertDialog.Builder(
                RegisterActivity.this).create();

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertbox, null);
        alertDialogbox.setView(dialogView);

        TextView log = (TextView) dialogView.findViewById(R.id.textView1);
        Button okay = (Button) dialogView.findViewById(R.id.okay);
        log.setText("Thank You For registering.Registration done Successfully");
        okay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                editor.putBoolean(Shared.K_Register, true);
                editor.putString(Shared.K_RegisterName, Name);
                editor.putString(Shared.K_RegisterEmail, Email);
                editor.putString(Shared.K_RegisterMobNo, Mobno);
                editor.putString(Shared.K_RegisterUsertype, Usertype);
                editor.commit();
                Intent go = new Intent(RegisterActivity.this, Home_Activity.class);
                go.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(go);
                alertDialogbox.dismiss();
                finish();
            }
        });
        alertDialogbox.show();
    }

}
