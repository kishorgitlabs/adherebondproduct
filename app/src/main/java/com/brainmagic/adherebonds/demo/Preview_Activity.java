package com.brainmagic.adherebond;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.brainmagic.adherebond.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import Logout.logout;
import SQL_Fields.SQLITE;
import SharedPreference.Shared;
import about.About_Activity;
import adapter.OldView;
import adapter.PreviewAdapter;
import alertbox.AlertDialogue;
import alertbox.Alertbox;
import api.models.ExistingOrder.request.ExistOrder;
import api.models.ExistingOrder.request2.ExistOrders;
import api.models.ExistingOrder.request2.Exist_Orders_List;
import api.models.ExistingOrder.response.OrderResult;
import api.models.Order.SalesOrderAdd;
import api.models.OrderView.DataItem;
import api.models.OrderView.SalesOrderView;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import contact.Contact_Activity;
import database.DBHelpersync;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import product.Product_Activity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import search.Search_Activity;

public class Preview_Activity extends AppCompatActivity {


    final Random myRandom = new Random();
    ImageView home, back,menu;
    private ProgressDialog progressDialog;
    private Button PlaceOrder;
    private String orderNo;
    private Connection connection;
    private Statement stmt;
    private ResultSet rset;
    private ArrayList<String> NameList, ProductList, PackList, QuantityList, DisList, DateList, TypeList,Modelist,Colorlist;
    private ArrayList<Integer> IDList;
    //list from local db to upload in online
    private ArrayList<String> AddCustomercodeList, AddOrderIDList, AddCusnameList, AddProdnameList, AddOrdertypeList, AddPackageList, AddQuantityList, AddDiscountList, AddDateList,Addnumberlist,Addcolorlist;


    private Alertbox alert = new Alertbox(Preview_Activity.this);
    private AlertDialogue alertDialogue = new AlertDialogue(Preview_Activity.this);
    NetworkConnection network = new NetworkConnection(Preview_Activity.this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private SQLiteDatabase db;
    String salesID, SalesName;
    String String_mail, DealerName, MobileNO, ProductPagequantity;
    private ListView Listview;
    //String for get Intent
    //private String Existing;
    //SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    Date converDate = new Date();
    private String SalesEmial, SalesPass, SalesBranch, SalesEmailSign, SalesOrdermail, SalesOrdermobile, SalesPaymentemail, SalesPaymentmobile;
    List<Exist_Orders_List> existOrderslist;
    List<DataItem> data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_);

        myshare = getSharedPreferences(Shared.MyPREFERENCES, Preview_Activity.this.MODE_PRIVATE);
        editor = myshare.edit();
        MobileNO=myshare.getString("mobile","");
data=new ArrayList<DataItem>();
        editor.commit();
        SalesName = myshare.getString(Shared.K_Sales_name, "");
        salesID = myshare.getString(Shared.K_Sales_salesID, "");
        SalesEmial = myshare.getString(Shared.K_Sales_email, "");
        SalesPass = myshare.getString(Shared.K_Sale_emailpass, "");
        SalesEmailSign = myshare.getString(Shared.K_Sales_signature, "");
        SalesBranch = myshare.getString(Shared.K_Sales_salesbranch, "");
        SalesOrdermail = myshare.getString(Shared.K_Sales_salesemail, "");
        SalesOrdermobile = myshare.getString(Shared.K_Sales_salessms, "");
        SalesPaymentemail = myshare.getString(Shared.K_Sales_paymentemail, "");
        SalesPaymentmobile = myshare.getString(Shared.K_Sales_paymentsms, "");
        menu=(ImageView)findViewById(R.id.menubar);

       // SalesEmial = "crm@cerachemindia.com";
       // SalesPass = "crm#2017";

        //Existing = getIntent().getStringExtra("a");

        progressDialog = new ProgressDialog(Preview_Activity.this);
        home = (ImageView) findViewById(R.id.home);
        back = (ImageView) findViewById(R.id.back);
        PlaceOrder = (Button) findViewById(R.id.order);

        Listview = (ListView) findViewById(R.id.listview);

        NameList = new ArrayList<String>();
        ProductList = new ArrayList<String>();
        PackList = new ArrayList<String>();
        QuantityList = new ArrayList<String>();
        DisList = new ArrayList<String>();
        DateList = new ArrayList<String>();
        TypeList = new ArrayList<String>();
        IDList = new ArrayList<Integer>();
        Modelist=new ArrayList<String>();
        Colorlist=new ArrayList<String>();


        AddCustomercodeList = new ArrayList<String>();
        AddOrderIDList = new ArrayList<String>();
        AddCusnameList = new ArrayList<String>();
        AddProdnameList = new ArrayList<String>();
        AddOrdertypeList = new ArrayList<String>();
        AddPackageList = new ArrayList<String>();
        AddQuantityList = new ArrayList<String>();
        AddDiscountList = new ArrayList<String>();
        AddDateList = new ArrayList<String>();
        Addnumberlist = new ArrayList<String>();
        Addcolorlist = new ArrayList<String>();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(Preview_Activity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {


            public void onClick(View view) {


                final PopupMenu popupMenu = new PopupMenu(Preview_Activity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    class Welcome_Activity {
                    }

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(Preview_Activity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(Preview_Activity.this, Product_Activity.class);
                                startActivity(product);
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(Preview_Activity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(Preview_Activity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.watsnew:
                                Intent contact1 = new Intent(Preview_Activity.this, com.brainmagic.adherebond.ProjectActivity.class);
                                startActivity(contact1);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(Preview_Activity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {

                                    Intent b = new Intent(Preview_Activity.this, Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));

                                    Intent b = new Intent(Preview_Activity.this, com.brainmagic.adherebond.Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(Preview_Activity.this).log_out();
                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_login_inner);
                //Whatsnew_Activity.super.onPrepareOptionsMenu(popupMenu);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });

        PlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (network.CheckInternet()) {

//                    placeOrder();
//                    new placeorder().execute();
                    new PlaceOrder().execute();

                    //place_order();
                } else {
                    alert.showAlertbox("Please check your network connection and try again!");
                }

            }
        });

//        getPriviewDetails();

        new GetPreviewDetails().execute();

    }

    private void getPriviewDetails(){

        try {

            APIService service = RetroClient.getApiService();
            Call<SalesOrderView> call = service.orderview(MobileNO,"Existing");
            call.enqueue(new Callback<SalesOrderView>() {
                @Override

                public void onResponse(Call<SalesOrderView> call, Response<SalesOrderView> response) {
                    try {

                        if (response.body().getResult().equals("Success")) {
//                            PreviewAdapter Adapter = new PreviewAdapter(Preview_Activity.this,response.body().getData());
//                            Listview.setAdapter(Adapter);
                        }
                       else if (response.body().getResult().equals("Failure")){
                           alert.showAlertboxwithback("No Record Found");
                        }
                        else
                        {
                            alert.showAlertbox(getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                        alert.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }

                @Override
                public void onFailure(Call<SalesOrderView> call, Throwable t) {

                    t.printStackTrace();
                    alert.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    private class PlaceOrder extends AsyncTask<String, Void, String> {

        DBHelpersync dbhelper = new DBHelpersync(Preview_Activity.this);
        ExistOrders existOrdersList = new ExistOrders();
        String Result;

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Placing Order...");
            progressDialog.show();
            existOrderslist = new ArrayList<Exist_Orders_List>();

            // Calling JSON
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            Log.v("OFF Line", "SQLITE Table");
            try {
                String query = "select * from " + SQLITE.TABLE_Additems;
                Log.v("Query order detail code", query);
                Log.v("OFF Line", "SQLITE Table");

                db = dbhelper.getWritableDatabase();
                Cursor curr = db.rawQuery(query, null);
                if (curr.moveToFirst()) {
                    do {
                        Exist_Orders_List existOrder = new Exist_Orders_List();
                        existOrder.setExecutivename(SalesName);
                        existOrder.setDealername(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Addcustomername)));
                        existOrder.setProductname(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Addproductname)));
                        existOrder.setOrderType(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_AddOrderType)));
                        existOrder.setPackage(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Addpackage)));
                        existOrder.setQuantity(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Addquantity)));
                        existOrder.setDiscount(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Adddiscount)));
                        existOrder.setDate(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Addcurrentdate)));
                        existOrder.setOrdermode(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Addstring_ordermode)));
                        existOrder.setColor(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Addstring_ordercolors)));
                        existOrder.setNumber(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Addstring_ordernumber)));
                        existOrder.setColor(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Addstring_ordercolors)));
                        existOrder.setUnpackage(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Unpackage)));

                        existOrder.setBasicprice(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_basic)));
                        existOrder.setTotalamount(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_total)));

                        existOrder.setStatus("Order Pending");
                        existOrder.setRemarks(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_AddRemarks)));
                        existOrderslist.add(existOrder);
                    } while (curr.moveToNext());
                    curr.close();
                    db.close();
                }

                existOrdersList.setExist_Orders_List(existOrderslist);
                APIService service = RetroClient.getApiService();
                // Calling JSON
                Call<OrderResult> call = service.EXIST_ORDERS_CALL(existOrdersList);
                Response<OrderResult> response = call.execute();
                return response.body().getResult();

            } catch (Exception e) {

                return "Failure";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            switch (result) {
                case "Success":

                    alertDialogue.showAlertbox("Order Placed Successfully");
                    alertDialogue.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            DBHelpersync dbHelper = new DBHelpersync(Preview_Activity.this);
                            SQLiteDatabase db = dbHelper.getWritableDatabase();
                            try {
                                db.execSQL("DROP TABLE " + SQLITE.TABLE_Additems);
                                db.close();
                            } catch (Exception e) {
                                db.close();
                                Log.v("DeleteAging", "Delete");
                            }
                            Intent a = new Intent(Preview_Activity.this, com.brainmagic.adherebond.Sales_Order_Activity.class);
                            a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(a);
                            finish();
                            //Toast.makeText(getApplicationContext(), "Order placed successfully", Toast.LENGTH_LONG).show();
                        }
                    });
                    break;

                case "Failure":

                    alert.showAlertbox("Error in Placing Order");
                    break;
                default:

                    alert.showAlertbox("Internet Connection is slow! Please try again!");
                    break;
            }


        }



    }


    private class GetPreviewDetails extends AsyncTask<String, Void, String> {
        DBHelpersync dbhelper = new DBHelpersync(Preview_Activity.this);
        Cursor cursor;

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            db = dbhelper.getWritableDatabase();

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            Log.v("OFF Line", "SQLITE Table");
            try {
                //String query ="select * from Orderpreview order by formattedDate asc";
                String query = "select * from " + SQLITE.TABLE_Additems + " order by formattedDate asc";
                Log.v("Dealer Name", query);
                cursor = db.rawQuery(query, null);
                if (cursor.moveToFirst()) {
                    do {
                        NameList.add(cursor.getString(cursor.getColumnIndex("CustomerName")));
                        ProductList.add(cursor.getString(cursor.getColumnIndex("ProductName")));
                        PackList.add(cursor.getString(cursor.getColumnIndex("Package")));
                        QuantityList.add(cursor.getString(cursor.getColumnIndex("quantity")));
                        DisList.add(cursor.getString(cursor.getColumnIndex("discount")));
                        DateList.add(cursor.getString(cursor.getColumnIndex("formattedDate")));
                        TypeList.add(cursor.getString(cursor.getColumnIndex("OrderType")));
                        Modelist.add(cursor.getString(cursor.getColumnIndex("order_mode")));
                        Colorlist.add(cursor.getString(cursor.getColumnIndex("colours")));
                        IDList.add(cursor.getInt(cursor.getColumnIndex("id")));
                    } while (cursor.moveToNext());
                    cursor.close();
                    db.close();
                    return "received";
                } else {
                    cursor.close();
                    db.close();
                    return "nodata";
                }
            } catch (Exception e) {
                cursor.close();
                db.close();
                Log.v("Error in get date", "catch is working");
                Log.v("Error in Incentive", e.getMessage());
                return "notsuccess";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            switch (result) {
                case "received":
                    PreviewAdapter Adapter = new PreviewAdapter(Preview_Activity.this, IDList, NameList, ProductList, PackList, QuantityList, DisList, DateList, TypeList,Modelist,Colorlist);
                    Listview.setAdapter(Adapter);
                    break;
                case "nodata":
                    alert.showAlertbox("No Orders Found");
                    break;
                default:
                    alert.showAlertbox("Error in Preview");
                    break;
            }

        }

    }

    public static String replacestring(String string) {

        String newstrin = "";
        try {
            if (!string.equals(""))
                newstrin = string.replaceAll("[^0-9./]", "");
            if (newstrin.contains("/")) {
                newstrin = "0.5";
            }
            char[] chars = string.toLowerCase().toCharArray();
            boolean found = false;
            for (int i = 0; i < chars.length; i++) {
                if (!found && Character.isLetter(chars[i])) {
                    chars[i] = Character.toUpperCase(chars[i]);
                    found = true;
                } else if (Character.isWhitespace(chars[i]) || chars[i] == '.' || chars[i] == '&') { // You can add other chars here
                    found = false;
                }
            }
            return String.valueOf(chars);
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
    }

    public static String capitalizeString(String string) {
        try

        {
            char[] chars = string.toLowerCase().toCharArray();
            boolean found = false;
            for (int i = 0; i < chars.length; i++) {
                if (!found && Character.isLetter(chars[i])) {
                    chars[i] = Character.toUpperCase(chars[i]);
                    found = true;
                } else if (Character.isWhitespace(chars[i]) || chars[i] == '.' || chars[i] == '&') { // You can add other chars here
                    found = false;
                }
            }
            return String.valueOf(chars);
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
    }
}
