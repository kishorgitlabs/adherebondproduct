package com.brainmagic.adherebond;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;

import com.brainmagic.adherebond.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import Logout.logout;
import SQL_Fields.SQLITE;
import SharedPreference.Shared;
import about.About_Activity;
import adapter.NewPreviewAdapter;
import adapter.PreviewAdapter;
import alertbox.AlertDialogue;
import alertbox.Alertbox;
import api.models.NewOrder.request.NewOrders;
import api.models.NewOrder.request.NewOrdersList;
import api.models.NewOrder.response.NewOrderResult;
import api.models.NewOrderView.NewOrdersView;
import api.models.OrderView.SalesOrderView;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import contact.Contact_Activity;
import database.DBHelpersync;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import product.Product_Activity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import search.Search_Activity;

public class Preview_New_Activity extends AppCompatActivity {
    final Random myRandom = new Random();
    ImageView home, back,menu;

    private ProgressDialog progressDialog;
    private Button PlaceOrder;
    private String orderNo;
    private Connection connection;
    private Statement stmt;
    private ResultSet rset;
    private ArrayList<String> NameList, ProductList, PackList, QuantityList, DisList, DateList, TypeList,Modelist,Colorlist;
    private ArrayList<Integer> IDList;
    //list from local db to upload in online
    private ArrayList<String> AddCustomercodeList, AddOrderIDList, AddCusnameList, AddProdnameList, AddOrdertypeList, AddPackageList, AddQuantityList, AddDiscountList, AddDateList;
    //private String NewOrder;

    private Alertbox alert = new Alertbox(Preview_New_Activity.this);
    private AlertDialogue alertDialogue = new AlertDialogue(Preview_New_Activity.this);
    NetworkConnection network = new NetworkConnection(Preview_New_Activity.this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private SQLiteDatabase db;
    String salesID, SalesName;
    String String_mail, DealerName, MobileNO, ProductPagequantity;
    public String emailad, cmpnymsg;
    private ListView Listview;
    //SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    Date converDate = new Date();
    //private String SalesOrdermail,SalesOrdermobile;
    private String MarketingpersionEmail, MarketingpersionMobile, SalesEmial, SalesPass, SalesBranch, SalesEmailSign;
    private String SalesOrdermail, SalesOrdermobile;
    List<NewOrdersList> newOrderList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview__new_);

        myshare = getSharedPreferences("Registration", Preview_New_Activity.this.MODE_PRIVATE);
        editor = myshare.edit();
        menu=(ImageView)findViewById(R.id.menubar);
        SalesName = myshare.getString("name", "");
        salesID = myshare.getString("salesID", "");
        MarketingpersionEmail = myshare.getString("email", "");
        MarketingpersionMobile = myshare.getString("mobile", "");
        SalesOrdermail = myshare.getString(Shared.K_Sales_salesemail, "");
        SalesOrdermobile = myshare.getString(Shared.K_Sales_salessms, "");
        SalesEmailSign = myshare.getString("signature", "");
        SalesBranch = myshare.getString("salesbranch", "");
        SalesEmial = myshare.getString(Shared.K_Sales_email, "");
        SalesPass = myshare.getString(Shared.K_Sale_emailpass, "");

        SalesEmial = "crm@cerachemindia.com";
        SalesPass = "crm#2017";
        MobileNO=myshare.getString("mobile","");

        editor.commit();
        progressDialog = new ProgressDialog(Preview_New_Activity.this);
        home = (ImageView) findViewById(R.id.home);
        back = (ImageView) findViewById(R.id.back);
        PlaceOrder = (Button) findViewById(R.id.order);

        Listview = (ListView) findViewById(R.id.listview);

        NameList = new ArrayList<String>();
        ProductList = new ArrayList<String>();
        PackList = new ArrayList<String>();
        QuantityList = new ArrayList<String>();
        DisList = new ArrayList<String>();
        DateList = new ArrayList<String>();
        TypeList = new ArrayList<String>();
        IDList = new ArrayList<Integer>();


        AddCustomercodeList = new ArrayList<String>();
        AddOrderIDList = new ArrayList<String>();
        AddCusnameList = new ArrayList<String>();
        AddProdnameList = new ArrayList<String>();
        AddOrdertypeList = new ArrayList<String>();
        AddPackageList = new ArrayList<String>();
        AddQuantityList = new ArrayList<String>();
        AddDiscountList = new ArrayList<String>();
        AddDateList = new ArrayList<String>();
        Modelist=new ArrayList<String>();
        Colorlist=new ArrayList<>();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(Preview_New_Activity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {


            public void onClick(View view) {


                final PopupMenu popupMenu = new PopupMenu(Preview_New_Activity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(Preview_New_Activity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(Preview_New_Activity.this, Product_Activity.class);
                                startActivity(product);
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(Preview_New_Activity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(Preview_New_Activity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.watsnew:
                                Intent contact1 = new Intent(Preview_New_Activity.this, com.brainmagic.adherebond.ProjectActivity.class);
                                startActivity(contact1);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(Preview_New_Activity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {

                                    Intent b = new Intent(Preview_New_Activity.this, com.brainmagic.adherebond.Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));

                                    Intent b = new Intent(Preview_New_Activity.this, com.brainmagic.adherebond.Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(Preview_New_Activity.this).log_out();
                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_login_inner);
                //Whatsnew_Activity.super.onPrepareOptionsMenu(popupMenu);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });

        PlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (network.CheckInternet()) {
                    new PlaceOrder().execute();
                    //place_order();
                } else {
                    alert.showAlertbox("Please check your network connection and try again!");
                }

            }
        });

        new GetPreviewDetails().execute();

//        getPriviewDetails();
    }


    private void getPriviewDetails(){

        try {

            APIService service = RetroClient.getApiService();
            Call<NewOrdersView> call = service.neworderview(MobileNO,"New");
            call.enqueue(new Callback<NewOrdersView>() {
                @Override

                public void onResponse(Call<NewOrdersView> call, Response<NewOrdersView> response) {
                    try {

                        if (response.body().getResult().equals("Success")) {
//                            NewPreviewAdapter Adapter = new NewPreviewAdapter(Preview_New_Activity.this,response.body().getData());
//                            Listview.setAdapter(Adapter);
                        }
                        else if (response.body().getResult().equals("Failure")){
                            alert.showAlertboxwithback("No Record Found");
                        }
                        else
                        {
                            alert.showAlertbox(getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                        alert.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }

                @Override
                public void onFailure(Call<NewOrdersView> call, Throwable t) {

                    t.printStackTrace();
                    alert.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private class PlaceOrder extends AsyncTask<String, Void, String> {

        DBHelpersync dbhelper = new DBHelpersync(Preview_New_Activity.this);
        NewOrders newOrdersList = new NewOrders();

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Placing Order...");
            progressDialog.show();
            newOrderList = new ArrayList<NewOrdersList>();
            // Calling JSON
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            Log.v("OFF Line", "SQLITE Table");
            try {
                String query = "select * from " + SQLITE.TABLE_NewPartsOrder;
                Log.v("Query order detail code", query);
                Log.v("OFF Line", "SQLITE Table");

                db = dbhelper.getWritableDatabase();
                Cursor curr = db.rawQuery(query, null);
                if (curr.moveToFirst()) {
                    do {
                        NewOrdersList newOrder = new NewOrdersList();
                        newOrder.setExecutivename(SalesName);
                        newOrder.setDealername(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_dealername)));
                        newOrder.setAddress(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_address)));
                        //newOrder.setDeliveryaddress(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_deliveryaddress)));
                        newOrder.setCity(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_city)));
                        newOrder.setState(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_state)));
                        newOrder.setPincode(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_pincode)));
                        newOrder.setContact(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_contact)));
                        newOrder.setContactname(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_contactname)));
                        newOrder.setEmail(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_email)));
                        newOrder.setCst(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_gst)));
                        newOrder.setPan(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_pan)));
                        newOrder.setCustype(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_custype)));
                        newOrder.setOrdermode(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_ordermode)));
                        newOrder.setProductname(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_prodname)));
                        newOrder.setOrderType(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_orderType)));
                        newOrder.setPackage(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_pack)));
                        newOrder.setPackage2(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_pack)).replaceAll("[^0-9./ ]", ""));
                        newOrder.setQuantity(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_quant)));
                        newOrder.setDiscount(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_discounts)));
                        newOrder.setDate(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_date)));
                        newOrder.setNumber(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_ordernumber)));
                        newOrder.setColor(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_ordercolours)));

                        newOrder.setBasicprice(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_basic)));
                        newOrder.setTotalamount(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_total)));

                        newOrder.setStatus("Order Pending");
                        newOrder.setRemarks(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_remark)));
                        newOrder.setmUnpackage(curr.getString(curr.getColumnIndex(SQLITE.COLUMN_Unpackage)));


                        newOrderList.add(newOrder);
                    } while (curr.moveToNext());
                    curr.close();
                    db.close();
                }

                newOrdersList.setNewOrdersList(newOrderList);
                APIService service = RetroClient.getApiService();
                Call<NewOrderResult> call = service.NEW_ORDER_RESULT_CALL(newOrdersList);
                Response<NewOrderResult> response = call.execute();
                return response.body().getResult();


            } catch (Exception e) {

                return "Failure";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            switch (result) {
                case "Success":
                    DBHelpersync dbHelper = new DBHelpersync(Preview_New_Activity.this);
                    SQLiteDatabase db = dbHelper.getWritableDatabase();
                    try {
                        db.execSQL("DROP TABLE " + SQLITE.TABLE_NewPartsOrder);
                        db.execSQL("DROP TABLE " + SQLITE.TABLE_Create_Profile);
                        db.close();
                    } catch (Exception e) {
                        db.close();
                        Log.v("Delete New order", "Delete");
                    }
                    alertDialogue.showAlertbox("Order Placed Successfully");
                    alertDialogue.alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {

                            Intent a = new Intent(Preview_New_Activity.this, com.brainmagic.adherebond.Sales_OrderNew_Activity.class);
                            a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(a);
                            finish();
                        }
                    });
                    break;

                case "Failure":

                    alert.showAlertbox("Error in Placing Order");
                    break;
                default:
                    Log.v("Default Placing order", "");

                    alert.showAlertbox("Internet Connection is slow! Please try again!");
                    break;
            }


        }

        String API_call() {
            try {
                APIService service = RetroClient.getApiService();
                Call<NewOrderResult> call = service.NEW_ORDER_RESULT_CALL(newOrdersList);
                Response<NewOrderResult> response = call.execute();
                return response.body().toString();
                /*call.enqueue(new Callback<NewOrderResult>() {
                    @Override
                    public void onResponse(Call<NewOrderResult> call, Response<NewOrderResult> response) {
                        Result = String.format("%s", response.body().getResult());
                    }

                    @Override
                    public void onFailure(Call<NewOrderResult> call, Throwable t) {
                        // progressDialog.dismiss();
                        Result = "Failure";
                        Log.v("Error in On failure", "");
                        // alert.showAlertbox("Internet Connection is slow! Please try again!");
                    }
                });*/
                //return Result;

            } catch (Exception ex) {
                Log.v("APICall Error", ex.getMessage());
                return "Failure";
            }
        }

    }




    private class GetPreviewDetails extends AsyncTask<String, Void, String> {
        DBHelpersync dbhelper = new DBHelpersync(Preview_New_Activity.this);
        Cursor cursor;

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            db = dbhelper.getWritableDatabase();

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            Log.v("OFF Line", "SQLITE Table");
            try {
                //String query ="select * from NewOrderpreview order by date asc";
                String query = "select * from " + SQLITE.TABLE_NewPartsOrder + " order by date asc";
                Log.v("Dealer Name", query);
                cursor = db.rawQuery(query, null);
                if (cursor.moveToFirst()) {
                    do {

                        NameList.add(cursor.getString(cursor.getColumnIndex("dealername")));
                        ProductList.add(cursor.getString(cursor.getColumnIndex("productname")));
                        PackList.add(cursor.getString(cursor.getColumnIndex("package")));
                        QuantityList.add(cursor.getString(cursor.getColumnIndex("quantity")));
                        DisList.add(cursor.getString(cursor.getColumnIndex("discount")));
                        DateList.add(cursor.getString(cursor.getColumnIndex("date")));
                        TypeList.add(cursor.getString(cursor.getColumnIndex("orderType")));
                        Modelist.add(cursor.getString(cursor.getColumnIndex("orderMode")));
                        Colorlist.add(cursor.getString(cursor.getColumnIndex("colours")));
                        IDList.add(cursor.getInt(cursor.getColumnIndex("id")));
                    } while (cursor.moveToNext());
                    cursor.close();
                    db.close();
                    return "received";
                } else {
                    cursor.close();
                    db.close();
                    return "nodata";
                }
            } catch (Exception e) {
                cursor.close();
                db.close();
                Log.v("Error in get date", "catch is working");
                Log.v("Error in Incentive", e.getMessage());
                return "notsuccess";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            switch (result) {
                case "received":
                    NewPreviewAdapter Adapter = new NewPreviewAdapter(Preview_New_Activity.this, IDList, NameList, ProductList, PackList, QuantityList, DisList, DateList, TypeList,Modelist,Colorlist);
                    Listview.setAdapter(Adapter);
                    break;
                case "nodata":
                    alert.showAlertbox("No Orders Found");
                    break;
                default:
                    //alert.showAlertbox("Error in Preview");
                    break;
            }

        }

    }

    public static String capitalizeString(String string) {
        try

        {
            char[] chars = string.toLowerCase().toCharArray();
            boolean found = false;
            for (int i = 0; i < chars.length; i++) {
                if (!found && Character.isLetter(chars[i])) {
                    chars[i] = Character.toUpperCase(chars[i]);
                    found = true;
                } else if (Character.isWhitespace(chars[i]) || chars[i] == '.' || chars[i] == '&') { // You can add other chars here
                    found = false;
                }
            }
            return String.valueOf(chars);
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
    }
    }

