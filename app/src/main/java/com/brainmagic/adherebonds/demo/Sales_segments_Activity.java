package com.brainmagic.adherebond;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brainmagic.adherebond.R;
import com.brainmagic.adherebonds.demo.Mobile_Orderstatus_Activity;
import com.brainmagic.adherebonds.demo.New_Mobile_Orderstatus_Activity;

import Logout.logout;
import about.About_Activity;
import alertbox.Alertbox;
import contact.Contact_Activity;
import enquiry.Enquiry_Activity;
import network.NetworkConnection;
import product.Product_Activity;
import search.Search_Activity;

public class Sales_segments_Activity extends AppCompatActivity {
    ImageView home, back, menu;
    RelativeLayout Order,History,Target,ViewMobileorders,Industry,OrderStatus,vistors;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;

    NetworkConnection network = new NetworkConnection(Sales_segments_Activity.this);
    Alertbox alert = new Alertbox(Sales_segments_Activity.this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_segments_);

        Order = (RelativeLayout)findViewById(R.id.order);
        OrderStatus = (RelativeLayout)findViewById(R.id.status);
        History = (RelativeLayout)findViewById(R.id.history);
        vistors=(RelativeLayout)findViewById(R.id.vistors);
        myshare = getSharedPreferences("Registration",Sales_segments_Activity.this.MODE_PRIVATE);
        editor = myshare.edit();

        home = (ImageView) findViewById(R.id.home);
        back = (ImageView) findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(Sales_segments_Activity.this, Sales_segments_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });
        menu=(ImageView)findViewById(R.id.menubar);
        menu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                final PopupMenu popupMenu = new PopupMenu(Sales_segments_Activity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(Sales_segments_Activity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(Sales_segments_Activity.this, Product_Activity.class);
                                startActivity(product);
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(Sales_segments_Activity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(Sales_segments_Activity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.watsnew:
                                Intent contact1 = new Intent(Sales_segments_Activity.this, ProjectActivity.class);
                                startActivity(contact1);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(Sales_segments_Activity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {

                                    Intent b = new Intent(Sales_segments_Activity.this, Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));

                                    Intent b = new Intent(Sales_segments_Activity.this, Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(Sales_segments_Activity.this).log_out();
                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_login_inner);
                //Whatsnew_Activity.super.onPrepareOptionsMenu(popupMenu);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });
        Order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final AlertDialog alertDialog = new AlertDialog.Builder(Sales_segments_Activity.this).create();

                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.order_alertbox, null);
                alertDialog.setView(dialogView);

                TextView existing = (TextView) dialogView.findViewById(R.id.text2);
                TextView newcustomer = (TextView) dialogView.findViewById(R.id.text3);
                existing.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub
                        alertDialog.dismiss();

                        startActivity(new Intent(Sales_segments_Activity.this, com.brainmagic.adherebond.Sales_Order_Activity.class));
                    }
                });

                newcustomer.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub
                        getnewoptions();

                        // /startActivity(new Intent(Sales_segments_Activity.this,Sales_OrderNew_Activity.class));
                        alertDialog.dismiss();
                    }
                });

                alertDialog.show();




            }

            private void getnewoptions() {
                final AlertDialog alertDialog = new AlertDialog.Builder(Sales_segments_Activity.this).create();

                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.new_order_alertbox, null);
                alertDialog.setView(dialogView);

                TextView existing = (TextView) dialogView.findViewById(R.id.text2);
                TextView newcustomer = (TextView) dialogView.findViewById(R.id.text3);

                existing.setText("Create Profile");
                newcustomer.setText("Order");
                existing.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub

                        alertDialog.dismiss();

                        startActivity(new Intent(Sales_segments_Activity.this, com.brainmagic.adherebond.Create_Customer_Activity.class));
                    }
                });

                newcustomer.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub

                        startActivity(new Intent(Sales_segments_Activity.this, com.brainmagic.adherebond.Sales_OrderNew_Activity.class));
                        alertDialog.dismiss();
                    }
                });

                alertDialog.show();


            }

        });

        vistors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



               final AlertDialog alertDialog = new AlertDialog.Builder(Sales_segments_Activity.this).create();

                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.order_alertbox, null);
                alertDialog.setView(dialogView);

                TextView existing = (TextView) dialogView.findViewById(R.id.text2);
                TextView newcustomer = (TextView) dialogView.findViewById(R.id.text3);
                existing.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub
                        alertDialog.dismiss();

                        startActivity(new Intent(Sales_segments_Activity.this,DailyOrderActivity.class));
                    }
                });

                newcustomer.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub
                       // getnewoptions();
                        startActivity(new Intent(Sales_segments_Activity.this, com.brainmagic.adherebond.Sales_OrderNew_Activity.class));
                        // /startActivity(new Intent(Sales_segments_Activity.this,Sales_OrderNew_Activity.class));
                        alertDialog.dismiss();
                    }
                });

                alertDialog.show();




            }

           private void getnewoptions() {
                final AlertDialog alertDialog = new AlertDialog.Builder(Sales_segments_Activity.this).create();

                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.new_order_alertbox, null);
                alertDialog.setView(dialogView);

                TextView existing = (TextView) dialogView.findViewById(R.id.text2);
                TextView newcustomer = (TextView) dialogView.findViewById(R.id.text3);

                existing.setText("Create Profile");
                newcustomer.setText("Order");
                existing.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub

                        alertDialog.dismiss();

                        startActivity(new Intent(Sales_segments_Activity.this,CreateCustomerVisitorActivity.class));
                    }
                });

                newcustomer.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub

                        startActivity(new Intent(Sales_segments_Activity.this, com.brainmagic.adherebond.Sales_OrderNew_Activity.class));
                        alertDialog.dismiss();
                    }
                });

                alertDialog.show();

            }
        });
        OrderStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final AlertDialog alertDialog = new AlertDialog.Builder(Sales_segments_Activity.this).create();

                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.order_alertbox, null);
                alertDialog.setView(dialogView);

                TextView existing = (TextView) dialogView.findViewById(R.id.text2);
                TextView newcustomer = (TextView) dialogView.findViewById(R.id.text3);
                existing.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub
                        alertDialog.dismiss();
                        startActivity(new Intent(Sales_segments_Activity.this, Mobile_Orderstatus_Activity.class));
                        //startActivity(new Intent(Sales_segments_Activity.this,Sales_Order_Activity.class));
                    }
                });

                newcustomer.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub
                        startActivity(new Intent(Sales_segments_Activity.this, New_Mobile_Orderstatus_Activity.class));
                        alertDialog.dismiss();
                    }
                });

                alertDialog.show();


            }
        });
        History.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent detalis = new Intent(Sales_segments_Activity.this,Sales_History_Activity.class);
                startActivity(detalis);
            }
        });



    }
}
