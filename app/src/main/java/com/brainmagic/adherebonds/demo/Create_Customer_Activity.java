package com.brainmagic.adherebond;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.Toast;

import com.brainmagic.adherebond.R;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import HideKeyboard.HideSoftKeyboard;
import Logout.logout;
import SQL_Fields.SQLITE;
import about.About_Activity;
import alertbox.Alertbox;
import contact.Contact_Activity;
import database.DBHelper;
import database.DBHelpersync;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import offers.Offers_Activity;
import product.Product_Activity;
import search.Search_Activity;
import whatsnew.Whatsnew_Activity;

public class Create_Customer_Activity extends AppCompatActivity {
    ImageView home,back;
    NetworkConnection network = new NetworkConnection(Create_Customer_Activity.this);
    private Alertbox box = new Alertbox(Create_Customer_Activity.this);
    private android.widget.EditText Name,Address,City,Pincode,Contact,ContactPerson,EmailID,GST,Pan,DeliveryAddress;
    private Spinner GSTSpinner,TypeCusSpinner,StateSpinner,Subcustypespinner;
    String usertype;
    private ArrayList<String> GSTList,TypeCusSpinnerList,StateSpinnerList;

    private SQLiteDatabase db2;
    private TableRow EditText,SubCategory;
    private Button Create;
    private String S_cusname,S_address,S_city,S_state,S_pincode,S_contact,S_contactperson,S_email,S_gst,S_pan,S_custype,S_subcustype,S_deliveryaddress;
    private SharedPreferences myshare;
    private String PartName,salesID,SalesName,SalesEmial,SalesPass,SalesBranch,SalesEmailSign,SalesOrdermail,SalesOrdermobile;
    private SharedPreferences.Editor editor;
    private ProgressDialog progressDialog;
    private Connection connection;
    private Statement stmt;
    public List<String> msgmobileno;
    public List<String> msgemail;
    public String emailad="";
    private ImageView menu;
    private ArrayAdapter<CharSequence> adapter;
    private String cmpnymsg;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create__customer_);

        new HideSoftKeyboard().setupUI(findViewById(R.id.activity_create),this);
        myshare = getSharedPreferences("Registration", Create_Customer_Activity.this.MODE_PRIVATE);
        editor = myshare.edit();
        salesID = myshare.getString("salesID", "");
        SalesName = myshare.getString("name", "");
        SalesEmial = myshare.getString("email", "");
        SalesPass = myshare.getString("emailpass", "");
        SalesEmailSign = myshare.getString("signature", "");
        SalesBranch = myshare.getString("salesbranch", "");
        SalesOrdermail = myshare.getString("salesemail", "");
        SalesOrdermobile = myshare.getString("salessms", "");
        EditText = (TableRow)findViewById(R.id.edittext);
        SubCategory = (TableRow)findViewById(R.id.SubCategory);
        Name = (EditText)findViewById(R.id.name_edit);
        Address = (EditText)findViewById(R.id.address_edit);
        City = (EditText)findViewById(R.id.city_edit);

        Pincode = (EditText)findViewById(R.id.pincode_edit);
        Contact = (EditText)findViewById(R.id.contactno_edit);
        ContactPerson = (EditText)findViewById(R.id.contactperson_edit);
        EmailID = (EditText)findViewById(R.id.email_edit);
        GST = (EditText)findViewById(R.id.gst_edit);
        Pan = (EditText)findViewById(R.id.panno_edit);
        DeliveryAddress = (EditText)findViewById(R.id.deliveryaddress);

        GST.setText("No");
        Create = (Button)findViewById(R.id.create);
        GSTSpinner = (Spinner)findViewById(R.id.gstspinner);
        TypeCusSpinner = (Spinner)findViewById(R.id.custypespinner);

        StateSpinner = (Spinner)findViewById(R.id.state_spinner);
        Subcustypespinner = (Spinner)findViewById(R.id.subcustypespinner);
        GSTList = new ArrayList<String>();

        TypeCusSpinnerList = new ArrayList<String>();

        StateSpinnerList = new ArrayList<String>();





        msgmobileno = new ArrayList<String>();
        msgemail = new ArrayList<String>();

        StateSpinnerList.add("Select");
        StateSpinnerList.add("Andhra Pradesh");
        StateSpinnerList.add("Assam");
        StateSpinnerList.add("Chhattisgarh");
        StateSpinnerList.add("Delhi");
        StateSpinnerList.add("Goa");
        StateSpinnerList.add("Gujarat");
        StateSpinnerList.add("Haryana");
        StateSpinnerList.add("Himachal Pradesh");
        StateSpinnerList.add("JammuKashmir");
        StateSpinnerList.add("Jharkhand");
        StateSpinnerList.add("Karnataka");
        StateSpinnerList.add("Kerala");
        StateSpinnerList.add("Lakshadweep");
        StateSpinnerList.add("Madhya Pradesh");
        StateSpinnerList.add("Maharashtra");
        StateSpinnerList.add("Manipur");
        StateSpinnerList.add("Megalaya");
        StateSpinnerList.add("Mizoram");
        StateSpinnerList.add("Nagaland");
        StateSpinnerList.add("Nepal");
        StateSpinnerList.add("New Delhi");
        StateSpinnerList.add("Odisha");
        StateSpinnerList.add("Puducherry");
        StateSpinnerList.add("Punjab");
        StateSpinnerList.add("Rajasthan");
        StateSpinnerList.add("Sikkim");
        StateSpinnerList.add("Tamil Nadu");
        StateSpinnerList.add("Telangana");
        StateSpinnerList.add("Tripura");
        StateSpinnerList.add("Uttar Pradesh");
        StateSpinnerList.add("Uttarakhand");
        StateSpinnerList.add("Visakhapatnam");
        StateSpinnerList.add("West Bengal");





        GSTList.add("Select");
        GSTList.add("Yes");
        GSTList.add("No");

        TypeCusSpinnerList.add("Select");
        TypeCusSpinnerList.add("Retailers");
        TypeCusSpinnerList.add("Builders");
        TypeCusSpinnerList.add("Developers");
        TypeCusSpinnerList.add("Traders");
        TypeCusSpinnerList.add("Dealers");
        TypeCusSpinnerList.add("Distributors");
        TypeCusSpinnerList.add("Contractors");
        TypeCusSpinnerList.add("Architects");
        TypeCusSpinnerList.add("Consultants");
        TypeCusSpinnerList.add("Inerior designers");
        TypeCusSpinnerList.add("Paver block");
        TypeCusSpinnerList.add("Precast");
        TypeCusSpinnerList.add("Govt. sectors");
        TypeCusSpinnerList.add("Manufacturer");
        TypeCusSpinnerList.add("Industries");
        TypeCusSpinnerList.add("Pvt. Sectors");
        TypeCusSpinnerList.add("Residential Apartments");
        TypeCusSpinnerList.add("Individuals");
        TypeCusSpinnerList.add("AAC block");


        home = (ImageView) findViewById(R.id.home);
        back = (ImageView) findViewById(R.id.back);
        menu=(ImageView)findViewById(R.id.menubar);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(Create_Customer_Activity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });


        TypeCusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String sp1=String.valueOf(TypeCusSpinner.getSelectedItem());
                if(sp1.contentEquals("Retailers")){
                    SubCategory.setVisibility(View.VISIBLE);
                    List<String> list = new ArrayList<String>();
                    list.add("Select");
                    list.add("Hardware shops");
                    list.add("Tile Dealers");
                    list.add("Marble Dealers");
                    list.add("Sanitarywares");
                    list.add("Paint Shops");
                    ArrayAdapter<String> sub = new ArrayAdapter<String>(Create_Customer_Activity.this, R.layout.simple_spinner_item, list);
                    sub.setNotifyOnChange(true);
                    Subcustypespinner.setAdapter(sub);
                }
                else if(sp1.contentEquals("Govt. sectors")){
                    SubCategory.setVisibility(View.VISIBLE);
                    List<String> list = new ArrayList<String>();
                    list.add("Select");
                    list.add("PWD");
                    list.add("CPWD");
                    list.add("Railways");
                    list.add("Port Trust");

                    ArrayAdapter<String> sub = new ArrayAdapter<String>(Create_Customer_Activity.this, R.layout.simple_spinner_item, list);
                    sub.setNotifyOnChange(true);
                    Subcustypespinner.setAdapter(sub);
                }else
                {
                    SubCategory.setVisibility(View.GONE);
                }
            }


            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        menu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                final PopupMenu popupMenu = new PopupMenu(Create_Customer_Activity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(Create_Customer_Activity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(Create_Customer_Activity.this, Product_Activity.class);
                                startActivity(product);
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(Create_Customer_Activity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(Create_Customer_Activity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.watsnew:
                                Intent contact1 = new Intent(Create_Customer_Activity.this, ProjectActivity.class);
                                startActivity(contact1);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(Create_Customer_Activity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {

                                    Intent b = new Intent(Create_Customer_Activity.this, Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));

                                    Intent b = new Intent(Create_Customer_Activity.this, Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(Create_Customer_Activity.this).log_out();
                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_login_inner);
                //Whatsnew_Activity.super.onPrepareOptionsMenu(popupMenu);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });

        GSTSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //I.E. if in the height spinner CM is selected I would like to hide the second height edittext field.
                // I'm not sure if this is meant to be "height1" or "height"
                if (position == 0){

                    EditText.setVisibility(View.GONE);

                } else if(position == 1)
                {
                    EditText.setVisibility(View.VISIBLE);
                    GST.setText("");
                }else if(position == 2)
                {
                    GST.setText("No");
                    EditText.setVisibility(View.GONE);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        StateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                S_state = StateSpinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Create.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                S_cusname = Name.getText().toString();
                S_address = Address.getText().toString();
                S_city = City.getText().toString();
                S_state = StateSpinner.getSelectedItem().toString();
                S_pincode = Pincode.getText().toString();
                S_contact = Contact.getText().toString();
                S_contactperson = ContactPerson.getText().toString();
                S_email = EmailID.getText().toString();
                S_gst = GST.getText().toString();
                S_pan = Pan.getText().toString();
                S_custype = TypeCusSpinner.getSelectedItem().toString();
                S_deliveryaddress = DeliveryAddress.getText().toString();


                if(S_cusname.equals("")|| S_address.equals("")|| S_city.equals("")|| S_state.equals("")|| S_contact.equals("")||
                        S_contactperson.equals("")|| S_custype.equals("Select"))
                {
                    Toast.makeText(getApplicationContext(), "Kindly enter all fields", Toast.LENGTH_LONG).show();
                } else if(S_custype.equals("Retailers") || S_custype.equals("Govt. sectors"))
                {
                    S_custype = Subcustypespinner.getSelectedItem().toString();
                    if(S_custype.equals("Select"))
                        Toast.makeText(getApplicationContext(), "Select Sub Category", Toast.LENGTH_LONG).show();
                    else
                    {
                        new savetolocal().execute();
                    }
                }else
                {
                    new savetolocal().execute();
                }
            }
        });

        new GetSpinners().execute();

    }


    private class savetolocal extends AsyncTask<String, Void, String> {


        ContentValues values = new ContentValues();
        private double success;
        SQLiteDatabase db;
        private int a;

        @SuppressLint("InflateParams")
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Create_Customer_Activity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Saving local...");
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... args0)
        {

            try {
                values.put(SQLITE.COLUMN_Create_cusname , S_cusname );
                values.put(SQLITE.COLUMN_Create_address , S_address);
                values.put(SQLITE.COLUMN_Create_city , S_city );
                values.put(SQLITE.COLUMN_Create_state , S_state);
                values.put(SQLITE.COLUMN_Create_pincode ,S_pincode );
                values.put(SQLITE.COLUMN_Create_contact , S_contact);
                values.put(SQLITE.COLUMN_Create_contactname , S_contactperson);
                values.put(SQLITE.COLUMN_Create_email ,S_email );
                values.put(SQLITE.COLUMN_Create_gst ,S_gst );
                values.put(SQLITE.COLUMN_Create_pan , S_pan);
                values.put(SQLITE.COLUMN_Create_custype , S_custype);
                values.put(SQLITE.COLUMN_Create_deliveryaddress , S_deliveryaddress);


                DBHelpersync dbHelper = new DBHelpersync(Create_Customer_Activity.this);
                db = dbHelper.getWritableDatabase();
                dbHelper.CreateCustomerProfile(db);
                success =  db.insert(SQLITE.TABLE_Create_Profile, null, values);
                db.close();
                return "received";

            } catch (Exception e)
            {
                Log.v("Error in Add local db ","catch is working");
                Log.v("Error in Add local db", e.getMessage());
                return "notsuccess";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if(result.equals("received"))
            {
                Log.v("success", Double.toString(success));
                startActivity(new Intent(Create_Customer_Activity.this,Sales_OrderNew_Activity.class));
                finish();
                Toast.makeText(getApplicationContext(), "Profile Created for "+S_cusname, Toast.LENGTH_LONG).show();

            }
            else
            {
                box.showAlertbox("Error in adding to local");
            }
        }
    }








    public class GetSpinners extends AsyncTask<String, Void, String>
    {

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            try
            {
                DBHelper dbHelper = new DBHelper(Create_Customer_Activity.this);
                db2 = dbHelper.readDataBase();
                String query ="select state from States";
                Log.v("States for state",query);
                Cursor cursor = db2.rawQuery(query, null);
                if(cursor.moveToFirst())
                {
                    do
                    {
                        StateSpinnerList.add(cursor.getString(cursor.getColumnIndex("state")));

                    } while (cursor.moveToNext());

                }
                db2.close();
                cursor.close();
                Log.v("OFF Line", "SQLITE Table");
                return "received";

            }catch (Exception e)
            {
                Log.v("Error in get date","catch is working");
                Log.v("Error in Incentive", e.getMessage());
                return "notsuccess";
            }

        }
        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            ArrayAdapter<String> Cusadapter = new ArrayAdapter<String>(Create_Customer_Activity.this, R.layout.simple_spinner_item, StateSpinnerList);
            Cusadapter.setNotifyOnChange(true);
            StateSpinner.setAdapter(Cusadapter);

            ArrayAdapter<String> CSTadapter = new ArrayAdapter<String>(Create_Customer_Activity.this, R.layout.simple_spinner_item,GSTList );
            CSTadapter.setNotifyOnChange(true);
            GSTSpinner.setAdapter(CSTadapter);

            ArrayAdapter<String> Cusadaper = new ArrayAdapter<String>(Create_Customer_Activity.this, R.layout.simple_spinner_item, TypeCusSpinnerList);
            Cusadaper.setNotifyOnChange(true);
            TypeCusSpinner.setAdapter(Cusadaper);
        }
    }



}

