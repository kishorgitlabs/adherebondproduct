package com.brainmagic.adherebond

import android.content.Intent
import android.net.Uri
//import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.brainmagic.adherebond.R
//import com.brainmagic.adherebonds.R
import com.halilibo.bettervideoplayer.BetterVideoCallback
import com.halilibo.bettervideoplayer.BetterVideoPlayer
import home.Home_Activity
import kotlinx.android.synthetic.main.activity_splash.*
import java.lang.Exception

class SplashActivity : AppCompatActivity(), BetterVideoCallback {

    private var str_url: String = "http://adherebonds.com/assets/img/ppt.mp4"
    // private var str_url: String = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"
    private var isFirtsTime = false;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        videoview.setSource(Uri.parse(str_url))
        videoview.start();


        videoview.setCallback(this);
        gotohome.setOnClickListener(View.OnClickListener { v ->
            videoview.pause();
            startActivity(Intent(this@SplashActivity, Home_Activity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
        })

    }


    override fun onPause() {
        super.onPause()
        videoview.pause();
    }

    override fun onPrepared(player: BetterVideoPlayer?) {

    }

    override fun onStarted(player: BetterVideoPlayer?) {

    }

    override fun onCompletion(player: BetterVideoPlayer) {

        if (isFirtsTime)
            startActivity(Intent(this@SplashActivity, Home_Activity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
        isFirtsTime = true;
    }

    override fun onBuffering(percent: Int) {
    }

    override fun onPreparing(player: BetterVideoPlayer?) {
    }

    override fun onError(player: BetterVideoPlayer?, e: Exception?) {
    }

    override fun onToggleControls(player: BetterVideoPlayer?, isShowing: Boolean) {
    }

    override fun onPaused(player: BetterVideoPlayer?) {
    }


}


