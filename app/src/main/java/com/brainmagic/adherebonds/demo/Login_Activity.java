package com.brainmagic.adherebond;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.brainmagic.adherebond.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import HideKeyboard.HideSoftKeyboard;
import SharedPreference.Shared;
import about.About_Activity;
import alertbox.Alertbox;
import alertbox.ForgetPasswordAlert;
import api.models.login.Login;
import api.models.login.LoginData;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import contact.Contact_Activity;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import product.Product_Activity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import search.Search_Activity;

public class Login_Activity extends AppCompatActivity {
    ImageView home, back, menu;
    EditText Username, Password;
    Button Loginbtn, cancel;
    Connection connection;
    Statement statement;
    ResultSet resultSet;
    TextView Forget;
    private ProgressDialog progress;
    String user, pass;
    Alertbox box = new Alertbox(Login_Activity.this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String salesName,salesPass,salesEmail,salesMobile,salesEmailPass,salesEmailSign,salesID,salesBranchcode,SalesOrdermail,SalesOrdermobile,SalesPaymentemail,SalesPaymentmobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login_);
        new HideSoftKeyboard().setupUI(findViewById(R.id.activity_login),this);
        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        // initial views
        home = (ImageView) findViewById(R.id.home);
        back = (ImageView) findViewById(R.id.back);
        Username = (EditText) findViewById(R.id.username);
        Password = (EditText) findViewById(R.id.password);
        Loginbtn = (Button) findViewById(R.id.login);
        cancel = (Button) findViewById(R.id.cancel);
        Forget = (TextView)findViewById(R.id.forget);

        TextView mTitle_text = findViewById(R.id.title_text);
        mTitle_text.setText("Login");

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(Login_Activity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });

        menu = (ImageView) findViewById(R.id.menubar);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Login_Activity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub
                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(Login_Activity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(Login_Activity.this, Product_Activity.class);
                                product.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                product.putExtra("a","");
                                startActivity(product);
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(Login_Activity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(Login_Activity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(Login_Activity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.watsnew:
                                Intent enquiry1 = new Intent(Login_Activity.this, com.brainmagic.adherebond.ProjectActivity.class);
                                startActivity(enquiry1);
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popupmenu_login);
                popupMenu.show();

            }
        });

    }

    public void Oncancel(View view) {
        onBackPressed();
    }

    public void Onforgot(View view) {
        ForgetPasswordAlert forget = new ForgetPasswordAlert(Login_Activity.this);
        forget.showLoginbox();
    }

    public void Onlogin(View view) {

        user = Username.getText().toString();
        pass = Password.getText().toString();
        if (user.equals("")) {
            Username.setError("Enter Username");
        } else if (pass.equals("")) {
            Password.setError("Enter Password");
        } else {
            CheckInternet();
        }
    }


    private void CheckInternet() {

            NetworkConnection network = new NetworkConnection(Login_Activity.this);
            if (network.CheckInternet()) {
               // new verifylogin().execute();
                //askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, WRITE_EXST);
                CheckUserLogin();
            } else {
                Alertbox alert = new Alertbox(Login_Activity.this);
                alert.showAlertbox("Kindly check your Internet Connection");
            }


    }


    private void CheckUserLogin() {

        try {

            final ProgressDialog loading = ProgressDialog.show(Login_Activity.this, getResources().getString(R.string.app_launcher_name), "Loading...", false, false);
            APIService service = RetroClient.getApiService();
            Call<Login> call = service.
                    Login(Username.getText().toString(), Password.getText().toString());
            call.enqueue(new Callback<Login>() {
                @Override

                public void onResponse(Call<Login> call, Response<Login> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {

                            if(response.body().getData().getUserType().equals("SalesExecutive")){
                                SuccessLogin(response.body().getData());
                            }else {
                                SuccessLoginpayment(response.body().getData());
                            }

                        } else if(response.body().getResult().equals("InvalidUser")) {
                            box.showAlertbox("Invalid User!");
                        }else
                        {
                            box.showAlertbox(getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }

                @Override
                public void onFailure(Call<Login> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void SuccessLoginpayment(final LoginData data) {
        final AlertDialog dialog = new AlertDialog.Builder(Login_Activity.this).create();
        dialog.setTitle(R.string.app_launcher_name);
        dialog.setMessage("Login Successfull");
        dialog.setButton("Okay", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                editor.putBoolean("isLogin", true);
                editor.putBoolean(Shared.K_Login, true);
                editor.putString(Shared.K_Sales_name, data.getUserName());
                editor.putString(Shared.K_Sales_pass, data.getPassword());
                editor.putString(Shared.K_Sales_email, data.getEmailId());
                editor.putString(Shared.K_Sales_mobile, data.getMobileNo());
                editor.putString(Shared.K_Sale_emailpass, data.getEmailPassword());
                editor.putString(Shared.K_Sales_signature, data.getEmailSignature());
                editor.putString(Shared.K_Sales_salesID, data.getId());
                editor.putString("usertype",data.getUserType());
                editor.putString(Shared.K_Sales_salesbranch,data.getBranchCode());
                editor.putString(Shared.K_Sales_salesemail, data.getSalesemail());
                editor.putString(Shared.K_Sales_salessms, data.getSalessms());
                editor.putString(Shared.K_Sales_paymentemail, data.getPaymentemail());
                editor.putString(Shared.K_Sales_paymentsms, data.getPaymentsms());
                editor.putBoolean(Shared.K_Synchronize,false);
                editor.commit();

                Intent i = new Intent(Login_Activity.this, com.brainmagic.adherebond.PaymentWelcomeActivity.class);
                startActivity(i);
            }
        });
        dialog.show();

    }
    


    private void SuccessLogin(final LoginData data) {

        final AlertDialog dialog = new AlertDialog.Builder(Login_Activity.this).create();
        dialog.setTitle(R.string.app_launcher_name);
        dialog.setMessage("Login Successfull");
        dialog.setButton("Okay", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                editor.putBoolean("isLogin", true);
                editor.putBoolean(Shared.K_Login, true);
                editor.putString(Shared.K_Sales_name, data.getUserName());
                editor.putString(Shared.K_Sales_pass, data.getPassword());
                editor.putString(Shared.K_Sales_email, data.getEmailId());
                editor.putString(Shared.K_Sales_mobile, data.getMobileNo());
                editor.putString(Shared.K_Sale_emailpass, data.getEmailPassword());
                editor.putString("usertype",data.getUserType());
                editor.putString(Shared.K_Sales_signature, data.getEmailSignature());
                editor.putString(Shared.K_Sales_salesID, data.getId());
                editor.putString(Shared.K_Sales_salesbranch,data.getBranchCode());
                editor.putString(Shared.K_Sales_salesemail, data.getSalesemail());
                editor.putString(Shared.K_Sales_salessms, data.getSalessms());
                editor.putString(Shared.K_Sales_paymentemail, data.getPaymentemail());
                editor.putString(Shared.K_Sales_paymentsms, data.getPaymentsms());
                editor.putBoolean(Shared.K_Synchronize,false);
                editor.commit();

                Intent i = new Intent(Login_Activity.this, com.brainmagic.adherebond.Welcome_Activity.class);
                startActivity(i);
            }
        });
        dialog.show();

    }
}
