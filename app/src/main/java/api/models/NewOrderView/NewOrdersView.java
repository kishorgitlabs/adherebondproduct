package api.models.NewOrderView;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class NewOrdersView{

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private List<NewOrderItem> data;

	public String getResult(){
		return result;
	}

	public List<NewOrderItem> getData(){
		return data;
	}
}