package api.models.project;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Project {
    @SerializedName("Image9")
    @Expose
    private Object Image9;
    @SerializedName("Image8")
    @Expose
    private Object Image8;
    @SerializedName("Description")
    @Expose
    private String Description;
    @SerializedName("Image7")
    @Expose
    private Object Image7;
    @SerializedName("Image6")
    @Expose
    private Object Image6;
    @SerializedName("ProjectName")
    @Expose
    private String ProjectName;
    @SerializedName("Image5")
    @Expose
    private String Image5;
    @SerializedName("Image4")
    @Expose
    private String Image4;
    @SerializedName("Image3")
    @Expose
    private String Image3;
    @SerializedName("Image2")
    @Expose
    private String Image2;
    @SerializedName("Image1")
    @Expose
    private String Image1;
    @SerializedName("Image10")
    @Expose
    private Object Image10;
    @SerializedName("id")
    @Expose
    private Integer id;
    public void setImage9(Object Image9){
        this.Image9=Image9;
    }
    public Object getImage9(){
        return Image9;
    }
    public void setImage8(Object Image8){
        this.Image8=Image8;
    }
    public Object getImage8(){
        return Image8;
    }
    public void setDescription(String Description){
        this.Description=Description;
    }
    public String getDescription(){
        return Description;
    }
    public void setImage7(Object Image7){
        this.Image7=Image7;
    }
    public Object getImage7(){
        return Image7;
    }
    public void setImage6(Object Image6){
        this.Image6=Image6;
    }
    public Object getImage6(){
        return Image6;
    }
    public void setProjectName(String ProjectName){
        this.ProjectName=ProjectName;
    }
    public String getProjectName(){
        return ProjectName;
    }
    public void setImage5(String Image5){
        this.Image5=Image5;
    }
    public String getImage5(){
        return Image5;
    }
    public void setImage4(String Image4){
        this.Image4=Image4;
    }
    public String getImage4(){
        return Image4;
    }
    public void setImage3(String Image3){
        this.Image3=Image3;
    }
    public String getImage3(){
        return Image3;
    }
    public void setImage2(String Image2){
        this.Image2=Image2;
    }
    public String getImage2(){
        return Image2;
    }
    public void setImage1(String Image1){
        this.Image1=Image1;
    }
    public String getImage1(){
        return Image1;
    }
    public void setImage10(Object Image10){
        this.Image10=Image10;
    }
    public Object getImage10(){
        return Image10;
    }
    public void setId(Integer id){
        this.id=id;
    }
    public Integer getId(){
        return id;
    }
}