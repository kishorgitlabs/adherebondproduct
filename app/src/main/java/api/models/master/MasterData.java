package api.models.master;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class MasterData {
  @SerializedName("Amt_list")
  @Expose
  private List<Amt_list> Amt_list;
  @SerializedName("Customer_list")
  @Expose
  private List<Customer_list> Customer_list;
  @SerializedName("PAl_list")
  @Expose
  private List<PAl_list> PAl_list;
  public void setAmt_list(List<Amt_list> Amt_list){
   this.Amt_list=Amt_list;
  }
  public List<Amt_list> getAmt_list(){
   return Amt_list;
  }
  public void setCustomer_list(List<Customer_list> Customer_list){
   this.Customer_list=Customer_list;
  }
  public List<Customer_list> getCustomer_list(){
   return Customer_list;
  }
  public void setPAl_list(List<PAl_list> PAl_list){
   this.PAl_list=PAl_list;
  }
  public List<PAl_list> getPAl_list(){
   return PAl_list;
  }
}