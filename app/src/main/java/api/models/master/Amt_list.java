package api.models.master;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Awesome Pojo Generator
 */
@Entity(tableName = "VMAgeing")
public class Amt_list {


    @ColumnInfo(name = "Id")
    @PrimaryKey(autoGenerate = true)
    private int Id;


    @ColumnInfo(name = "Expr9")
    private String Expr9;

    @ColumnInfo(name = "SalesPersonName")
    private String SalesPersonName;

    @ColumnInfo(name = "Expr5")
    private String Expr5;

    @ColumnInfo(name = "LRNo")
    private String LRNo;

    @ColumnInfo(name = "Expr6")
    private String Expr6;

    @ColumnInfo(name = "InsertDate")
    private String InsertDate;

    @ColumnInfo(name = "Expr8")
    private String Expr8;

    @ColumnInfo(name = "TransportCompany")
    private String TransportCompany;

    @ColumnInfo(name = "CreditAmount")
    private String CreditAmount;

    @ColumnInfo(name = "PartyName")
    private String PartyName;

    @ColumnInfo(name = "Name")
    private String Name;

    @ColumnInfo(name = "Date1")
    private String Date1;

    @ColumnInfo(name = "Particulars")
    private String Particulars;

    @ColumnInfo(name = "UpdateDate")
    private String UpdateDate;

    @ColumnInfo(name = "UpdateDateTime")
    private String UpdateDateTime;

    @ColumnInfo(name = "EmailId")
    private String EmailId;

    @ColumnInfo(name = "address1")
    private String address1;

    @ColumnInfo(name = "RefNo")
    private String RefNo;

    @ColumnInfo(name = "CreditLimit")
    private String CreditLimit;

    @ColumnInfo(name = "DueOn")
    private String DueOn;

    @ColumnInfo(name = "OverDueByDay")
    private String OverDueByDay;

    @ColumnInfo(name = "Mobile")
    private String Mobile;

    @ColumnInfo(name = "deletedate")
    private String deletedate;

    @ColumnInfo(name = "Date")
    private String Date;

    @ColumnInfo(name = "ContactPerson1")
    private String ContactPerson1;

    @ColumnInfo(name = "VchNo")
    private String VchNo;

    @ColumnInfo(name = "CreditDays")
    private String CreditDays;

    @ColumnInfo(name = "Expr1")
    private String Expr1;

    @ColumnInfo(name = "Expr2")
    private String Expr2;

    @ColumnInfo(name = "Expr10")
    private String Expr10;


    @ColumnInfo(name = "Expr4")
    private String Expr4;

    @ColumnInfo(name = "updatetime")
    private String updatetime;

    @ColumnInfo(name = "VchType")
    private String VchType;

    @ColumnInfo(name = "DebitAmount")
    private String DebitAmount;

    @ColumnInfo(name = "PendingAmount")
    private String PendingAmount;

    public void setExpr9(String Expr9) {
        this.Expr9 = Expr9;
    }

    public String getExpr9() {
        return Expr9;
    }

    public void setSalesPersonName(String SalesPersonName) {
        this.SalesPersonName = SalesPersonName;
    }

    public String getSalesPersonName() {
        return SalesPersonName;
    }

    public void setExpr5(String Expr5) {
        this.Expr5 = Expr5;
    }

    public String getExpr5() {
        return Expr5;
    }

    public void setLRNo(String LRNo) {
        this.LRNo = LRNo;
    }

    public String getLRNo() {
        return LRNo;
    }

    public void setExpr6(String Expr6) {
        this.Expr6 = Expr6;
    }

    public String getExpr6() {
        return Expr6;
    }

    public void setInsertDate(String InsertDate) {
        this.InsertDate = InsertDate;
    }

    public String getInsertDate() {
        return InsertDate;
    }

    public void setExpr8(String Expr8) {
        this.Expr8 = Expr8;
    }

    public String getExpr8() {
        return Expr8;
    }

    public void setTransportCompany(String TransportCompany) {
        this.TransportCompany = TransportCompany;
    }

    public String getTransportCompany() {
        return TransportCompany;
    }

    public void setCreditAmount(String CreditAmount) {
        this.CreditAmount = CreditAmount;
    }

    public String getCreditAmount() {
        return CreditAmount;
    }

    public void setPartyName(String PartyName) {
        this.PartyName = PartyName;
    }

    public String getPartyName() {
        return PartyName;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getName() {
        return Name;
    }

    public void setDate1(String Date1) {
        this.Date1 = Date1;
    }

    public String getDate1() {
        return Date1;
    }

    public void setParticulars(String Particulars) {
        this.Particulars = Particulars;
    }

    public String getParticulars() {
        return Particulars;
    }

    public void setUpdateDate(String UpdateDate) {
        this.UpdateDate = UpdateDate;
    }

    public String getUpdateDate() {
        return UpdateDate;
    }

    public void setUpdateDateTime(String UpdateDateTime) {
        this.UpdateDateTime = UpdateDateTime;
    }

    public String getUpdateDateTime() {
        return UpdateDateTime;
    }

    public void setEmailId(String EmailId) {
        this.EmailId = EmailId;
    }

    public String getEmailId() {
        return EmailId;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress1() {
        return address1;
    }

    public void setRefNo(String RefNo) {
        this.RefNo = RefNo;
    }

    public String getRefNo() {
        return RefNo;
    }

    public void setCreditLimit(String CreditLimit) {
        this.CreditLimit = CreditLimit;
    }

    public String getCreditLimit() {
        return CreditLimit;
    }

    public void setDueOn(String DueOn) {
        this.DueOn = DueOn;
    }

    public String getDueOn() {
        return DueOn;
    }

    public void setOverDueByDay(String OverDueByDay) {
        this.OverDueByDay = OverDueByDay;
    }

    public String getOverDueByDay() {
        return OverDueByDay;
    }

    public void setMobile(String Mobile) {
        this.Mobile = Mobile;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setDeletedate(String deletedate) {
        this.deletedate = deletedate;
    }

    public String getDeletedate() {
        return deletedate;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getDate() {
        return Date;
    }

    public void setContactPerson1(String ContactPerson1) {
        this.ContactPerson1 = ContactPerson1;
    }

    public String getContactPerson1() {
        return ContactPerson1;
    }

    public void setVchNo(String VchNo) {
        this.VchNo = VchNo;
    }

    public String getVchNo() {
        return VchNo;
    }

    public void setCreditDays(String CreditDays) {
        this.CreditDays = CreditDays;
    }

    public String getCreditDays() {
        return CreditDays;
    }

    public void setExpr1(String Expr1) {
        this.Expr1 = Expr1;
    }

    public String getExpr1() {
        return Expr1;
    }

    public void setExpr2(String Expr2) {
        this.Expr2 = Expr2;
    }

    public String getExpr2() {
        return Expr2;
    }

    public void setExpr10(String Expr10) {
        this.Expr10 = Expr10;
    }

    public String getExpr10() {
        return Expr10;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public int getId() {
        return Id;
    }

    public void setExpr4(String Expr4) {
        this.Expr4 = Expr4;
    }

    public String getExpr4() {
        return Expr4;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setVchType(String VchType) {
        this.VchType = VchType;
    }

    public String getVchType() {
        return VchType;
    }

    public void setDebitAmount(String DebitAmount) {
        this.DebitAmount = DebitAmount;
    }

    public String getDebitAmount() {
        return DebitAmount;
    }

    public void setPendingAmount(String PendingAmount) {
        this.PendingAmount = PendingAmount;
    }

    public String getPendingAmount() {
        return PendingAmount;
    }
}