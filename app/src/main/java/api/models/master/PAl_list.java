package api.models.master;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Awesome Pojo Generator
 * */
@Entity(tableName = "VWCustomerBalance")
public class PAl_list{

  @ColumnInfo(name = "Id")
  @PrimaryKey(autoGenerate = true)
  private int id;

  @ColumnInfo(name = "ContactPerson1")

  private String ContactPerson1;
  @ColumnInfo(name = "SalesPersonName")

  private String SalesPersonName;
  @ColumnInfo(name = "RefNo")

  private String RefNo;
  @ColumnInfo(name = "DueOn")

  private String DueOn;
  @ColumnInfo(name = "Expr1")
  private String Expr1;
  @ColumnInfo(name = "OverDueByDay")

  private String OverDueByDay;
  @ColumnInfo(name = "PartyName")

  private String PartyName;
  @ColumnInfo(name = "Mobile")

  private String Mobile;
  @ColumnInfo(name = "PendingAmount")

  private String PendingAmount;
  @ColumnInfo(name = "Name")

  private String Name;
  public void setContactPerson1(String ContactPerson1){
   this.ContactPerson1=ContactPerson1;
  }
  public String getContactPerson1(){
   return ContactPerson1;
  }
  public void setSalesPersonName(String SalesPersonName){
   this.SalesPersonName=SalesPersonName;
  }
  public String getSalesPersonName(){
   return SalesPersonName;
  }
  public void setRefNo(String RefNo){
   this.RefNo=RefNo;
  }
  public String getRefNo(){
   return RefNo;
  }
  public void setDueOn(String DueOn){
   this.DueOn=DueOn;
  }
  public String getDueOn(){
   return DueOn;
  }
  public void setExpr1(String Expr1){
   this.Expr1=Expr1;
  }
  public String getExpr1(){
   return Expr1;
  }
  public void setOverDueByDay(String OverDueByDay){
   this.OverDueByDay=OverDueByDay;
  }
  public String getOverDueByDay(){
   return OverDueByDay;
  }
  public void setPartyName(String PartyName){
   this.PartyName=PartyName;
  }
  public String getPartyName(){
   return PartyName;
  }
  public void setId(int id){
   this.id=id;
  }
  public int getId(){
   return id;
  }
  public void setMobile(String Mobile){
   this.Mobile=Mobile;
  }
  public String getMobile(){
   return Mobile;
  }
  public void setPendingAmount(String PendingAmount){
   this.PendingAmount=PendingAmount;
  }
  public String getPendingAmount(){
   return PendingAmount;
  }
  public void setName(String Name){
   this.Name=Name;
  }
  public String getName(){
   return Name;
  }


}