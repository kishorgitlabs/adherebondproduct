package api.models.commen;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Awesome Pojo Generator
 * */
public class CommenResponce{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private String data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(String data){
   this.data=data;
  }
  public String getData(){
   return data;
  }
}