package api.models;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("traveldate")
	private String traveldate;

	@SerializedName("usertype")
	private String usertype;

	@SerializedName("otheramt")
	private String otheramt;

	@SerializedName("remark")
	private String remark;

	@SerializedName("totamt")
	private String totamt;

	@SerializedName("Mobile")
	private String mobile;

	@SerializedName("Name")
	private String name;

	@SerializedName("openkm")
	private String openkm;

	@SerializedName("mode")
	private String mode;

	@SerializedName("fuelamt")
	private String fuelamt;

	@SerializedName("foodamt")
	private String foodamt;

	@SerializedName("insertdate")
	private String insertdate;

	@SerializedName("Id")
	private String id;

	@SerializedName("closekm")
	private String closekm;

	@SerializedName("ticketamt")
	private String ticketamt;

	public String getTraveldate(){
		return traveldate;
	}

	public String getUsertype(){
		return usertype;
	}

	public String getOtheramt(){
		return otheramt;
	}

	public String getRemark(){
		return remark;
	}

	public String getTotamt(){
		return totamt;
	}

	public String getMobile(){
		return mobile;
	}

	public String getName(){
		return name;
	}

	public String getOpenkm(){
		return openkm;
	}

	public String getMode(){
		return mode;
	}

	public String getFuelamt(){
		return fuelamt;
	}

	public String getFoodamt(){
		return foodamt;
	}

	public String getInsertdate(){
		return insertdate;
	}

	public String getId(){
		return id;
	}

	public String getClosekm(){
		return closekm;
	}

	public String getTicketamt(){
		return ticketamt;
	}
}