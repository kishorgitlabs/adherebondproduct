package api.models.ExistingOrder.request2;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Exist_Orders_List{
  @SerializedName("date")
  @Expose
  private String date;
  @SerializedName("orderType")
  @Expose
  private String orderType;
  @SerializedName("amount")
  @Expose
  private String amount;
  @SerializedName("package")
  @Expose
  private String packag;
  @SerializedName("quantity")
  @Expose
  private String quantity;
  @SerializedName("ordermode")
  @Expose
  private String ordermode;
  @SerializedName("executivename")
  @Expose
  private String executivename;
  @SerializedName("Color")
  @Expose
  private String Color;
  @SerializedName("discount")
  @Expose
  private String discount;
  @SerializedName("dealername")
  @Expose
  private String dealername;
  @SerializedName("Number")
  @Expose
  private String Number;

    public String getPackag() {
        return packag;
    }

    public void setPackag(String packag) {
        this.packag = packag;
    }

    public String getBasicprice() {
        return basicprice;
    }

    public void setBasicprice(String basicprice) {
        this.basicprice = basicprice;
    }

    public String getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount;
    }

    @SerializedName("productname")
  @Expose
  private String productname;
  @SerializedName("remarks")
  @Expose
  private String remarks;

  @SerializedName("status")
  @Expose
  private String status;


    @SerializedName("basicprice")
    @Expose
    private String basicprice;

    @SerializedName("totalamount")
    @Expose
    private String totalamount;



    public String getUnpackage() {
        return Unpackage;
    }

    public void setUnpackage(String unpackage) {
        Unpackage = unpackage;
    }

    @SerializedName("Unpackage")
    @Expose
    private String Unpackage;
  public void setDate(String date){
   this.date=date;
  }
  public String getDate(){
   return date;
  }
  public void setOrderType(String orderType){
   this.orderType=orderType;
  }
  public String getOrderType(){
   return orderType;
  }
  public void setAmount(String amount){
   this.amount=amount;
  }
  public String getAmount(){
   return amount;
  }
  public void setPackage(String packag){
   this.packag=packag;
  }
  public String getPackage(){
   return packag;
  }
  public void setQuantity(String quantity){
   this.quantity=quantity;
  }
  public String getQuantity(){
   return quantity;
  }
  public void setOrdermode(String ordermode){
   this.ordermode=ordermode;
  }
  public String getOrdermode(){
   return ordermode;
  }
  public void setExecutivename(String executivename){
   this.executivename=executivename;
  }
  public String getExecutivename(){
   return executivename;
  }
  public void setColor(String Color){
   this.Color=Color;
  }
  public String getColor(){
   return Color;
  }
  public void setDiscount(String discount){
   this.discount=discount;
  }
  public String getDiscount(){
   return discount;
  }
  public void setDealername(String dealername){
   this.dealername=dealername;
  }
  public String getDealername(){
   return dealername;
  }
  public void setNumber(String Number){
   this.Number=Number;
  }
  public String getNumber(){
   return Number;
  }
  public void setProductname(String productname){
   this.productname=productname;
  }
  public String getProductname(){
   return productname;
  }
  public void setRemarks(String remarks){
   this.remarks=remarks;
  }
  public String getRemarks(){
   return remarks;
  }
  public void setStatus(String status){
   this.status=status;
  }
  public String getStatus(){
   return status;
  }
}