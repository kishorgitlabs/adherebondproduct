package api.models.ExistingOrder.request;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class ExistOrder {
  @SerializedName("date")
  @Expose
  private String date;
  @SerializedName("ModeofTransport")
  @Expose
  private Object ModeofTransport;
  @SerializedName("orderType")
  @Expose
  private String orderType;
  @SerializedName("amount")
  @Expose
  private String amount;
  @SerializedName("package")
  @Expose
  private String packag;
  @SerializedName("quantity")
  @Expose
  private String quantity;
  @SerializedName("Description")
  @Expose
  private Object Description;
  @SerializedName("LrNo")
  @Expose
  private Object LrNo;
  @SerializedName("orderid")
  @Expose
  private String orderid;
  @SerializedName("executivename")
  @Expose
  private String executivename;
  @SerializedName("inserteddate")
  @Expose
  private String inserteddate;
  @SerializedName("discount")
  @Expose
  private String discount;
  @SerializedName("package2")
  @Expose
  private String package2;
  @SerializedName("unitprice")
  @Expose
  private String unitprice;
  @SerializedName("Branchcode")
  @Expose
  private String Branchcode;
  @SerializedName("Voucher")
  @Expose
  private Object Voucher;
  @SerializedName("dealername")
  @Expose
  private String dealername;
  @SerializedName("dealercode")
  @Expose
  private String dealercode;
  @SerializedName("productname")
  @Expose
  private String productname;
  @SerializedName("remarks")
  @Expose
  private String remarks;
  @SerializedName("ReferenceNo")
  @Expose
  private Object ReferenceNo;
  @SerializedName("status")
  @Expose
  private String status;
  @SerializedName("shiptocity")
  @Expose
  private Object shiptocity;
  public void setDate(String date){
   this.date=date;
  }
  public String getDate(){
   return date;
  }
  public void setModeofTransport(Object ModeofTransport){
   this.ModeofTransport=ModeofTransport;
  }
  public Object getModeofTransport(){
   return ModeofTransport;
  }
  public void setOrderType(String orderType){
   this.orderType=orderType;
  }
  public String getOrderType(){
   return orderType;
  }
  public void setAmount(String amount){
   this.amount=amount;
  }
  public String getAmount(){
   return amount;
  }
  public void setPackage(String packag){
   this.packag=packag;
  }
  public String getPackage(){
   return packag;
  }
  public void setQuantity(String quantity){
   this.quantity=quantity;
  }
  public String getQuantity(){
   return quantity;
  }
  public void setDescription(Object Description){
   this.Description=Description;
  }
  public Object getDescription(){
   return Description;
  }
  public void setLrNo(Object LrNo){
   this.LrNo=LrNo;
  }
  public Object getLrNo(){
   return LrNo;
  }
  public void setOrderid(String orderid){
   this.orderid=orderid;
  }
  public String getOrderid(){
   return orderid;
  }
  public void setExecutivename(String executivename){
   this.executivename=executivename;
  }
  public String getExecutivename(){
   return executivename;
  }
  public void setInserteddate(String inserteddate){
   this.inserteddate=inserteddate;
  }
  public String getInserteddate(){
   return inserteddate;
  }
  public void setDiscount(String discount){
   this.discount=discount;
  }
  public String getDiscount(){
   return discount;
  }
  public void setPackage2(String package2){
   this.package2=package2;
  }
  public String getPackage2(){
   return package2;
  }
  public void setUnitprice(String unitprice){
   this.unitprice=unitprice;
  }
  public String getUnitprice(){
   return unitprice;
  }
  public void setBranchcode(String Branchcode){
   this.Branchcode=Branchcode;
  }
  public String getBranchcode(){
   return Branchcode;
  }
  public void setVoucher(Object Voucher){
   this.Voucher=Voucher;
  }
  public Object getVoucher(){
   return Voucher;
  }
  public void setDealername(String dealername){
   this.dealername=dealername;
  }
  public String getDealername(){
   return dealername;
  }
  public void setDealercode(String dealercode){
   this.dealercode=dealercode;
  }
  public String getDealercode(){
   return dealercode;
  }
  public void setProductname(String productname){
   this.productname=productname;
  }
  public String getProductname(){
   return productname;
  }
  public void setRemarks(String remarks){
   this.remarks=remarks;
  }
  public String getRemarks(){
   return remarks;
  }
  public void setReferenceNo(Object ReferenceNo){
   this.ReferenceNo=ReferenceNo;
  }
  public Object getReferenceNo(){
   return ReferenceNo;
  }
  public void setStatus(String status){
   this.status=status;
  }
  public String getStatus(){
   return status;
  }
  public void setShiptocity(Object shiptocity){
   this.shiptocity=shiptocity;
  }
  public Object getShiptocity(){
   return shiptocity;
  }
}