package api.models.ExistingOrder.request;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class ExistOrdersList {
  @SerializedName("ExistOrder")
  @Expose
  private List<ExistOrder> Exist_Orders_List;
  public void setExist_Orders_List(List<ExistOrder> Exist_Orders_List){
   this.Exist_Orders_List=Exist_Orders_List;
  }
  public List<ExistOrder> getExist_Orders_List(){
   return Exist_Orders_List;
  }
}