package api.models.enquiry;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Enqiry {
  @SerializedName("ContactNo")
  @Expose
  private Long ContactNo;
  @SerializedName("Description")
  @Expose
  private String Description;
  @SerializedName("Email")
  @Expose
  private String Email;
  @SerializedName("Address")
  @Expose
  private String Address;
  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("InsertTime")
  @Expose
  private String InsertTime;
  @SerializedName("Name")
  @Expose
  private String Name;
  public void setContactNo(Long ContactNo){
   this.ContactNo=ContactNo;
  }
  public Long getContactNo(){
   return ContactNo;
  }
  public void setDescription(String Description){
   this.Description=Description;
  }
  public String getDescription(){
   return Description;
  }
  public void setEmail(String Email){
   this.Email=Email;
  }
  public String getEmail(){
   return Email;
  }
  public void setAddress(String Address){
   this.Address=Address;
  }
  public String getAddress(){
   return Address;
  }
  public void setId(Integer id){
   this.id=id;
  }
  public Integer getId(){
   return id;
  }
  public void setInsertTime(String InsertTime){
   this.InsertTime=InsertTime;
  }
  public String getInsertTime(){
   return InsertTime;
  }
  public void setName(String Name){
   this.Name=Name;
  }
  public String getName(){
   return Name;
  }
}