package api.models.Expense;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("traveldate")
	private Object traveldate;

	@SerializedName("usertype")
	private String usertype;

	@SerializedName("otheramt")
	private double otheramt;

	@SerializedName("remark")
	private String remark;

	@SerializedName("totamt")
	private double totamt;

	@SerializedName("Mobile")
	private String mobile;

	@SerializedName("Name")
	private String name;

	@SerializedName("openkm")
	private int openkm;

	@SerializedName("mode")
	private Object mode;

	@SerializedName("fuelamt")
	private double fuelamt;

	@SerializedName("foodamt")
	private double foodamt;

	@SerializedName("insertdate")
	private Object insertdate;

	@SerializedName("Id")
	private int id;

	@SerializedName("closekm")
	private int closekm;

	@SerializedName("ticketamt")
	private double ticketamt;

	public Object getTraveldate(){
		return traveldate;
	}

	public String getUsertype(){
		return usertype;
	}

	public double getOtheramt(){
		return otheramt;
	}

	public String getRemark(){
		return remark;
	}

	public double getTotamt(){
		return totamt;
	}

	public String getMobile(){
		return mobile;
	}

	public String getName(){
		return name;
	}

	public int getOpenkm(){
		return openkm;
	}

	public Object getMode(){
		return mode;
	}

	public double getFuelamt(){
		return fuelamt;
	}

	public double getFoodamt(){
		return foodamt;
	}

	public Object getInsertdate(){
		return insertdate;
	}

	public int getId(){
		return id;
	}

	public int getClosekm(){
		return closekm;
	}

	public double getTicketamt(){
		return ticketamt;
	}
}