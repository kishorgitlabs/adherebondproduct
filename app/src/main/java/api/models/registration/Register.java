package api.models.registration;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Register{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private RegisterData data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(RegisterData data){
   this.data=data;
  }
  public RegisterData getData(){
   return data;
  }
}