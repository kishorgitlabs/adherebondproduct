package api.models.login;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class LoginData {
  @SerializedName("MobileNo")
  @Expose
  private String MobileNo;
  @SerializedName("Status")
  @Expose
  private String Status;
  @SerializedName("EmailId")
  @Expose
  private String EmailId;
  @SerializedName("UserName")
  @Expose
  private String UserName;
  @SerializedName("paymentemail")
  @Expose
  private String paymentemail;
  @SerializedName("EmailSignature")
  @Expose
  private String EmailSignature;
  @SerializedName("LocationCode")
  @Expose
  private String LocationCode;
  @SerializedName("EmailPassword")
  @Expose
  private String EmailPassword;
  @SerializedName("BranchId")
  @Expose
  private String BranchId;
  @SerializedName("Createddate")
  @Expose
  private String Createddate;
  @SerializedName("type")
  @Expose
  private String type;
  @SerializedName("Name")
  @Expose
  private String Name;
  @SerializedName("salesemail")
  @Expose
  private String salesemail;
  @SerializedName("paymentsms")
  @Expose
  private String paymentsms;
  @SerializedName("updatedate")
  @Expose
  private String updatedate;
  @SerializedName("LastLogin")
  @Expose
  private String LastLogin;
  @SerializedName("salessms")
  @Expose
  private String salessms;
  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("BranchName")
  @Expose
  private String BranchName;
  @SerializedName("Locationname")
  @Expose
  private String Locationname;
  @SerializedName("UserType")
  @Expose
  private String UserType;
  @SerializedName("BranchCode")
  @Expose
  private String BranchCode;
  @SerializedName("Password")
  @Expose
  private String Password;
  public void setMobileNo(String MobileNo){
   this.MobileNo=MobileNo;
  }
  public String getMobileNo(){
   return MobileNo;
  }
  public void setStatus(String Status){
   this.Status=Status;
  }
  public String getStatus(){
   return Status;
  }
  public void setEmailId(String EmailId){
   this.EmailId=EmailId;
  }
  public String getEmailId(){
   return EmailId;
  }
  public void setUserName(String UserName){
   this.UserName=UserName;
  }
  public String getUserName(){
   return UserName;
  }
  public void setPaymentemail(String paymentemail){
   this.paymentemail=paymentemail;
  }
  public String getPaymentemail(){
   return paymentemail;
  }
  public void setEmailSignature(String EmailSignature){
   this.EmailSignature=EmailSignature;
  }
  public String getEmailSignature(){
   return EmailSignature;
  }
  public void setLocationCode(String LocationCode){
   this.LocationCode=LocationCode;
  }
  public String getLocationCode(){
   return LocationCode;
  }
  public void setEmailPassword(String EmailPassword){
   this.EmailPassword=EmailPassword;
  }
  public String getEmailPassword(){
   return EmailPassword;
  }
  public void setBranchId(String BranchId){
   this.BranchId=BranchId;
  }
  public String getBranchId(){
   return BranchId;
  }
  public void setCreateddate(String Createddate){
   this.Createddate=Createddate;
  }
  public String getCreateddate(){
   return Createddate;
  }
  public void setType(String type){
   this.type=type;
  }
  public String getType(){
   return type;
  }
  public void setName(String Name){
   this.Name=Name;
  }
  public String getName(){
   return Name;
  }
  public void setSalesemail(String salesemail){
   this.salesemail=salesemail;
  }
  public String getSalesemail(){
   return salesemail;
  }
  public void setPaymentsms(String paymentsms){
   this.paymentsms=paymentsms;
  }
  public String getPaymentsms(){
   return paymentsms;
  }
  public void setUpdatedate(String updatedate){
   this.updatedate=updatedate;
  }
  public String getUpdatedate(){
   return updatedate;
  }
  public void setLastLogin(String LastLogin){
   this.LastLogin=LastLogin;
  }
  public String getLastLogin(){
   return LastLogin;
  }
  public void setSalessms(String salessms){
   this.salessms=salessms;
  }
  public String getSalessms(){
   return salessms;
  }
  public void setId(String id){
   this.id=id;
  }
  public String getId(){
   return id;
  }
  public void setBranchName(String BranchName){
   this.BranchName=BranchName;
  }
  public String getBranchName(){
   return BranchName;
  }
  public void setLocationname(String Locationname){
   this.Locationname=Locationname;
  }
  public String getLocationname(){
   return Locationname;
  }
  public void setUserType(String UserType){
   this.UserType=UserType;
  }
  public String getUserType(){
   return UserType;
  }
  public void setBranchCode(String BranchCode){
   this.BranchCode=BranchCode;
  }
  public String getBranchCode(){
   return BranchCode;
  }
  public void setPassword(String Password){
   this.Password=Password;
  }
  public String getPassword(){
   return Password;
  }
}