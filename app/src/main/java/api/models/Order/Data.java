package api.models.Order;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("customertype")
	private String customertype;

	@SerializedName("unpackage")
	private String unpackage;

	@SerializedName("orderType")
	private Object orderType;

	@SerializedName("package")
	private String jsonMemberPackage;

	@SerializedName("quantity")
	private int quantity;

	@SerializedName("color")
	private String color;

	@SerializedName("OrderMode")
	private Object orderMode;

	@SerializedName("discount")
	private double discount;

	@SerializedName("mobileno")
	private String mobileno;

	@SerializedName("basicprice")
	private Object basicprice;

	@SerializedName("number")
	private String number;

	@SerializedName("totalamount")
	private Object totalamount;

	@SerializedName("productname")
	private String productname;

	@SerializedName("insertdate")
	private String insertdate;

	@SerializedName("Id")
	private int id;

	@SerializedName("customername")
	private String customername;

	@SerializedName("remarks")
	private String remarks;

	public String getCustomertype(){
		return customertype;
	}

	public String getUnpackage(){
		return unpackage;
	}

	public Object getOrderType(){
		return orderType;
	}

	public String getJsonMemberPackage(){
		return jsonMemberPackage;
	}

	public int getQuantity(){
		return quantity;
	}

	public String getColor(){
		return color;
	}

	public Object getOrderMode(){
		return orderMode;
	}

	public double getDiscount(){
		return discount;
	}

	public String getMobileno(){
		return mobileno;
	}

	public Object getBasicprice(){
		return basicprice;
	}

	public String getNumber(){
		return number;
	}

	public Object getTotalamount(){
		return totalamount;
	}

	public String getProductname(){
		return productname;
	}

	public String getInsertdate(){
		return insertdate;
	}

	public int getId(){
		return id;
	}

	public String getCustomername(){
		return customername;
	}

	public String getRemarks(){
		return remarks;
	}
}