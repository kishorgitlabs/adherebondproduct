package api.models.Order;

import com.google.gson.annotations.SerializedName;

public class SalesOrderAdd{

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private Data data;

	public String getResult(){
		return result;
	}

	public Data getData(){
		return data;
	}
}