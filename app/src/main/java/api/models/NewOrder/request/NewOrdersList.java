
package api.models.NewOrder.request;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class NewOrdersList {

    @SerializedName("address")
    private String mAddress;
    @SerializedName("city")
    private String mCity;
    @SerializedName("Color")
    private String mColor;
    @SerializedName("contact")
    private String mContact;
    @SerializedName("contactname")
    private String mContactname;
    @SerializedName("cst")
    private String mCst;
    @SerializedName("custype")
    private String mCustype;
    @SerializedName("date")
    private String mDate;
    @SerializedName("dealername")
    private String mDealername;
    @SerializedName("deliveryaddress")
    private String mDeliveryaddress;
    @SerializedName("discount")
    private String mDiscount;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("executivename")
    private String mExecutivename;
    @SerializedName("Number")
    private String mNumber;
    @SerializedName("orderType")
    private String mOrderType;
    @SerializedName("ordermode")
    private String mOrdermode;
    @SerializedName("package")
    private String mPackage;
    @SerializedName("package2")
    private String mPackage2;
    @SerializedName("pan")
    private String mPan;
    @SerializedName("pincode")
    private String mPincode;
    @SerializedName("productname")
    private String mProductname;
    @SerializedName("quantity")
    private String mQuantity;
    @SerializedName("remarks")
    private String mRemarks;
    @SerializedName("state")
    private String mState;
    @SerializedName("status")
    private String mStatus;

    @SerializedName("vat")
    private String mVat;

    @SerializedName("basicprice")
    private String basicprice;

    @SerializedName("totalamount")
    private String totalamount;



    public String getmUnpackage() {
        return mUnpackage;
    }

    public void setmUnpackage(String mUnpackage) {
        this.mUnpackage = mUnpackage;
    }

    @SerializedName("Unpackage")
    private String mUnpackage;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getColor() {
        return mColor;
    }

    public void setColor(String color) {
        mColor = color;
    }

    public String getContact() {
        return mContact;
    }

    public void setContact(String contact) {
        mContact = contact;
    }

    public String getContactname() {
        return mContactname;
    }

    public void setContactname(String contactname) {
        mContactname = contactname;
    }

    public String getCst() {
        return mCst;
    }

    public String getBasicprice() {
        return basicprice;
    }

    public void setBasicprice(String basicprice) {
        this.basicprice = basicprice;
    }

    public String getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount;
    }

    public void setCst(String cst) {
        mCst = cst;
    }

    public String getCustype() {
        return mCustype;
    }

    public void setCustype(String custype) {
        mCustype = custype;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDealername() {
        return mDealername;
    }

    public void setDealername(String dealername) {
        mDealername = dealername;
    }

    public String getDeliveryaddress() {
        return mDeliveryaddress;
    }

    public void setDeliveryaddress(String deliveryaddress) {
        mDeliveryaddress = deliveryaddress;
    }

    public String getDiscount() {
        return mDiscount;
    }

    public void setDiscount(String discount) {
        mDiscount = discount;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getExecutivename() {
        return mExecutivename;
    }

    public void setExecutivename(String executivename) {
        mExecutivename = executivename;
    }

    public String getNumber() {
        return mNumber;
    }

    public void setNumber(String number) {
        mNumber = number;
    }

    public String getOrderType() {
        return mOrderType;
    }

    public void setOrderType(String orderType) {
        mOrderType = orderType;
    }

    public String getOrdermode() {
        return mOrdermode;
    }

    public void setOrdermode(String ordermode) {
        mOrdermode = ordermode;
    }

    public String getPackage() {
        return mPackage;
    }

    public void setPackage(String packag) {
        mPackage = packag;
    }

    public String getPackage2() {
        return mPackage2;
    }

    public void setPackage2(String package2) {
        mPackage2 = package2;
    }

    public String getPan() {
        return mPan;
    }

    public void setPan(String pan) {
        mPan = pan;
    }

    public String getPincode() {
        return mPincode;
    }

    public void setPincode(String pincode) {
        mPincode = pincode;
    }

    public String getProductname() {
        return mProductname;
    }

    public void setProductname(String productname) {
        mProductname = productname;
    }

    public String getQuantity() {
        return mQuantity;
    }

    public void setQuantity(String quantity) {
        mQuantity = quantity;
    }

    public String getRemarks() {
        return mRemarks;
    }

    public void setRemarks(String remarks) {
        mRemarks = remarks;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getVat() {
        return mVat;
    }

    public void setVat(String vat) {
        mVat = vat;
    }

}
