package api.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import api.converter.NullStringToEmptyAdapterFactory;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Sathriyan on 9/21/2017.
 */
public class RetroClient {
    private static final String ROOT_URL = "http://adherebondjson.brainmagicllc.com/";
    // private static final String ROOT_URL = "http://10.0.2.2:54358/";

    /**
     * Get Retrofit Instance
     */
    private static Retrofit getRetrofitInstance() {

        GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create(
                new GsonBuilder()
                        .registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory())
                        .create());
        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    private static Retrofit getRetrofitUplaodInstance() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .client(okClient())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    private static OkHttpClient okClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.MINUTES)
                .writeTimeout(15, TimeUnit.MINUTES)
                .readTimeout(15, TimeUnit.MINUTES)

                .build();
    }

    /**
     * Get API Service
     *
     * @return API Service
     */
    public static APIService getApiService() {
        return getRetrofitInstance().create(APIService.class);
    }
    public static APIService getApiUplaodService() {
        return getRetrofitUplaodInstance().create(APIService.class);
    }
}
