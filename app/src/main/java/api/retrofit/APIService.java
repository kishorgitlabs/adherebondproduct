package api.retrofit;


import api.models.ExistingOrder.request2.ExistOrders;
import api.models.ExistingOrder.response.OrderResult;
import api.models.Expense.ExpesneResponse;
import api.models.ExpenseModel;
import api.models.NewOrder.request.NewOrders;
import api.models.NewOrder.response.NewOrderResult;
import api.models.NewOrderView.NewOrdersView;
import api.models.Order.SalesOrderAdd;
import api.models.OrderView.SalesOrderView;
import api.models.addpdc.PdcData;
import api.models.colors.ColorsData;
import api.models.commen.CommenResponce;

import api.models.enquiry.EnqiryData;
import api.models.forget.ForgetData;
import api.models.login.Login;
import api.models.master.Master;
import api.models.packages.PackageData;
import api.models.payment.PaymentData;
import api.models.project.ProjectData;
import api.models.registration.Register;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;


/**
 * Created by system01 on 5/11/2017.
 */

public interface APIService {


    //Post Login User
    @FormUrlEncoded
    @POST("api/Values/salesorderpreview")
    public Call<SalesOrderView> orderview(
            @Field("mobileno") String UserName,
            @Field("customertype") String type
   );

    @FormUrlEncoded
    @POST("api/Values/salesorderpreview")
    public Call<NewOrdersView> neworderview(
            @Field("mobileno") String UserName,
            @Field("customertype") String type
    );
    @FormUrlEncoded
    @POST("api/Values/Login")
    public Call<Login> Login(
            @Field("Username") String UserName,
            @Field("Password") String Password);

    //Post Login User
    @FormUrlEncoded
    @POST("api/Values/salesorder")
    public Call<SalesOrderAdd> orderadd(
            @Field("mobileno") String mobileno,
            @Field("customertype") String customertype,
            @Field("customername") String customername,
            @Field("productname") String productname,
            @Field("package") String packages,
            @Field("unpackage") String unpackage,
            @Field("quantity") String quantity,
            @Field("number") String number,
            @Field("color") String color,
            @Field("basicprice") String basicprice,
            @Field("discount") String discount,
            @Field("totalamount") String total,
            @Field("ordermode") String order,
            @Field("ordertype") String ordertype,
            @Field("remarks") String remarks

    );


    @FormUrlEncoded
    @POST("api/Values/getexpense")
    public Call<ExpenseModel> expense(
            @Field("Mobile") String mobno);

    @FormUrlEncoded
    @POST("api/Values/expensedetail")
    public Call<ExpesneResponse> postExpense(
            @Field("Name") String UserName,
            @Field("Mobile") String mobile,
            @Field("usertype") String usertype,
            @Field("mode") String mode,
            @Field("ticketamt") String tiamt,
            @Field("foodamt") String food,
            @Field("otheramt") String other,
            @Field("openkm") String Password,
            @Field("closekm") String openkm,
            @Field("fuelamt") String famt,
            @Field("remark") String remark,
            @Field("traveldate") String tradae
    );



 //Post Forget Password User
    @FormUrlEncoded
    @POST("api/Values/Forgot")
    public Call<CommenResponce> Forget(
            @Field("Email") String Email);

    // Registration
    @FormUrlEncoded
    @POST("api/Values/mobileregister")
    Call<Register> register(
            @Field("name") String name,
            @Field("mobileno") String mobno,
            @Field("email") String email,
            @Field("usertype") String usertype,
            @Field("city") String city,
            @Field("state") String selectedState,
            @Field("country")  String country);

    // Download data
    @FormUrlEncoded
    @POST("api/Values/SalesData")
    Call<Master> GetMasterData(
            @Field("SalesPersonName") String name);

    @FormUrlEncoded
    @POST("api/Values/Forgot")
    Call<ForgetData> GetMasterData1(
            @Field("EmailId") String emailid);

    @POST("api/Values/PlaceOrderExisting")
    Call<OrderResult> EXIST_ORDERS_CALL(@Body ExistOrders existOrders);

    //Save New Order
    @POST("api/Values/PlaceOrderNew")
    Call<NewOrderResult> NEW_ORDER_RESULT_CALL(@Body NewOrders newOrdersList);

    @GET("api/Values/Project")
    public Call<ProjectData> current1();

    //payment collection
    @FormUrlEncoded
    @POST("api/Values/PaymentCollection")
    Call<PaymentData> payment(
            @Field("SalesPersonName") String name,
            @Field("PartyName") String partyname,
            @Field("PaymentType") String type,
            @Field("Remarks") String remarks,
            @Field("BillNo") String billno,
            @Field("Amount") String amount,
            @Field("PaidAmount")  String paidamount,
            @Field("ChqueNumber")  String chequenum,
            @Field("ChequeDate")  String cheqdate,
            @Field("BankName")  String bank,
            @Field("DueDate")  String duedate);

   @FormUrlEncoded
   @POST("api/Values/Enquirydetail")
   Call<EnqiryData> enquiry(
           @Field("name") String name,
           @Field("ContactNo") String mobno,
           @Field("Description") String description,
           @Field("Address") String address,
           @Field("Email ") String email);

    //pdc cheque
    @FormUrlEncoded
    @POST("api/Values/AddPDCCheque")
    Call<PdcData> pdc(
            @Field("Customername") String name,
            @Field("ChequeNo") String cheque,
            @Field("ChequeDate") String chequedate,
            @Field("BankName") String bankname,
            @Field("Amount") String amount,
            @Field("SalesPerson") String sales);

    @GET("api/Values/Color")
    public Call<ColorsData> colors();


    @GET("api/Values/Package")
    public Call<PackageData> package1();
}
